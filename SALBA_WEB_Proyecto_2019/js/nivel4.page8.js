﻿$(document).ready(function () {
    $("#funcion").click(function () {


        var text = "<p style='margin:-1.5% pr:2.5% 0;color:black;font-size:20px;margin-bottom:35px;'align=justify>// Ejemplo : calculo de la potencia Usaremos el mismo ejercicio que usamos para los procedimientos, para haceruna demostración de cómo cambiaria el programa usando una función para el cálculo de la potencia. </p>"
            + "<p style='margin:-2.5% 0;color:blue ;font-size: 18px;'>entero base,expo,pot</p>"
            + "<br><br><p style='margin:-2.5% 0;color:blue ;font-size: 17px;'>funcion Potencia(entero base , entero expo): entero</p>"
            + "<br><br><p style='margin:-2.5% 0;color: blue;font-size: 17px;margin-left: 50px'>entero i, resp</p>"
            + "<br><br><p style='margin:-2.5% 0;color: blue;font-size: 17px;'>inicio</p>"
            + `<br><br><p style='margin:-2.5% 0;color: blue;font-size: 17px;margin-left: 50px'>resp<-1</p>`
            + `<br><br><p style='margin:-2.5% 0;color: blue;font-size: 17px;margin-left: 50px'>para i <- 1 hasta expo haga</p>`
            + `<br><br><p style='margin:-2.5% 0;color: blue;font-size: 17px;margin-left: 70px'>resp <- resp * base<p>`
            + `<br><p style='margin:-2.5% 0;color: blue;font-size: 17px;margin-left: 50px'>fin para</p>`
            + `<br><br><p style='margin:-2.5% 0;color: blue;font-size: 17px;margin-left: 50px'>retorne resp</p>`
            + `<br><br><p style='margin:-2.5% 0;color: blue;font-size: 17px;'>fin</p>`
            + `<br><br><p style='margin:-2.5% 0;color: blue;font-size: 17px;>procedimiento ingreso(var entero base,var entero expo)</p>`
            + `<br><br><p style='margin:-2.5% 0;color: blue;font-size: 17px;'>inicio</p>`
            + `<br><br><p style='margin:-2.5% 0;color: blue;font-size: 17px;margin-left: 50px'>escriba "Ingresar la base"</p>`
            + `<br><br><p style='margin:-2.5% 0;color: blue;font-size: 17px;margin-left: 50px'>lea base</p>`
            + `<br><br><p style='margin:-2.5% 0;color: blue;font-size: 17px;margin-left: 50px'>Escriba " Ingresar el exponente "</p>`
            + `<br><br><p style='margin:-2.5% 0;color: blue;font-size: 17px;margin-left: 50px'>lea expo</p>`
            + `<br><br><p style='margin:-2.5% 0;color: blue;font-size: 17px;'>fin</p>`
            + `<br><br><p style='margin:-2.5% 0;color: blue;font-size: 17px;'>inicio</p>`
            + `<br><br><p style='margin:-2.5% 0;color: blue;font-size: 17px;margin-left: 50px'>llamar ingreso(base,expo)</p>`
            + `<br><br><p style='margin:-2.5% 0;color: blue;font-size: 17px;margin-left: 50px'>pot<-potencia(base,expo)</p>`
            + `<br><br><p style='margin:-2.5% 0;color: blue;font-size: 17px;margin-left: 50px'>Escriba "Potencia es ", pot</p>`
            + `<br><br><p style='margin:-2.5% 0;color: blue;font-size: 17px;'>fin</p>`;

        var writer = ""; writer.length = 0; //Limpiar el string
        var maxLength = text.length;
        var count = 0;
        var speed = 10000 / maxLength; //La velocidad varía dependiendo de la cantidad de caracteres

        var write = setInterval(function () {

            if (count > text.length) { clearInterval(write); }

            writer += text.charAt(count);
            document.getElementById("text").innerHTML = "" + writer + "";
            count++;

        }, speed);

        $("#proce2").click(function () { clearInterval(write); });
    });
});