$(document).ready(function(){
    function layerUno(){
        //Palabras recervadas del lenguaje lua.
        let k = ["FIN CASE", "Fin","fin","FIN","fin case","Llamar nueva_linea","llamar nueva_linea","LLAMAR NUEVA_LINEA","ESCRIBA","escriba","Escriba", "fincase","FINCASE","fincase","CASE","inicio","INICIO", "SINO","sino", "elseif", "function",
             "entonces","ENTONCES","REAL","para","Para","PARA","fin mientras","FIN MIENTRAS","Fin Mientras","fin para","FIN PARA","Fin Para","mientras","Mientras","hasta","haga","Hasta","Haga","HAGA","HAGA","MIENTRAS","REPITA","repita","Repita" ,"Inicio","INICIO","inicio","entero","Entero","Real,","real","si","SI" ,"finsi","fin","FIN","ESCRIBA","escriba","lea","LEA", "not", "or", "repeat", "return", "then", "until", "while"];

        let re;
        let c = document.querySelector(".highlight").innerHTML; //c�digo crudo del Textarea.
      
        //Cadenas de texto entre comillas simples y comillas dobles.
        c = c.replace(/(['""].*?['""])/g, "<span class=\"string\">$1</span>");

        //colores para los numeros.
        c = c.replace(/\b(\d+)/g, "<span class=\"numberColor\">$1</span>");

        //Colores para las palabras recervadas de love2d.
        c = c.replace(/(love.load|love.update|love.draw|love.keypressed)/g, "<span class=\"keywordLove2dEvents\">$1</span>");

        //Coloreado para las functiones queno esten definidas.
        c = c.replace(/(function\s*\w*)/g, "<span class=\"customFunctions\">$1</span>");

        //Colores para los corchetes en lua [ "(" y ")" ].
        c = c.replace(/([\(\)])/g, "<span class=\"sc\">$1</span>");

        //Palabras recervadas de lua.
        for (var i = 0; i < k.length; i++) {
        re = new RegExp("\\b" + k[i] + "\\b", "g");
        c = c.replace(re, "<span class=\"keyword\">" + k[i] + "</span>");
        }

        //Comentarios de dobles --[[...]]
        c = c.replace(/<span class=\"sc\">--\/\*<\/span>/g, "//");
        c = c.replace(/<span class=\"sc\">]]<\/span>/g, "]]");

        //En JS el operador punto no puede coincidir con saltos de l�nea.
        //As� que vamos a utilizar [\s\S] como un truco para seleccionar todo (espacio o caracteres no espaciales).
        c = c.replace(/(--\[\[[\s\S]*?\]\])|(--.*\n?)/g, clear_spans);

        //Etiquetas para encerar codigos c and c++ en lua
        c = c.replace(/\b(\[\[[\s\S]*?\]\])|\s+(\[\[[\s\S]*?\]\])/g, clear_spans2);

        $(".layer").html(c)//injecting the code into the pre tag

        //Elimina los estilos que contengan las palabras y les asigna la de los comentarios. 
        function clear_spans(match) {
        match = match.replace(/<span.*?>/g, "");
        match = match.replace(/<\/span>/g, "");
        return "<span class=\"comment\">" + match + "</span>";
        }

        //Elimina los estilos que contengan las palabras y les asigna la de los comentarios.
        function clear_spans2(match) {
        match = match.replace(/<span.*?>/g, "");
        match = match.replace(/<\/span>/g, "");
        return "<span class=\"UsingcommentC\">" + match + "</span>";
        }
    }
	layerUno();
    function layerdos(){
        //Palabras recervadas del lenguaje lua.
        let k = ["FIN CASE", "Fin","fin","FIN","fin case","Llamar nueva_linea","llamar nueva_linea","LLAMAR NUEVA_LINEA","ESCRIBA","escriba","Escriba", "fincase","FINCASE","fincase","CASE","inicio","INICIO", "SINO","sino", "elseif", "function",
             "entonces","ENTONCES","para","Para","PARA","fin mientras","FIN MIENTRAS","Fin Mientras","fin para","FIN PARA","Fin Para","mientras","Mientras","hasta","haga","Hasta","Haga","HAGA","HAGA","MIENTRAS","REPITA","repita","Repita" ,"Inicio","INICIO","inicio","entero","Entero","Real,","real","si","SI" ,"finsi","fin","FIN","ESCRIBA","escriba","lea","LEA", "not", "or", "repeat", "return", "then", "until", "while"];

        let re;
        let c = document.querySelector(".highlight2").innerHTML; //c�digo crudo del Textarea.

        //Cadenas de texto entre comillas simples y comillas dobles.
        c = c.replace(/(['""].*?['""])/g, "<span class=\"string\">$1</span>");

        //colores para los numeros.
        c = c.replace(/\b(\d+)/g, "<span class=\"numberColor\">$1</span>");

        //Colores para las palabras recervadas de love2d.
        c = c.replace(/(love.load|love.update|love.draw|love.keypressed)/g, "<span class=\"keywordLove2dEvents\">$1</span>");

        //Coloreado para las functiones queno esten definidas.
        c = c.replace(/(function\s*\w*)/g, "<span class=\"customFunctions\">$1</span>");

        //Colores para los corchetes en lua [ "(" y ")" ].
        c = c.replace(/([\(\)])/g, "<span class=\"sc\">$1</span>");

        //Palabras recervadas de lua.
        for (var i = 0; i < k.length; i++) {
        re = new RegExp("\\b" + k[i] + "\\b", "g");
        c = c.replace(re, "<span class=\"keyword\">" + k[i] + "</span>");
        }

        //Comentarios de dobles --[[...]]
        c = c.replace(/<span class=\"sc\">--\/\*<\/span>/g, "//");
        c = c.replace(/<span class=\"sc\">]]<\/span>/g, "]]");

        //En JS el operador punto no puede coincidir con saltos de l�nea.
        //As� que vamos a utilizar [\s\S] como un truco para seleccionar todo (espacio o caracteres no espaciales).
        c = c.replace(/(--\[\[[\s\S]*?\]\])|(--.*\n?)/g, clear_spans);

        //Etiquetas para encerar codigos c and c++ en lua
        c = c.replace(/\b(\[\[[\s\S]*?\]\])|\s+(\[\[[\s\S]*?\]\])/g, clear_spans2);

        $(".layer2").html(c)//injecting the code into the pre tag

        //Elimina los estilos que contengan las palabras y les asigna la de los comentarios. 
        function clear_spans(match) {
        match = match.replace(/<span.*?>/g, "");
        match = match.replace(/<\/span>/g, "");
        return "<span class=\"comment\">" + match + "</span>";
        }

        //Elimina los estilos que contengan las palabras y les asigna la de los comentarios.
        function clear_spans2(match) {
        match = match.replace(/<span.*?>/g, "");
        match = match.replace(/<\/span>/g, "");
        return "<span class=\"UsingcommentC\">" + match + "</span>";
        }
    }
    layerdos();
})
