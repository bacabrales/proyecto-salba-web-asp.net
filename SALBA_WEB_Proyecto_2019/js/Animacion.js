﻿$(document).ready(function () {
    //alert("Hola mundo");
    $("#write").click(function () {
        var text = "<p style='margin:2.5% pr:2.5% 0;color:black;font-size:20px;margin-bottom:35px;'align=justify>Ejemplo: elaborar un procedimiento que presente 5 asteriscos en una línea horizontal.</p>"
            + "<p style='margin:-2.5% 0;color:blue ;font-size: 18px;'>cadena[25] nombre</p>" +
            "<br><br><p style='margin:-2.5% 0;color: blue;font-size: 18px;'>procedimiento asteriscos (Entero numero)</p>"
            + "<br><br><p style='margin:-2.5% 0;color: blue;font-size: 18px; margin-left: 40px;'>entero I</p>"
            + "<br><br><p style='margin:-2.5% 0;color: blue;font-size: 18px; '>inicio</p>"
            + `<br><br><p style='margin:-2.5% 0;color: blue;font-size: 18px; margin-left: 40px;'>para i <- 1 hasta 5 haga</p>`
            + `<br><br><p style='margin:-2.5% 0;color: blue;font-size: 18px; margin-left: 45px;'>escriba "*"<p>`
            + `<br><p style='margin:-2.5% 0;color: blue;font-size: 18px; margin-left: 40px;'>fin para</p>`
            + `<br><br><p style='margin:-2.5% 0;color: blue;font-size: 18px;'>fin</p>`
            + `<br><br><p style='margin:-2.5% 0;color: blue;font-size: 18px; '>inicio</p>`
            + `<br><br><p style='margin:-2.5% 0;color: blue;font-size: 18px; margin-left: 40px;'>escriba "Ingresar el nombre ..:"</p>`
            + `<br><br><p style='margin:-2.5% 0;color: blue;font-size: 18px; margin-left: 40px;'>lea nombre</p>`
            + `<br><br><p style='margin:-2.5% 0;color: blue;font-size: 18px; margin-left: 40px;'>llamar asteriscos</p>`
            + `<br><br><p style='margin:-2.5% 0;color: blue;font-size: 18px; margin-left: 40px;'>llamar nueva_linea</p>`
            + `<br><br><p style='margin:-2.5% 0;color: blue;font-size: 18px; margin-left: 40px;'>escriba nombre</br>`
            + `<br><p style='margin:-2.5% 0;color: blue;font-size: 18px; margin-left: 40px;'>llamar nueva_linea</br>`
            + `<br><p style='margin:-2.5% 0;color: blue;font-size: 18px; margin-left: 40px;'>llamar asteriscos</p>`
            + `<br><br><p style='margin:-2.5% 0;color: blue;font-size: 18px;'>fin</p>`;

        var writer = ""; writer.length = 0; 
        var maxLength = text.length;
        var count = 0;
        var speed = 10000 / maxLength; 

        var write = setInterval(function () {

            if (count > text.length) { clearInterval(write); }

            writer += text.charAt(count);
            document.getElementById("text").innerHTML = "" + writer + "";
            count++;

        }, speed);

        $("#write").click(function () { clearInterval(write); }); 
    });
});