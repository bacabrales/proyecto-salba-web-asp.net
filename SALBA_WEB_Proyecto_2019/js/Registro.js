$(document).ready(function(){

    $('#frmRegistro').on('submit',validarFormulario);
    $('#txtUsuario').on('input',validarUsuarioExistente);
    $('#txtEmail').on('input',validarCorreoExistente);
    function validarFormulario(event){
        event.preventDefault();
        let txtNombre = document.querySelector('#txtNombre').value,
        txtApellidos = document.querySelector('#txtApellidos').value,
        txtUsuario = document.querySelector('#txtUsuario').value,
        txtEmail = document.querySelector('#txtEmail').value,
        txtContraseña = document.querySelector('#txtContrasena').value,
        txtContraseñaVerifi = document.querySelector('#txtContraseñaVerifi').value;
        if(txtNombre.trim()===''||txtApellidos.trim()===''||txtUsuario.trim()===''||txtEmail.trim()===''||txtContraseña.trim()===''){
            alert('Todos los campos son obligatorios');
        }else{
            if(txtContraseña != txtContraseñaVerifi){
                alert('No coincide la contraseña');
            }
            else{
                registrarUsuario();
            }
        }
    }
    function registrarUsuario(){
        $.ajax({
            type: "POST",
            url: "../App/Controlador/UsuarioControlador.php",
            data: $('#frmRegistro').serialize()+"&ac=insert", //Acccion insert para insertar
            dataType: "JSON",
        })
        .done(function(response){
            if(response.informacion==='correcto'){
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Registro correcto',
                    showConfirmButton: false,
                    timer: 2000
                  })
            }else{
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: 'No se pudo registrar',
                    showConfirmButton: false,
                    timer: 2000
                  })
            }
        })
        .fail(function(responsee){
            console.log(responsee.responseText);
        });
    }
    function validarUsuarioExistente(e){
        let txtUsuario = e.target.value
        $.ajax({
            type: "POST",
            url: "../App/Controlador/UsuarioControlador.php",
            data: "&txtUsuario="+txtUsuario+"&ac=conusu", //Acccion conusu para consultar :V
            dataType: "JSON",
        })
        .done(function(response){
            if(response.count==='1'){
                $("#txtUsuario").css({"border": "2px solid #dc3545"});
            }else{
                $("#txtUsuario").css("border", "2px solid #59B548");
            }
        })
        .fail(function(responsee){
            console.log(responsee.responseText);
        });
    }
    function validarCorreoExistente(e){
        let txtCorreo = e.target.value
        $.ajax({
            type: "POST",
            url: "../App/Controlador/UsuarioControlador.php",
            data: "&txtUsuario="+txtCorreo+"&ac=conusu", //Acccion conusu para consultar :V
            dataType: "JSON",
        })
        .done(function(response){
            if(response.count==='1'){
                $("#txtEmail").css({"border": "2px solid #dc3545"});
            }else{
                $("#txtEmail").css("border", "2px solid #59B548");
            }
        })
        .fail(function(responsee){
            console.log(responsee.responseText);
        });
    }


});