﻿$(document).ready(function () {

    $("#btnModNom").on("click", function () {
        $("#txtNombreMo").prop("disabled",false);
    });

    $("#btnModApe").on("click", function () {
        $("#txtApellidoMo").prop("disabled", false);
    });

    $("#btnModUsu").on("click", function () {
        $("#txtUsuarioMo").prop("disabled", false);
    });

    $("#btnModCorr").on("click", function () {
        $("#txtCorreoMo").prop("disabled", false);
    });

    $("#btnModContra").on("click", function () {
        $("#modificarContrasena").removeAttr("hidden");
        $("#contGuardar").removeAttr("hidden");
        $("#btnModContraCerr").removeAttr("hidden");
        $("#btnModContra").hide();
    });

    $("#btnModContraCerr").on("click", function () {
        $("#modificarContrasena").attr("hidden","hidden");
        $("#contGuardar").attr("hidden", "hidden");
        $("#btnModContraCerr").attr("hidden", "hidden");
        $("#btnModContra").show();
    });

});