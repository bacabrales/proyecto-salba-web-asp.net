﻿$(document).ready(function () {

    $("#winston2").draggable({ scope: "dos" });
    $("#winston1").draggable({ scope: "uno" });



    $("#dropzone1").droppable({
        scope: "uno",
        drop: function () {
            $("#txtValidarIntroA").val(1);
        },
        out: function () {
            $("#txtValidarIntroA").val(0);
        }
    });

    $("#dropzone2").droppable({
        scope: "dos",
        drop: function () {
            $("#txtValidarIntroB").val(1);
        },
        out: function () {
            $("#txtValidarIntroB").val(0);
        }
    });



    $("#winston3").draggable({ scope: "tres" });
    $("#winston4").draggable({ scope: "cuatro" });
    $("#winston5").draggable({ scope: "cinco" });


    $("#dropzone3").droppable({
        scope: "tres",
        drop: function () {
            $("#txtValiPeg3A").val(1);
        },
        out: function () {
            $("#txtValiPeg3A").val(0);
        }
    });

    $("#dropzone4").droppable({
        scope: "cuatro",
        drop: function () {
            $("#txtValiPeg3B").val(1);
        },
        out: function () {
            $("#txtValiPeg3B").val(0);
        }
    });

    $("#dropzone5").droppable({
        scope: "cinco",
        drop: function () {
            $("#txtValiPeg3C").val(1);
        },
        out: function () {
            $("#txtValiPeg3C").val(0);
        }
    });

});