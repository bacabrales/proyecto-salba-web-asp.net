﻿
$(document).ready(function () {
    $("#item1-1,#item1-2,#item1-3,#item1-4").draggable();
    var total = 0;
    $("#cont1-1").droppable({
        tolerance: "fit",
        drop: function (event, ui) {
            accept: "#item1-1",
                $(this)
                    .addClass("ui-state-highlight")
                    .find("p")
            total++
            validarTotal(total);
            $("#item1-1").draggable("disable");
        }
    });
    $("#cont1-2").droppable({
        tolerance: "fit",
        accept: "#item1-3",
        drop: function (event, ui) {
            $(this)
                .addClass("ui-state-highlight")
                .find("p");
            total++
            validarTotal(total);
            $("#item1-3").draggable("disable");
        }
    });
    function validarTotal(total2) {
        if (total2 === 2) {
            alert('correcto')
            $("#btnEnviar").prop("disabled", false);
            total = 0;
        }
    }
    //Actividad 2
    $("#item2-1,#item2-2,#item2-3,#item2-4,#item2-5,#item2-6").draggable();

    $("#cont2-1").droppable({
        tolerance: "fit",
        accept: "#item2-1",
        drop: function (event, ui) {
            
                $(this)
                    .addClass("ui-state-highlight")
                    .find("p")
            total++
            validarTotal2(total);
            $("#item2-1").draggable("disable");
        }
    });
    $("#cont2-2").droppable({
        tolerance: "fit",
        accept: "#item2-2",
        drop: function (event, ui) {
            $(this)
                .addClass("ui-state-highlight")
                .find("p");
            total++
            validarTotal2(total);
            $("#item2-2").draggable("disable");
        }
    });
    $("#cont2-3").droppable({
        tolerance: "fit",
        accept: "#item2-3",
        drop: function (event, ui) {
            $(this)
                .addClass("ui-state-highlight")
                .find("p");
            total++
            validarTotal2(total);
            $("#item2-3").draggable("disable");
        }
    });
    $("#cont2-4").droppable({
        tolerance: "fit",
        accept: "#item2-4",
        drop: function (event, ui) {
            $(this)
                .addClass("ui-state-highlight")
                .find("p");
            total++
            validarTotal2(total);
            $("#item2-4").draggable("disable");
        }
    });
    $("#cont2-5").droppable({
        tolerance: "fit",
        accept: "#item2-5",
        drop: function (event, ui) {
            $(this)
                .addClass("ui-state-highlight")
                .find("p");
            total++
            validarTotal2(total);
            $("#item2-5").draggable("disable");
        }
    });
   
    function validarTotal2(total2) {
        if (total2 === 5) {
            alert('correcto')
            $("#btnEnviar").prop("disabled", false);
            total = 0;
        }
    }
    //actividad tres
    $("#btnActividad3").on("click", function () {
        if ($("input:radio[name=r1]:checked").val() == 0) {
            alert('correcto')
            $("#btnEnviar").prop("disabled", false);
        } else {
            alert('incorrecto');
        }
    });
    //actvidad cuatro
    $("#btnActividad4").on("click", function () {
        if ($("input:radio[name=r2]:checked").val() == 0) {
            alert('correcto')
            $("#btnEnviar").prop("disabled", false);
        } else {
            alert('incorrecto');
        }
    });


    //EXamen
    $("#item3-1,#item3-2,#item3-3,#item3-4,#item3-5").draggable();
 
 
    $("#cont3-1").droppable({
        tolerance: "fit",
        accept: "#item3-1",
        drop: function (event, ui) {
            
            $(this)
                .addClass("ui-state-highlight")
                    .find("p")


            $("#p11").attr('checked', true);
            $("#item3-1").draggable("disable");

         
        },
        out: function (event, ui) {
            $(this).removeClass("ui-state-highlight");
           
        },
      

    });
    $("#cont3-2").droppable({
        tolerance: "fit",
        accept: "#item3-2",
        drop: function (event, ui) {
            $(this)
                .addClass("ui-state-highlight")
                .find("p");
            $("#p12").attr('checked', true);
            $("#item3-2").draggable("disable");
        },
        out: function (event, ui) {
            $(this).removeClass("ui-state-highlight");
            
        }
    });
    $("#cont3-3").droppable({
        tolerance: "fit",
        accept: "#item3-3",
        drop: function (event, ui) {
            $(this)
                .addClass("ui-state-highlight")
                .find("p");
            $("#p13").attr('checked', true);
            $("#item3-3").draggable("disable");
          
        },
        out: function (event, ui) {
            $(this).removeClass("ui-state-highlight");
           
        }
    });
    $("#cont3-4").droppable({
        tolerance: "fit",
        accept: "#item3-4",
        drop: function (event, ui) {
            $(this)
                .addClass("ui-state-highlight")
                .find("p");
            $("#p14").attr('checked', true);
            $("#item3-4").draggable("disable");
           
        },
        out: function (event, ui) {
            $(this).removeClass("ui-state-highlight");
           
        }
    });
    $("#cont3-5").droppable({
        tolerance: "fit",
        accept: "#item3-5",
        drop: function (event, ui) {
            $(this)
                .addClass("ui-state-highlight")
                .find("p");
            $("#p15").attr('checked', true);
            $("#item3-5").draggable("disable");
          
           
        },
        out: function (event, ui) {
            $(this).removeClass("ui-state-highlight");
          
        }
        
    });


    
});
