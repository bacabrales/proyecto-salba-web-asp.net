$(document).ready(function(){

    $('#frmSesion').on('submit',validarSesion);

    function validarSesion(event){
        event.preventDefault();
        let txtNombre = document.querySelector('#txtUsuarioSesion').value,
        txtContraseña = document.querySelector('#txtContrasenaSesion').value;
        if(txtNombre.trim()===''||txtContraseña.trim()===''){
            alert('Todos los campos son obligatorios');
        }else{
            iniciarSesion();
        }
    }
    function iniciarSesion(){
        $.ajax({
            async: true,
            type: "POST",
            url: "../App/Controlador/UsuarioControlador.php",
            data: $('#frmSesion').serialize()+"&ac=consult", //Acccion consul para consultar :V
            dataType: "JSON",
        })
        .done(function(response){
            if(response.informacion==='correcto'){
                location.href="principal.php";
            }
        })
        .fail(function(responsee){
            console.log(responsee.responseText);
        });
    }



});


