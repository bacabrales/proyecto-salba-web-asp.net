﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SALBA_WEB_Proyecto_2019.Controlador;

namespace SALBA_WEB_Proyecto_2019
{
    public partial class FrmNivel6 : System.Web.UI.Page
    {
        private ControladorTemporalidad tem = new ControladorTemporalidad();
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                
                
                string usuario = (string)Session["usuario"];
                string password = (string)Session["password"];
                if (usuario != string.Empty && password != string.Empty)
                {
                    nombreUsuario.InnerText = (string)Session["nombre"] + " " + (string)Session["apellido"];
                    ViewState["pregunta"] = tem.bucarTemporalidadNivel6((int)Session["id"]);
                    switch ((int)ViewState["pregunta"])
                    {
                        case 1:
                            ViewState["pregunta"] = 1;
                            break;
                        case 2:
                            preguntaUno.Visible = false;
                            preguntaDos.Visible = true;
                            break;
                        case 3:
                            preguntaUno.Visible = false;
                            preguntaTres.Visible = true;
                            break;
                        case 4:
                            preguntaUno.Visible = false;
                            preguntaCuatro.Visible = true;
                            break;
                        case 5:
                            preguntaUno.Visible = false;
                            PreguntarExamen.Visible = true;
                            break;
                        
                    }
                    valor.InnerText = Convert.ToString((int)ViewState["pregunta"]);
                    

                }
                else
                {
                    Response.Redirect("https://localhost:44300/FrmHome.aspx");
                }
                

                
            }

        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            int num;
            switch ((int)ViewState["pregunta"])
            {

                case 1:
                    preguntaUno.Visible = false;
                    preguntaDos.Visible = true;
                    num = (int)ViewState["pregunta"] + 1;
                    ViewState["pregunta"] = num;
                    tem.insertarTem6((int)Session["id"], 2);
                    break;
                case 2:
                    preguntaDos.Visible = false;
                    preguntaTres.Visible = true;
                    num = (int)ViewState["pregunta"] + 1;
                    ViewState["pregunta"] = num;
                    tem.insertarTem6((int)Session["id"], 3);
                    break;
                case 3:
                    preguntaTres.Visible = false;
                    preguntaCuatro.Visible = true;
                    num = (int)ViewState["pregunta"] + 1;
                    ViewState["pregunta"] = num;
                    tem.insertarTem6((int)Session["id"], 4);
                    break;
                case 4:
                    preguntaCuatro.Visible = false;
                    PreguntarExamen.Visible = true;
                    num = (int)ViewState["pregunta"] + 1;
                    ViewState["pregunta"] = num;
                    tem.insertarTem6((int)Session["id"], 5);
                    break;
                case 5:
                    //Aqui validar el envio a la base de datos de cuantas preguntas fueron correctas
                    ControladorNivel6 n = new ControladorNivel6();
                    int nota;
                    if(p11.Checked && p12.Checked && p13.Checked && p14.Checked && p15.Checked)
                    {
                        nota = 5;
                    }
                    else
                    {
                        nota = 0;
                    }
                    
                    int cont=n.guardar(this, (int)Session["id"],nota);
                    if (cont>=1)
                    {
                        Response.Write("<script>alert('Aprobó el Examen')</script>");
                        Response.Redirect("https://localhost:44300/Views/FrmPrincipal.aspx");

                    }
                   

                    break;
            }
           
            valor.InnerText = Convert.ToString((int)ViewState["pregunta"]);
        }

        protected void btnRegresar_Click(object sender, EventArgs e)
        {
            if ((int)ViewState["pregunta"] == 1)
            {
                //Redirecciona al inicio de la pagina de las instrucciones
            }
            else if ((int)ViewState["pregunta"] == 2)
            {
                preguntaDos.Visible = false;
                preguntaUno.Visible = true;
                ViewState["pregunta"] = (int)ViewState["pregunta"] - 1;
                tem.insertarTem6((int)Session["id"], 1);

            }
            else if ((int)ViewState["pregunta"] == 3)
            {
                preguntaTres.Visible = false;
                preguntaDos.Visible = true;
                ViewState["pregunta"] = (int)ViewState["pregunta"] - 1;
                tem.insertarTem6((int)Session["id"], 2);

            }
            else if ((int)ViewState["pregunta"] == 4)
            {
                preguntaCuatro.Visible = false;
                preguntaTres.Visible = true;
                ViewState["pregunta"] = (int)ViewState["pregunta"] - 1;
                tem.insertarTem6((int)Session["id"], 3);

            }
            else if ((int)ViewState["pregunta"] == 5)

            {
                PreguntarExamen.Visible = false;
                preguntaCuatro.Visible = true;
                //Aqui validar el envio a la base de datos de cuantas preguntas fueron correctas
                ViewState["pregunta"] = (int)ViewState["pregunta"] - 1;
                tem.insertarTem6((int)Session["id"], 4);

            }
            valor.InnerText = Convert.ToString((int)ViewState["pregunta"]);
        }

        protected void btnCerrarSesion_Click(object sender, EventArgs e)
        {
            Session["usuario"] = string.Empty;
            Session["password"] = string.Empty;
            Response.Redirect("https://localhost:44300/FrmHome.aspx");
        }

        protected void btnSi_Click(object sender, EventArgs e)
        {
            PreguntarExamen.Visible = false;
            preguntaCinco.Visible = true;
            btnRegresar.Enabled = false;
            btnEnviar.Text = "Enviar";
            btnEnviar.Enabled = true;
        }

        protected void btnNo_Click(object sender, EventArgs e)
        {
            Response.Redirect("https://localhost:44300/Views/FrmPrincipal.aspx");
        }
    }
}