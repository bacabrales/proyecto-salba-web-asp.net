﻿.<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrmNivel3.aspx.cs" Inherits="SALBA_WEB_Proyecto_2019.FrmNivel3" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>SALBA - Nivel 3</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <link rel="stylesheet" href="../css/Style.css" />
    <link rel="stylesheet" href="../css/estilosPropios.css" />
    <link href="../css/estilos.css" rel="stylesheet" />
    <link rel="stylesheet" href="../css/main.css" />
    <link rel="icon" href="../img/logoSena.png" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" />
    <script src="https://kit.fontawesome.com/a5cc566dc4.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" />
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web&display=swap" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <style>
        .azul {
            color: blue;
        }
    </style>
</head>
<body>
    <form id="frmPreguntaNivel3" runat="server" style="height: 100%; width: 100%">
        <nav class="navbar navbar-expand-sm navbar-light bg-light bg-Blanco navbar-expand-md shadow-sm fixed-top">
            <div class="container">

                <a class="navbar-brand prueba" href="#">
                    <img src="../img/LogoSalba.png" width="130" class="img-fluid" alt="" /></a>
                <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="collapsibleNavId">
                    <div class="navbar-nav ml-auto mt-2 mt-lg-0">
                        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                            <li class="nav-item active">
                                <a href="#" class="btn  mr-1 text-primary enlace">Inicio</a>
                            </li>
                            <li class="nav-item active">
                                <a href="#" class="btn  mr-1 text-primary enlace">Scores</a>
                            </li>
                            <li class="nav-item">
                                <a href="#" style="" class="btn text-black ml-1 enlace"><i class="fas fa-user-circle mr-2"></i><b id="nombreUsuario" runat="server"></b></a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-cog"></i>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#">Configurar Perfil</a>
                                    <a class="dropdown-item" href="#">Politicas</a>
                                    <div class="dropdown-divider"></div>
                                    <asp:Button ID="btnCerrarSesion" CssClass="dropdown-item" runat="server" Text="Cerrar sesión" OnClick="btnCerrarSesion_Click" />
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>

        <%-- ///////////////////////////NIVEL TRES//////////////////////////////////// --%>

        <div id="preguntaUno" class="row contenedor-barra1 " runat="server" visible="true">
            <div class="col-lg-4 contenedor-barra">
                <div class="columna-info">
                    <div class=" mt-3">
                        <div class="col bg-alert alert-success">
                            <!--Titulo del Nivel-->
                            <h2 class="py-2"><strong>Nivel Tres</strong> </h2>
                        </div>
                    </div>
                    <div class="">
                        <div class="">
                            <!--Contenido del nivel informacion referente-->
                            <p class="text-justify mt-3">Introduccion a los ciclos </p>
                            <h4><strong>Introducción del nivel tres</strong></h4>
                            <p class="text-justify">
                                Este nivel especifica el concepto  de ciclos y los
              diferentes tipos. Al tener la claridad de los temas,
              se efectúa prácticas didácticas con cada uno
              de los conceptos aprendidos y se realizará un
              test evaluativo como una actividad de aprehensión
              de los puntos tratados.
                            </p>
                            <p class="text-justify">
                                Los ciclos o también conocidos como bucles, son una estructura de control de total importancia para el proceso de creación de un programa. Prácticamente todos los lenguajes más populares de la actualidad nos permiten hacer uso de estas estructuras y muchas más.
                Acontinuación las variables que más se utilizan en los ciclos:
                            </p>
                            <h5>Contador</h5>

                            <p>
                                Es una variable en la cual se le asigna un valor que se
                  incrementa o decrementa en cada iteración de un bucle.
                            </p>
                            <h5>Acumulador</h5>
                            <p>
                                Es una variable cuya misión es almacenar diferentes valores.
              Sumas, restas, multiplicaciones, divisiones, potenciaciones entre otros.
                            </p>
                            <h5>Los diferentes tipos de ciclo que existen</h5>
                            <p>Ciclo para</p>
                            <p>Ciclo mientras</p>
                            <p>Ciclo repita</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <!--contenido a evaluar o preguntar-->
                <h5 class="text-center"><strong>Ejemplo de ciclo</strong></h5>
                <p>Un algoritmo que muestre en consola los  números del  0 hasta el 10</p>
                <div>
                    <pre class="layer">&nbsp;</pre>
                    <textarea class="highlight">
ENTERO i
INICIO
PARA i<-0 HASTA 10 HAGA

ESCRIBA  i
LLAMAR NUEVA_LINEA
FIN PARA
FIN</textarea>
                </div>

            </div>
        </div>

        <%-- //////////////////////CICLO PARA///////////////////////////--%>

        <div id="preguntaDos" class="row contenedor-barra1 " runat="server" visible="false">
            <div class="col-lg-4 contenedor-barra">
                <div class="columna-info">
                    <div class=" mt-3">
                        <div class="col bg-alert alert-success">
                            <!--Titulo del Nivel-->
                            <h2 class="py-2"><strong>Ciclo para</strong> </h2>
                        </div>
                    </div>
                    <div class="">
                        <div class="">
                            <!--Contenido del nivel informacion referente-->
                            <h4><strong>Definición</strong></h4>
                            <p class="text-justify">
                                El ciclo para o for se utiliza generalmente para ejecutar un conjunto de instrucciones que se repiten un número de veces, hasta que la condición asignada a dicho bucle deja de cumplirse.
                            </p>
                            <h4><strong>Sintaxis</strong></h4>
                            <p class="text-justify">
                                para variable <- valor_inicial hasta valor_final haga 
                                <p class="ml-3">instrucciones</p>
                                fin para
                            </p>
                            <h4><strong>Descripción</strong></h4>
                            <p class="text-justify">
                                El ciclo Para se utiliza generalmente para ejecutar un conjunto de instrucciones que se repiten un número de veces, establecido antes de ejecutar el ciclo.
                                <br />
                                <strong>Variable :</strong>Es de tipo entero
                                <br />
                                <strong>Valor_inicial :</strong>Este puede se un numero entero o una variable entera.
                                <br />
                                <strong>Valor_final : </strong>Este puede se un numero entero o una variable entera.
                            </p>
                            <h4><strong>Ejemplo</strong></h4>
                            <pre class="layer">&nbsp;</pre>
                            <textarea class="highlight" name="highlight">
ENTERO i, final
INICIO
ESCRIBA “Ingresar el numero de veces a repetir el ciclo “
LEA final
PARA i <- 1 HASTA final HAGA

ESCRIBA i
LLAMAR NUEVA_LINEA
FIN PARA
FIN</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <!--contenido a evaluar o preguntar-->
                <h5 class="text-center my-4"><strong>Ciclo para</strong></h5>
                <p class="">El ejercico es para calcular el factorial de un número Arrastrar las palabras claves en donde está el vacio </p>
                <div class="">
                    <div class="mt-5">
                        <p><b class="azul">ENTERO</b><b> i, numero, factorial</b></p>
                        <p><b class="azul">INICIO</b></p>
                        <p><b class="ml-3">Factorial    <-</b><b class="cafe">1</b></p>
                        <p><b class=" azul ml-3">ESCRIBA</b> <b class="cafe">"Ingresar el numero para determinar su factorial"</b></p>
                        <p><b class="azul ml-3">LEA </b><b>numero</b></p>
                        <p>
                            <asp:TextBox ID="drop" runat="server" class="azul ml-4" Enabled="false" Text=""></asp:TextBox><b> i<-</b><b class="cafe mr-1">1</b><b class="azul mr-1">HASTA</b><asp:TextBox ID="drop2" runat="server" Enabled="false" Text=""></asp:TextBox><b class="azul"> HAGA</b></p>
                        <p><b class="ml-5">factorial<-</b><asp:TextBox ID="drop3" runat="server" class="ml-4" Enabled="false" Text=""></asp:TextBox><b>* i</b></p>
                        <p><b class="azul ml-4">FIN PARA</b></p>
                        <p><b class="azul ml-4 mr-1">ESCRIBA</b><b class="cafe">"factorial de "</b><b> , numero,</b><b class="cafe">"es"</b><b>,factorial</b></p>
                        <p><b class="azul">FIN</b></p>
                        <p>Arrastar las palabras donde está el vacio, para saber que es correcto debe salir un mensaje diciendo que es correcto</p>
                        <br>
                        <div>
                            <div>
                                <b class="azul" id="dragga1">PARA</b>
                            </div>
                            <br />
                            <div>
                                <b class="azul" id="dragga2">numero</b>
                            </div>
                            <br />
                            <div>
                                <b class="azul" id="dragga3">factorial</b>
                            </div>
                        </div>
                        <br />
                        <div class="mt-5 mb-5">
                            <input type="text" value="0" id="txtParaUno" runat="server" />
                            <input type="text" value="0" id="txtParaDos" runat="server" />
                            <input type="text" value="0" id="txtParaTres" runat="server" />
                        </div>
                        <br />
                    </div>
                </div>
            </div>
        </div>

        <%-- /////////////////////////CICLO MIENTRAS///////////////////////// --%>

        <div class="row contenedor-barra1 " id="preguntaTres" runat="server" visible="false">
            <div class="col-lg-4 contenedor-barra">
                <div class="columna-info">
                    <div class=" mt-3">
                        <div class="col bg-alert alert-success">
                            <!--Titulo del Nivel-->
                            <h2 class="py-2"><strong>Ciclo mientras</strong> </h2>
                        </div>
                    </div>
                    <div class="">
                        <div class="">
                            <!--Contenido del nivel informacion referente-->
                            <h4><strong>Definición</strong></h4>
                            <p class="text-justify">
                                El ciclo mientras o while se utiliza cuando se quiere ejecutar     repetidamente un bloque
                               de instrucciones basado en una condición. el bucle se repite mientras la condición se cumple.
                            </p>
                            <h4><strong>Sintaxis</strong></h4>
                            <p class="text-justify">
                                mientras condición haga 
                                <p class="ml-3">instrucciones</p>
                                fin mientras
                            </p>
                            <h4><strong>Descripción</strong></h4>
                            <p class="text-justify">El ciclo mientras se utiliza cuando se quiere ejecutar repetidamente un bloque instrucciones basado en una condición, el ciclo se repite mientras la condición se cumple.</p>
                            <h4><strong>Ejemplo</strong></h4>
                            <pre class="layer">&nbsp;</pre>
                            <textarea class="highlight" name="highlight">
Entero i,contador
Inicio

PARA i <- 1 HASTA 4 HAGA
contador<-contador+i+2
ESCRIBA contador
LLAMAR NUEVA_LINEA
FIN PARA
FIN</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <!--contenido a evaluar o preguntar-->
                <h5 class="text-center my-4"><strong>Ejemplo del ciclo mientras</strong></h5>
                <p class="">El ejercico es para calcular la suma siguiente: 100 + 98 + 96 + 94 + . . . + 0 en este orden. Arrastrar las palabras claves en donde está el vacio</p>
                <div class="">
                    <b class="azul">ENTERO</b> <b>total</b>
                    <br />
                    <b class="azul">INICIO</b><br />
                    <b>total   <-</b> <b class="cafe">100</b><br />
                    <asp:TextBox ID="dropM1" runat="server" class="azul ml-3"></asp:TextBox><b> total   >   0</b><b class="azul"> HAGA </b>
                    <br />
                    <b class="azul ml-5">ESCRIBA</b><b class="cafe">" + "</b> <b>, total</b><br />
                    <asp:TextBox ID="dropM2" runat="server" class="ml-5"></asp:TextBox><b> <-total-</b><b class="cafe"> 2</b><br />
                    <br />
                    <asp:TextBox ID="dropM3" runat="server" class="azul ml-3"></asp:TextBox><br />
                    <b class="azul ml-5">ESCRIBA</b><b class="cafe">" + "</b> <b>,</b><b class="cafe"> 0</b><br />
                    <b class="azul">FIN</b>
                    <br />
                    <p id="bien"></p>
                    <p id="mal"></p>
                    <p>Las palabras para arrastar donde esta el vacio</p>
                    <br />
                    <div>
                        <p>
                            <b id="draggaM" class="azul">MIENTRAS</b>
                        </p>
                        <br />
                        <p>
                            <b id="draggaM2">total</b>
                        </p>
                        <br />
                        <p>
                            <b id="draggaM3" class="azul">FIN MIENTRAS</b>
                        </p>
                    </div>
                    <br />
                    <div class="mt-5 mb-5">
                        <input type="text" value="0" id="txtmientrasUno" runat="server" />
                        <input type="text" value="0" id="txtMientrasDos" runat="server" />
                        <input type="text" value="0" id="txtMientrasTres" runat="server" />
                    </div>
                </div>
            </div>
        </div>

        <%-- /////////////////////CICLO REPITA///////////////////////////--%>

        <div class="row contenedor-barra1 " id="preguntaCuatro" runat="server" visible="false">
            <div class="col-lg-4 contenedor-barra">
                <div class="columna-info">
                    <div class=" mt-3">
                        <div class="col bg-alert alert-success">
                            <!--Titulo del Nivel-->
                            <h2 class="py-2"><strong>Ciclo repita</strong></h2>
                        </div>
                    </div>
                    <div class="">
                        <div class="">
                            <!--Contenido del nivel informacion referente-->
                            <h4><strong>Definición</strong></h4>
                            <p class="text-justify">El ciclo repita o de while, no interrumpirá su ejecución hasta que la condición sea cumplida</p>
                            <h4><strong>Sintaxis</strong></h4>
                            <p class="text-justify">
                                Repita
                                <p class="ml-3">instrucciones</p>
                                Hasta condición
                            </p>
                            <h4><strong>Descripción</strong></h4>
                            <p class="text-justify">El ciclo repita es lo contrario al ciclo mientras, en éste la ejecución se lleva a cabo hasta que se cumple la condición impuesta. La condición no se verifica al inicio como el en ciclo mientras se verifica al final </p>
                            <h4><strong>Ejemplo</strong></h4>
                            <p class="text-justify">Realice un algoritmo que permita calcular y dar como salida el promedio general de una sección, tomando en cuenta que está compuesta por 25 estudiantes y que se tiene la nota definitiva de cada uno de ellos.</p>
                            <pre class="layer">&nbsp;</pre>
                            <textarea class="highlight" name="highlight">
ENTERO e,est
REAL pr,pg,suma
INICIO
e<-1
est<-0
suma<-0
REPITA
ESCRIBA " digitar el promedio final de cada estudinte " ,e, ":"
LEA pr
LLAMAR NUEVA_LINEA
suma<-suma+pr
e<-e+1
est<-est+1

HASTA (est=10)
  pg<-suma/est
  ESCRIBA " el promedio general de la seccion es: " ,pg
FIN
            </textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <!--contenido a evaluar o preguntar-->
                <div>
                    <h5 class="text-center my-4"><strong>Ejercicio del ciclo repita</strong></h5>
                    <p class="">Ingresar el nombre del alumno, la  nota  , luego preguntar si desea continuar , al final presentar el numero de aprobados y reprobados.</p>
                    <div class="">
                        <b class="azul">CARACTER</b> <b>resp</b>
                        <br />
                        <b class="azul">REAL</b> <b>nota</b>
                        <br />
                        <b class="azul">ENTERO</b> <b>ca,cr</b>
                        <br />
                        <asp:TextBox ID="droppableR1" runat="server" class="azul" Enabled="false"></asp:TextBox><b>[</b><b class="cafe">25</b><b>]</b><b> nombre</b><br />
                        <b class="azul">INICIO</b><br />
                        <b>ca<- </b><b class="cafe">0</b><br />
                        <b>cr<- </b><b class="cafe">0</b><br />
                        <asp:TextBox ID="droppableR2" runat="server" class="azul" Enabled="false"></asp:TextBox><br />
                        <b class="azul ml-3">ESCRIBA</b> <b class="cafe">"Ingresar el nombre del alumno :"</b><br />
                        <b class="azul ml-3">LEA</b> <b>nombre</b><br />
                        <b class="azul ml-3">ESCRIBA</b> <b class="cafe">"Ingresar la nota del alumno:"</b><br />
                        <b class="azul ml-3">LEA</b> <b>nota</b><br />
                        <b class="azul ml-4">SI</b><b class="ml-2">nota >=</b><b class="cafe">60</b><b class="azul">ENTONCES</b><br />
                        <b class="ml-5">ca<-ca+</b><b class="cafe">1</b><br />
                        <b class="azul ml-4">SINO</b><br />
                        <b class="ml-5">cr<-cr+</b><b class="cafe">1</b><br />
                        <b class="azul ml-4">FIN SI</b><br />
                        <b class="azul ml-5">ESCRIBA</b> <b class="cafe">" Desea continuar S/N"</b><br />
                        <b class="azul ml-5">Lea</b> <b>resp</b><br />
                        <asp:TextBox ID="droppableR3" runat="server" class="azul mr-1" Enabled="false"></asp:TextBox><b>(resp=</b><b class="cafe">'n'</b><b>)</b><b class="azul ml-1 mr-1">o</b><b>(resp=</b><b class="cafe">'N'</b><b>)</b><br />
                        <b class="azul ml-5">ESCRIBA</b> <b class="cafe">"Aprobados "</b><b>,ca</b><br />
                        <b class="azul ml-5">LLAMAR NUEVA_LINEA</b><br />
                        <b class="azul ml-5">ESCRIBA</b> <b class="cafe">"Reprobados "</b><b>,cr</b><br />
                        <b class="azul">FIN</b>
                        <br />
                        <p>Arrastar las palabras donde está el vacio, para saber que es correcto debe salir un mensaje diciendo que es correcto</p>
                        <br />
                        <p id="mostrar"></p>
                        <div>
                            <p>
                                <b id="draggaR1" class="azul">CADENA</b>
                            </p>
                            <br />
                            <p>
                                <b id="draggaR2" class="azul">REPITA</b>
                            </p>
                            <br />
                            <p>
                                <b id="draggaR3" class="azul">HASTA</b>
                            </p>
                        </div>
                        <div class="mt-5 mb-5">
                            <input type="text" value="0" id="txtRepitaUno" runat="server" />
                            <input type="text" value="0" id="txtRepitaDos" runat="server" />
                            <input type="text" value="0" id="txtRepitaTres" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <%-- ////////////////////////PRIMER PREGUNTA/////////////////////////7777 --%>

        <div class="row contenedor-barra1 " id="preguntaCinco" runat="server" visible="false">
            <div class="col-lg-4 contenedor-barra">
                <div class="columna-info">
                    <div class=" mt-3">
                        <div class="col bg-alert alert-success">
                            <!--Titulo del Nivel-->
                            <h2 class="py-2"><strong>Evaluación</strong></h2>
                        </div>
                    </div>
                    <div class="">
                        <div class="">
                            <!--Contenido del nivel informacion referente-->
                            <p class="text-justify mt-3"></p>
                            <h4><strong>Primera pregunta</strong></h4>
                            <p>1.Realizar un algoritmo para calcular los múltiplos del número 5</p>
                            <pre class="layer">&nbsp;</pre>
                            <textarea class="highlight">
ENTERO n,i,m4
INICIO
ESCRIBA "calcula multiplos de 5 :"
LEA n
PARA i <- HASTA n HAGA
m4 <- i*4
LLAMAR NUEVA_LINEA
escriba M4
FIN PARA
FIN</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <!--contenido a evaluar o preguntar-->
                <div>
                    <div class="mt-5 ml-2">
                        <h5 class="text-justify ">¿Cuál es el resultado del siguiente ejercicio?</h5>
                        <div>
                            <p>A)<asp:RadioButton ID="rbtUno" runat="server" GroupName="uno" Text="5,10,15,20" /></p>
                            <p>B)<asp:RadioButton ID="rbtDos" runat="server" GroupName="uno" Text="2,4,6,8" /></p>
                            <p>C)<asp:RadioButton ID="rbtTres" runat="server" GroupName="uno" Text="4,8,12,16,20" /></p>
                            <p>D)<asp:RadioButton ID="rbtCuatro" runat="server" GroupName="uno" Text="4,8" /></p>
                        </div>
                        <div class="text-center ">
                            <img src="../img/Pensando.jpg" width="300" height="300" />
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <%-- ///////////////SEGUNDA PREGUNTA////////////////////////////// --%>

        <div class="row contenedor-barra1 " id="preguntaSeis" runat="server" visible="false">
            <div class="col-lg-4 contenedor-barra">
                <div class="columna-info">
                    <div class=" mt-3">
                        <div class="col bg-alert alert-success">
                            <!--Titulo del Nivel-->
                            <h2 class="py-2"><strong>Segunda pregunta</strong></h2>
                        </div>
                    </div>
                    <div class="">
                        <div class="">
                            <!--Contenido del nivel informacion referente-->
                            <p class="text-justify mt-3"></p>
                            <h4><strong></strong></h4>
                            <p class="text-justify">Realizar un algoritmo para calcular el areá de un rectángulo.</p>
                            <p class="text-justify">Complete la condicion usando el ciclo while “Arrastre la palabra a la casilla correspondiente”.</p>
                            <div class="mt-3">
                                <img src="../img/pensando2.png" width="300px" height="300px" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <!--contenido a evaluar o preguntar-->
                <div>
                    <b class="azul">ENTERO</b><b>lado1,</b><b> lado2,</b><b>area,</b><b>contador</b><br />
                    <b class="azul">INICIO</b><br />
                    <b>contador</b><b><-</b><b>0</b>
                    <br />
                    <asp:TextBox ID="drooppa1" runat="server" class=""></asp:TextBox></> <b>contador</b> <b><-</b> <b>0</b> <b class="azul">HAGA</b><br />
                    <b class="azul ml-5 mr-1">ESCRIBA</b> <b>"digite el primer lado:"</b><br />
                    <b class="azul ml-5 mr-1">LEA</b> <b>lado1</b><br />
                    <b class="azul ml-5 mr-1">ESCRIBA</b> <b class="cafe">"digite el segundo lado :"</b><br />
                    <b class="azul ml-5 mr-1">LEA</b><asp:TextBox ID="drooppa2" runat="server"></asp:TextBox><br />
                    <br />
                    <asp:TextBox ID="drooppa3" runat="server" class="ml-5"></asp:TextBox><b><-</b> <b>lado1</b> <b>*</b> <b>lado2</b><br />
                    <b class="azul ml-5 ">ESCRIBA</b> <b>"el area del rectangulo es :" ,area</b><br />
                    <b class="azul ml-4 ">FIN</b> <b class="azul">MIENTRAS</b><br />
                    <b class="">FIN</b>
                    <br />
                    <br />
                    <div>
                        <div>
                            <p>
                                <b id="draggable1" class="azul">MIENTRAS</b>
                            </p>
                            <br />
                            <p>
                                <b id="draggable2">lado2</b>
                            </p>
                            <br />
                            <p>
                                <b id="draggable3">area</b>
                            </p>
                        </div>
                        <div class="mt-5 mb-5">
                            <input type="text" value="0" id="txtSegunda1" runat="server" />
                            <input type="text" value="0" id="txtSegunda2" runat="server" />
                            <input type="text" value="0" id="txtSegunda3" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <%-- ///////////////////////TERCERA PREGUNTA///////////////////////////////////// --%>

        <div class="row contenedor-barra1 " id="preguntaSiete" runat="server" visible="false">
            <div class="col-lg-4 contenedor-barra">
                <div class="columna-info">
                    <div class=" mt-3">
                        <div class="col bg-alert alert-success">
                            <!--Titulo del Nivel-->
                            <h2 class="py-2"><strong>Tercera pregunta</strong> </h2>
                        </div>
                    </div>
                    <div class="">
                        <div class="">
                            <!--Contenido del nivel informacion referente-->
                            <p class="text-justify mt-3"></p>
                            <h4><strong></strong></h4>
                            <p>3..¿En el siguiente ejercicio, seleccione que variable esta mal escrita?</p>
                            <p></p>
                            <pre class="layer">&nbsp;</pre>
                            <textarea class="highlight">
REAL num, suma,cont
INICIO
	cont<-0
	suma<-0
		MIENTRAS cont < 3 HASTA
			ESCRIBIR "ingrese el numero"
			LEER num
			suma <- suma+num
			cont <- cont+1
		FIN ENTONCES
	ESCRIBIR "el resultado de la suma es: ",suma
FIN </textarea><br />
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <!--contenido a evaluar o preguntar-->
                <div>
                    <div>
                        <div class="mt-5">
                            <h5 class="text-justify ">¿Cuál es el resultado del siguiente ejercicio?</h5>
                            <p>A)<asp:RadioButton ID="rbtTresUNO" runat="server" GroupName="tres" Text="suma,cont,MIENTRAS" /></p>
                            <p>B)<asp:RadioButton ID="rbtTresDOS" runat="server" GroupName="tres" Text="ESCRIBA,LEA,HAGA" /></p>
                            <p>C)<asp:RadioButton ID="rbtTresTRES" runat="server" GroupName="tres" Text="<-,FIN MIENTRAS, REAL" /></p>
                            <p>D)<asp:RadioButton ID="rbtTresCUATRO" runat="server" GroupName="tres" Text="HASTA,ESCRIBIR,LEER,ENTONCES" /></p>
                            <br />
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <%-- /////////////////////////////////CUARTA PREGUNTA/////////////////////////////////////////////// --%>

        <div class="row contenedor-barra1 " id="preguntaOcho" runat="server" visible="false">
            <div class="col-lg-4 contenedor-barra">
                <div class="columna-info">
                    <div class=" mt-3">
                        <div class="col bg-alert alert-success">
                            <!--Titulo del Nivel-->
                            <h2 class="py-2"><strong>Cuarta pregunta</strong> </h2>
                        </div>
                    </div>
                    <div class="">
                        <div class="">
                            <!--Contenido del nivel informacion referente-->
                            <p class="text-justify mt-3"></p>
                            <h4><strong></strong></h4>
                            <p class="text-justify">Realizar un algoritmo para calcular la cantidad de nombres de los empleados de la empresa Distribuidora Aldana</p>
                            <p class="text-justify">Complete el código arrastrando los textos hasta completar el algoritmo</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <!--contenido a evaluar o preguntar-->
                <div>
                    <b class="azul">ENTERO </b><b>contador</b><br />
                    <b class="azul">CADENA</b><b>[</b><b class="cafe">25</b><b>]</b><asp:TextBox ID="droopp1" runat="server" Enabled="false"></asp:TextBox><br />
                    <b class="azul ">INICIO</b><br />
                    <b>contador<-</b><b class="cafe ml-3">0</b><br />
                    <b class="azul ml-5 mr-1">MIENTRAS</b><asp:TextBox ID="droopp2" runat="server" Enabled="false"></asp:TextBox><b><</b><b class="cafe ml-1 mr-1">10</b><b class="azul">HAGA</b></p>
                    <b class="azul ml-5 mr-1">ESCRIBA</b><b class="cafe">"Ingresar los nombres"</b>
                    <b class="azul  ml-5 mr-1">LEA</b><b>nombre</b>
                    <b>contador</b><b> <-</b><b>contador + 1</b><br />
                    <b>
                        <asp:TextBox ID="droopp3" runat="server" class="azul ml-3 mr-2" Enabled="False"></asp:TextBox></b><br />
                    <b class="azul">FIN <b />
                        <br />
                </div>
                <div>
                    <p>
                        <b id="dragg1">nombre</b>
                    </p>
                    <br />
                    <p>
                        <b id="dragg2">contador</b>
                    </p>
                    <p>
                        <b id="dragg3" class="azul">FIN MIENTRAS</b>
                    </p>
                </div>
                <br />
                <div class="mt-5 mb-5">
                    <input type="text" value="0" id="txtCuatro1" runat="server" />
                    <input type="text" value="0" id="txtCuatro2" runat="server" />
                    <input type="text" value="0" id="txtCuatro3" runat="server" />
                </div>
            </div>
        </div>

        <%-- ////////////////////////////////QUINTA PREGUNTA//////////////////////////////////////////////////// --%>

        <div class="row contenedor-barra1 " id="preguntaNueve" runat="server" visible="false">
            <div class="col-lg-4 contenedor-barra">
                <div class="columna-info">
                    <div class=" mt-3">
                        <div class="col bg-alert alert-success">
                            <!--Titulo del Nivel-->
                            <h2 class="py-2"><strong>Quinta pregunta</strong> </h2>
                        </div>
                    </div>
                    <div class="">
                        <div class="">
                            <!--Contenido del nivel informacion referente-->
                            <p class="text-justify mt-3"></p>
                            <h4><strong></strong></h4>
                            <p class="text-justify">Realizar un algoritmo para calcular el nombre y la cantidad de estudiantes que aprueben o reprueban el examen final del área de informática</p>
                            <p>Complete el siguiente codigo del ciclo repita.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <!--contenido a evaluar o preguntar-->
                <div>
                    <div>
                        <asp:TextBox ID="txtQuintoUno" runat="server" Class=" azul mr-1"></asp:TextBox><b>respuesta</b><br />
                        <b class="azul">REAL</b><b>nota</b><br />
                        <b class="azul">ENTERO</b><b>aprobado, reprobado</b><br />
                        <b class="azul">CADENA</b><b>[</b><b class="cafe">25</b><b>]</b><b>nombre</b><br />
                        <b class="azul">INICIO</b><br />
                        <asp:TextBox ID="txtQuintoDos" runat="server" Class="azul ml3"></asp:TextBox><br />
                        <b class="azul ml-5 mr-1">ESCRIBA</b><b class="cafe">"Ingrese el nombre del alumno: "</b><br />
                        <b class="azul ml-5 mr-1">LEA</b><b>nombre</b><br />
                        <b class="azul ml-5 mr-1">ESCRIBA</b><b class="cafe">"Ingrese la nota del alumno"</b><br />
                        <b class="azul ml-5 mr-1">LEA</b><b>nota</b><br />
                        <b class="azul ml-5 mr-1">SI</b><b>nota</b><b>>=</b><b class="cafe mx-1">3.0</b><b class="azul">ENTONCES</b><br />
                        <b class="ml-5">aprobado</b><b><-</b><b>aprobado</b><b>+</b><b class="cafe">1</b><br />
                        <b class="azul ml-4">SINO</b><br />
                        <b class="ml-5">reprobado</b><b><-</b><b>reprobado</b><b>+</b><b class="cafe">1</b><br />
                        <b class="azul ml-4">FIN SI</b><br />
                        <b class="azul ml-5 mr-1">ESCRIBA</b><b class="cafe">"Desea continuar S/N"</b><br />
                        <b class="azul ml-5 mr-1">LEA</b><asp:TextBox ID="txtQuintaTres" runat="server"></asp:TextBox><br />
                        <b class="azul ml-3 mr-1">HASTA</b><b>(respuesta=</b><b class="cafe">'n'</b><b>)</b><b class="azul">o</b><b>(respuesta=</b><b class="cafe">'N'</b><b>)</b><br />
                        <b class="azul ml-5">ESCRIBA</b><b class="cafe">"aprobado:"</b><b>,aprobado</b><br />
                        <b class="azul ml-5">LLAMAR NUEVA_LINEA</b><br />
                        <b class="azul ml-5">ESCRIBA</b><b class="cafe">"réprobado:"</b><b>,reprobado</b><br />
                        <b class="azul">FIN</b>
                    </div>
                </div>
                <div>
                    <div>
                        <asp:Button ID="btnPreguntaCinco" runat="server" Text="Validar" OnClick="btnPreguntaCinco_Click" />
                    </div>
                </div>
            </div>
        </div>

        <footer id="footerPrincipal" class="bg-dark" runat="server">
            <div class="container bg-dark">
                <div class="row bg-dark p-2 text-center justify-content-center align-items-center">
                    <div class="col-2 col-lg-4">
                        <asp:Button CssClass="btn btn-outline-light" ID="btnRegresar" runat="server" Text="Atrás" OnClick="btnRegresar_Click" />
                    </div>
                    <div class="col-2 col-lg-4">
                        <p class="text-white d-block"><span id="valor" runat="server">1</span>/<span>9</span></p>
                    </div>
                    <div class="col-2 col-lg-4">
                        <asp:Button CssClass="btn btn-outline-light" ID="Button1" runat="server" Text="Siguiente" OnClick="btnEnviar_Click" />
                    </div>
                </div>
            </div>
        </footer>
    </form>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="../js/PreguntasDos.js"></script>
    <script src="../js/PreguntaCuatro.js"></script>
    <script src="../js/bootstrap.bundle.js"></script>
    <script src="../js/bootstrap.bundle.min.js"></script>
    <script src="../js/bootstrap.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/main.js"></script>
    <script src="../js/mientras.js"></script>
    <script src="../js/para.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/PreguntasNivelTres.js"></script>
    <script src="../js/Registro.js"></script>
    <script src="../js/repita.js"></script>
    <script src="../js/Script.js"></script>
    <script src="../js/sweetalert2.all.min.js"></script>

</body>
</html>
