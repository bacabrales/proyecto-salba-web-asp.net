﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace SALBA_WEB_Proyecto_2019
{
    public partial class FrmNivel5 : System.Web.UI.Page
    {
        public static int loco;
        protected void Page_Load(object sender, EventArgs e)
        {
          
           
            if (!IsPostBack)
            {
                verMas.Visible = false;
                ViewState["pregunta"] = 0;
                ViewState["P1"] = 0;
                ViewState["P2"] = 0;
                ViewState["P3"] = 0;
                ViewState["P4"] = 0;
                ViewState["P5"] = 0;
                ViewState["validar"] = 0;
                Session["validar07"] = 0;




                //   cambiarScript.InnerHtml = "<script src='../js/MainNivel55.js' ></script>";

                //string usuario = (string)Session["usuario"];
                //string password = (string)Session["password"];
                //if (usuario != string.Empty && password != string.Empty)
                //{
                //    nombreUsuario.InnerText = (string)Session["nombre"] + " " + (string)Session["apellido"];
                //}
                /*
                else
                {
                    Response.Redirect("https://localhost:44300/FrmHome.aspx");
                }
                */
            }
            else
            {
                loco = (int)ViewState["validar"];
                if (loco == 1)
                {
                    ViewState["validar"] = 0;
                    cambiarScript.InnerHtml = "<script src='../js/MainNivel5.js' ></script>";
                }
                else
                {

                    cambiarScript.InnerHtml = "<script src='../js/MainNivel55.js' ></script>";
                }
              
            }
        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {

           string Resultado = ResultadoOne.Value.ToString();
           string Resultado1 =TotalTres.Value.ToString();

            if ((int)ViewState["pregunta"] == 0) {

                introduccion.Visible = false;
                preguntaUno.Visible = true;
                ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;
            }
            else if ((int)ViewState["pregunta"] == 1)
            {

                if (rbPregunta1.SelectedItem != null)
                {





                    if (rbPregunta1.SelectedValue == "C)una zona de almacenamiento contiguo que contiene una serie de elementos del mismo tipo")
                    {
                        ViewState["P1"] = 0;
                        ViewState["P1"] = (int)ViewState["P1"] + 5;

                    }
                    else
                    {
                        ViewState["P1"] = 0;
                    }


                    preguntaUno.Visible = false;
                    preguntaDos.Visible = true;
                    ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;

                }
                else
                {
                    preguntaUno.Visible = true;
                }
            }


            else if ((int)ViewState["pregunta"] == 2)
            {


                if (rbPregunta2.SelectedItem != null)
                {




                    if (rbPregunta2.SelectedValue == "B)168")
                    {
                        ViewState["P2"] = 0;
                        ViewState["P2"] = (int)ViewState["P2"] + 5;

                    }
                    else {
                        ViewState["P2"] = 0;
                    }



                    preguntaDos.Visible = false;
                    preguntaTres.Visible = true;
                    ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;

                }
                else
                {
                    preguntaDos.Visible = true;
                }
            }
            else if ((int)ViewState["pregunta"] == 3)
            {
                ViewState["validar"] = (int)ViewState["validar"] + 1;


                if (dl1.SelectedItem != null && dl2.SelectedItem != null)
                {
                    int x = 0;

                    if (dl1.SelectedValue == "Falso")
                    {

                        x++;


                    }
                    if (dl2.SelectedValue == "Verdadero")
                    {
                        x++;

                    }

                    if (x == 2)
                    {

                        ViewState["P3"] = 0;
                        ViewState["P3"] = (int)ViewState["P3"] + 5;
                    }
                    else {
                        ViewState["P3"] = 0;
                    }



                    preguntaTres.Visible = false;
                    preguntaCuatro.Visible = true;
                    ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;

                }
                else
                {
                    preguntaTres.Visible = true;
                }

            }
            else if ((int)ViewState["pregunta"] == 4)
            {


                if (Resultado1 == "3")
                {
                    ViewState["P4"] = 0;
                    ViewState["P4"] = (int)ViewState["P4"] + 5;
                    preguntaCuatro.Visible = false;
                    preguntaCinco.Visible = true;
                      
                }
                else {
                    ViewState["P4"] = 0;
                }
                if (Resultado1 == "")

                    preguntaCuatro.Visible = false;
                    preguntaCinco.Visible = true;
                {

                }

                ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;
                ultimoNivel.Value = "0";








            }
            else if ((int)ViewState["pregunta"] == 5)
            {

                int total = 0;
                //en la variable promedio queda el número de respuestas correctas
                int promedio = 0;
                if (Resultado == "2")
                {

                    ViewState["P5"] = 0;
                    ViewState["P5"] = (int)ViewState["P5"] + 5;



                }
                else {
                    ViewState["P5"] = 0;
                }
                if (Resultado == "")
                {


                    total = (int)ViewState["P1"] + (int)ViewState["P2"] + (int)ViewState["P3"] + (int)ViewState["P4"] + (int)ViewState["P5"];
                    promedio = total / 5;
                    if (promedio >= 4)
                    {
                        Response.Redirect("FrmNivel6.aspx");
                    }
                    else
                    {

                        Response.Redirect("FrmNivel5.aspx", true);


                    }

                }
                else
                {

                    total = (int)ViewState["P1"] + (int)ViewState["P2"] + (int)ViewState["P3"] + (int)ViewState["P4"] + (int)ViewState["P5"];
                    promedio = total / 5;
                    if (promedio >= 4)
                    {
                        Response.Redirect("FrmNivel6.aspx");
                    }
                    else
                    {

                        Response.Redirect("FrmNivel5.aspx", true);
                      

                    }
                }


                //Aqui validar el envio a la base de datos de cuantas preguntas fueron correctas


            }
            valor.InnerText = Convert.ToString((int)ViewState["pregunta"]);
          
        }

        protected void btnRegresar_Click(object sender, EventArgs e)
        {
            if ((int)ViewState["pregunta"] == 0)
            {


            }
            else if ((int)ViewState["pregunta"] == 1)
            {
                introduccion.Visible = true;
                preguntaUno.Visible = false;
                ViewState["pregunta"] = (int)ViewState["pregunta"] - 1;
            }
            else if ((int)ViewState["pregunta"] == 2)
            {
                preguntaDos.Visible = false;
                preguntaUno.Visible = true;
                ViewState["pregunta"] = (int)ViewState["pregunta"] - 1;
            }
            else if ((int)ViewState["pregunta"] == 3)
            {
                preguntaTres.Visible = false;
                preguntaDos.Visible = true;
                ViewState["pregunta"] = (int)ViewState["pregunta"] - 1;
            }
            else if ((int)ViewState["pregunta"] == 4)
            {
                preguntaCuatro.Visible = false;
                preguntaTres.Visible = true;
                ViewState["pregunta"] = (int)ViewState["pregunta"] - 1;
            }
            else if ((int)ViewState["pregunta"] == 5)
            {
                preguntaCinco.Visible = false;
                preguntaCuatro.Visible = true;
                //Aqui validar el envio a la base de datos de cuantas preguntas fueron correctas
                ViewState["pregunta"] = (int)ViewState["pregunta"] - 1;
                ViewState["validar"] = (int)ViewState["validar"] + 1;
            }
            valor.InnerText = Convert.ToString((int)ViewState["pregunta"]);
        }

        protected void btnCerrarSesion_Click(object sender, EventArgs e)
        {
            Session["usuario"] = string.Empty;
            Session["password"] = string.Empty;
            Response.Redirect("https://localhost:44300/FrmHome.aspx");
        }

        protected void btnVermas_Click(object sender, EventArgs e)
        {
            verMas.Visible = true;
       
        }

        protected void btnRestablecerImagenes_Click(object sender, EventArgs e)
        {
           
            cajaOrigen07.InnerHtml = "  <img src='../img/Vector9.png' id='imagenOrigen07' />  <hr/>    <img src='../img/Vector12.png' id='imagenOrigen08' />     <hr /> <img src ='../img/Vector16.png' id='imagenOrigen09' /> ";
            cajaDestino07.InnerHtml = "Campo Vacío";
            cajaDestino08.InnerHtml = "Campo Vacío";
            cajaDestino09.InnerHtml = "Campo Vacío";


        }

       
    }
}