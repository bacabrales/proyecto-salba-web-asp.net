﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrmNivel1.aspx.cs" Inherits="SALBA_WEB_Proyecto_2019.FrmNivel1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>SALBA - Nivel 1</title>
    <link rel="stylesheet" href="../css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../css/estilosPropios.css"/>
    <link href="../css/estilos.css" rel="stylesheet" />
    <link rel="stylesheet" href="../css/main.css"/>
    <link rel="icon" href="../img/logoSena.png"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" />
    <script src="https://kit.fontawesome.com/a5cc566dc4.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"/>
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web&display=swap" rel="stylesheet"/>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <link href="../css/Style.css" rel="stylesheet" />
    <link href="../css/colorEtiquetaB.css" rel="stylesheet" />
</head>
<body>
    <form id="frmPreguntaNivel1" runat="server" style="height: 100%; width: 100%">
        <nav class="navbar navbar-expand-sm navbar-light bg-light bg-Blanco navbar-expand-md shadow-sm fixed-top">
            <div class="container">

                <a class="navbar-brand prueba" href="#">
                    <img src="../img/LogoSalba.png" width="130" class="img-fluid" alt="" /></a>
                <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="collapsibleNavId">
                    <div class="navbar-nav ml-auto mt-2 mt-lg-0">
                        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                            <li class="nav-item active">
                                <a href="#" class="btn  mr-1 text-primary enlace">Inicio</a>
                            </li>
                            <li class="nav-item active">
                                <a href="#" class="btn  mr-1 text-primary enlace">Scores</a>
                            </li>
                            <li class="nav-item">
                                <a href="#" style="" class="btn text-black ml-1 enlace"><i class="fas fa-user-circle mr-2"></i><b id="nombreUsuario" runat="server"></b></a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-cog"></i>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#">Configurar Perfil</a>
                                    <a class="dropdown-item" href="#">Politicas</a>
                                    <div class="dropdown-divider"></div>
                                    <asp:Button ID="btnCerrarSesion" CssClass="dropdown-item" runat="server" Text="Cerrar sesión" OnClick="btnCerrarSesion_Click" />
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>



        <div id="preguntaUno" class="row contenedor-barra1 " runat="server" visible="true">
        <div class="col-lg-4 contenedor-barra">
            <div class="columna-info">
                <div class=" mt-3">
                    <div class="col bg-alert alert-success">
                        <!--Titulo del Nivel-->
                        <h2 class="py-2"><strong>Nivel uno</strong> </h2>
                    </div>
                </div>
                <div class="">
                    <div class="">
                        <!--Contenido del nivel informacion referente-->
                        <p class="text-justify mt-3">Introduccion al nivel uno</p>
                        <p>Este nivel especifica el concepto de variables y sus diferentes tipos. Al tener la claridad del tema, se realiza un test evaluativo como una actividad de aprehensión de los puntos tratados. Se aprueba con el 80% de respuestas correctas, de lo contrario, el nivel debe ser repetido. </p>
                        <h4><strong>¿Qué es un variable?</strong></h4>
                        <hr/>
                        <p>
                            En programación, una variable es un espacio de memoria reservado para almacenar un valor determinado que corresponde a un tipo de dato soportado por el lenguaje de programación en el cual se trabaja.
                    Una variable es representada y usada a través de una etiqueta (un nombre simbólico) que le asigna un programador o que ya viene predefinida en el lenguaje.
                            <br />
                            El programador emplea ese nombre de variable para poder usar la información que está contenida en ella. Durante el tiempo de ejecución del programa la variable puede adquirir un valor determinado y puede cambiar durante el curso de ejecución del mismo.
                            <br />
                            Una variable en programación no es lo mismo que una variable en matemática.
                            <br />
                            Usualmente el nombre que se le da a una variable es largo (pero no demasiado) y descriptivo, permitiendo al programador recordar que contiene.
                    <a href=" http://www.alegsa.com.ar/Dic/variable.php">Fuente</a>
                        </p>
                        <h5><strong>Ejemplo</strong></h5>
                        <p>
                            Ejemplo en <b>LPP</b> se declara una o más variable en este caso de (tipo entero) y cómo asignar un valor a una variable en de esta manera:
                    <br/>
                            <b>Recomendaciones</b> no pueden llevar números ni símbolos especiales al declarar la variable.
                        </p>
                        <pre class="layer">&nbsp;</pre>
                        <textarea class="highlight" name="highlight">
INICIO
--/*Declarando variables de tipo entero
ENTERO nro1, nro2

--/*Asignandoles un valor númerico1
    nro1 <- 20
FIN
                </textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <!--contenido a evaluar o preguntar-->
            <div class="row" id="informacionUno">
                <div class="col">
                    <h2 class="text-center">Lee toda la información <i class="fas fa-info-circle info-in"></i></h2>
                    <hr/>
                </div>
            </div>
            <div class="row mt-3" id="ejercicioUno">
                <div class="col-12">
                    <h3>Ejercicio practico</h3>
                    <hr/>
                    <p>Declare el tipo de la variable llamada “nroDeAutos” de tipo decimal y asígnale el tipo de la variable nro1,nro2 de tipo numeros completos<br/> Arrastre los elementos en el campo correcto!</p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p><b id="dropzone1" runat="server">________</b>nro1, nro2</p>
                    <p><b id="dropzone2" runat="server">________</b>nroDeAutos</p>
                    <p><b>INICIO</b></p>
                    <p class="text-danger"><b>ESCRIBA</b>"Asigna un valor númerico"</p>
                    <p><b>LEER</b> nro1</p>
                    <p><b>LEER</b> nro2</p>
                    <p><b>FIN</b></p>
                    <hr />
                    <hr />
                    <div><b  id="winston1">ENTERO</b></div>
                    <input type="text" value="0" id="txtValidarIntroA" runat="server" hidden="hidden"/>
                    <br />
                    <div><b id="winston2">REAL</b></div>
                    <input type="text" value="0" id="txtValidarIntroB" runat="server" hidden="hidden"/>
                </div>
            </div>
        </div>
    </div>

        <div id="preguntaDos" class="row contenedor-barra1 " runat="server" visible="false">
        <div class="col-lg-4 contenedor-barra">
            <div class="columna-info">
                <div class=" mt-3">
                    <div class="col bg-alert alert-success">
                        <!--Titulo del Nivel-->
                        <h2 class="py-2"><strong>Nivel uno</strong> </h2>
                    </div>
                </div>
                <div class="">
                    <div class="">
                        <!--Contenido del nivel informacion referente-->
                        <p class="text-justify mt-3">Operadoress</p>
                        <h4><strong>¿Qué son los operadores?</strong></h4>
                        <p>
                            Los operadores son símbolos que indican cómo se deben manipular los operandos. Los operadores junto con los operandos forman una expresión, que es una fórmula que define el cálculo de un valor.
                        </p>
                        <h5><strong>Ejemplo</strong></h5>
                        <p>
                            Ejemplo en  <b>LPP</b>  se declaran uno o más operadores de esta manera:
                        </p>
                        <pre class="layer">&nbsp;</pre>
                        <textarea class="highlight" name="highlight">
--/*Algunos operadores...


INICIO
    ()  --/* Agrupar expresiones
    ^   --/* Operador para exponenciación
    *   --/* Operador de multiplicación
    +   --/* Operador de suma
    -   --/* Operador de Resta
    /   --/* Operador de división
    mod --/* Operador de cáculo de residuo

FIN

                </textarea>

                        <h4><strong>¿Qué son los operadores lógicos?</strong></h4>
                        <hr />
                        <p>
                            Operadores lógicos. Muy utilizados en Informática, Lógica proposicional y Álgebra booleana, entre otras disciplinas. Los operadores lógicos nos proporcionan un resultado a partir de que se cumpla o no una cierta condición, producen un resultado booleano, y sus operandos son también valores lógicos o asimilables a ellos (los valores numéricos son asimilados a cierto o falso según su valor sea cero o distinto de cero). Esto genera una serie de valores que, en los casos más sencillos, pueden ser parametrizados con los valores numéricos 0 y 1. La combinación de dos o más operadores lógicos conforma una función lógica.
                        </p>
                        <pre class="layer">&nbsp;</pre>
                        <textarea class="highlight">

INICIO
    O Operador lógico
    Y Operador lógico
FIN

                </textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <!--contenido a evaluar o preguntar-->
            <div class="row">
                <div class="col">
                    <h2 class="text-center">Lee toda la información <i class="fas fa-info-circle info-in"></i></h2>
                    <hr />
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-12">
                    <h3>Ejercicio practico</h3>
                    <hr />
                    <p>Se necesita un algoritmo que sume dos números enteros declare las variables "nro1","nro2" y "resultado asignale el valor de 6 y 2"</p>
                    <br />
                    <hr />
                    <p><b>ENTERO</b> nro1, nro2, resultado</p>
                    <p><b>INICIO</b></p>
                    <p class="text-danger"><b>ESCRIBA</b>"Digite el primer número"</p>
                    <p>
                        <asp:TextBox ID="txtValidarUno" CssClass="w-25" runat="server"></asp:TextBox>
                        <- 6</p>
                    <p><b>ESCRIBA</b>"Digite el segundo número"</p>
                    <p>nro2 <- 2</p>
                    <p>
                        <asp:TextBox ID="txtValidarDos" CssClass="w-25" runat="server"></asp:TextBox>
                        <- nro1 +    nro2</p>
                    <p><b>FIN</b></p>

                    <hr />
                    <p class="text-muted">"Se debe ingresar en la caja de texto los mismo valores para evitar validaciones erroneas"</p>
                    <b>nro1</b>
                    <br />
                    <b>resultado</b>
                    <hr />
                </div>
            </div>
        </div>
    </div>

        <div class="row contenedor-barra1 " id="preguntaTres" runat="server" visible="false">
        <div class="col-lg-4 contenedor-barra">
            <div class="columna-info">
                <div class=" mt-3">
                    <div class="col bg-alert alert-success">
                        <!--Titulo del Nivel-->
                        <h2 class="py-2"><strong>Nivel uno</strong> </h2>
                    </div>
                </div>
                <div class="">
                    <div class="">
                        <!--Contenido del nivel informacion referente-->
                        <p class="text-justify mt-3">Tipos de datos en programación</p>
                        <h4><strong>¿Qué son los tipos de datos?</strong></h4>
                        <p>
                            Un tipo de datos es la propiedad de un valor que determina su dominio (qué valores puede tomar), qué operaciones se le pueden aplicar y cómo es representado internamente por el computador.
                            <br />
                            Todos los valores que aparecen en un programa tienen un tipo, en lenguajes de programación un tipo de dato es un atributo de una parte de los datos que indica al ordenador y al programador algo sobre la clase de datos sobre los que se va a procesar.
                    <a href=" http://www.alegsa.com.ar/Dic/variable.php">Fuente</a>
                        </p>
                        <h5><strong>Tipo de dato entero</strong></h5>
                        <p>
                            Una variable de tipo entero, representa un conjunto finito de números enteros. Este conjunto tiene un mínimo y un máximo que están determinados por el tipo de dato y por el lenguaje de programación que se utilice. Los valores que puede tomar un entero son todos  los números: ... -3, -2, -1, 0, 1, 2, 3
                        </p>
                        <pre class="layer">&nbsp;</pre>
                        <textarea class="highlight" name="highlight">
--/*Declarando variables de tipo Entero
ENTERO nro1, nro2
                </textarea>
                        <h5><strong>Tipo de dato Real</strong></h5>
                        <p>
                            El formato de dato del tipo “Real” se aplica a los números con decimales.
                        </p>
                        <pre class="layer">&nbsp;</pre>
                        <textarea class="highlight" name="highlight">
--/*Declarando variables de tipo Real
REAL real1, real2
                </textarea>
                        <h5><strong>Tipo de dato Cadena</strong></h5>
                        <p>
                            El formato de dato del tipo “Cadena” representa un texto.
                            <br />
                            <b>Recomendaciones</b> para declarar un tipo de dato cadena hay que especificar el numero de caracteres que se almacenarán en memoria
                        </p>
                        <pre class="layer">&nbsp;</pre>
                        <textarea class="highlight" name="highlight">
--/*Declarando variables de tipo Cadena
CADENA cadena1[20], cadena2[30]
                </textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <!--contenido a evaluar o preguntar-->
            <div class="row">
                <div class="col">
                    <h2 class="text-center">Lee toda la información <i class="fas fa-info-circle info-in"></i></h2>
                    <hr />
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-12">
                    <h3>Ejercicio practico</h3>
                    <hr />
                    <p>Se necesita un algoritmo que se reciba el nombre de la persona <b>"Camilo Vasquez"</b> , la edad <b>"20"</b> y su estatura <b>"1.75"</b> Declare variables de <b>"nombrePersona"</b>,<b>"edadDePersona"</b>,<b>"estaturaPersona"</b> y asignales su respectivo valor<br />
                        Completa los espacios</p>
                    <br />
                    <br />
                    <p><b id="dropzone3" runat="server">_________</b> edadDePersona</p>
                    <p><b id="dropzone4" runat="server">_________</b> nombrePersona</p>
                    <p><b id="dropzone5" runat="server">_________</b> estaturaPersona</p>
                    <p><b>INICIO</b></p>
                    <p class="text-danger"><b>ESCRIBA</b>"¿Cúal es el nombre?"</p>
                    <p>nombrePersona <- "Camilo Vasquez"</p>
                    <p class="text-danger"><b>ESCRIBA</b>"¿Cúal es la edad?"</p>
                    <p>edadDePersona <- 20</p>
                    <p class="text-danger"><b>ESCRIBA</b>"¿Cúal es la estatura?"</p>
                    <p>estaturaPersona <- 1.70</p>
                    <p><b>FIN</b></p>
                    <hr />
                    <p class="text-muted">"Arraste a la caja de texto la respuesta correcta"</p>
                    <div><b id="winston3">ENTERO</b></div>
                    <div><b id="winston4">CADENA[60]</b></div>
                    <div><b id="winston5">REAL</b></div>
                    <input type="text" id="txtValiPeg3A" value="0" runat="server" hidden="hidden"/>
                    <input type="text" id="txtValiPeg3B" value="0" runat="server" hidden="hidden"/>
                    <input type="text" id="txtValiPeg3C" value="0" runat="server" hidden="hidden"/>
                    <hr />
                </div>
            </div>
        </div>
    </div>

        <div class="row contenedor-barra1 " id="preguntaCuatro" runat="server" visible="false">
            <div class="col-lg-4 contenedor-barra">
                <div class="columna-info">
                    <div class=" mt-3">
                        <div class="col bg-alert alert-success">
                            <!--Titulo del Nivel-->
                            <h2 class="py-2"><strong>Pregunta cuatro</strong> </h2>
                        </div>
                    </div>
                    <div class="">
                        <div class="">
                            <!--Contenido del nivel informacion referente-->
                            <p class="text-justify mt-3">Introduccion a los procedimientos</p>
                            <h4><strong>¿Qué es un procedimiento?</strong></h4>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Excepturi autem rem unde fugiat dicta cumque, dolorum est quo dignissimos, repellendus expedita mollitia. Vitae vero voluptatibus quis consectetur dicta blanditiis quibusdam!</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <!--contenido a evaluar o preguntar-->
            </div>
        </div>

        <div class="row contenedor-barra1 " id="preguntaCinco" runat="server" visible="false">
        <div class="col-lg-4 contenedor-barra">
            <div class="columna-info">
                <div class=" mt-3">
                    <div class="col bg-alert alert-success">
                        <!--Titulo del Nivel-->
                        <h2 class="py-2"><strong>Nivel uno</strong> </h2>
                    </div>
                </div>
                <div class="">
                    <div class="">
                        <!--Contenido del nivel informacion referente-->
                        <p class="text-justify mt-3">Test</p>
                        <h4><strong>Contesta las siguientes preguntas</strong></h4>
                        <hr />
                        <p class="text-success">Pregunta Uno</p>
                        <p><b>1. ¿Qué es una variable?</b></p>
                        <p class="text-success">Pregunta Dos</p>
                        <p><b>2. ¿Qué es un tipo de dato?</b></p>
                        <p class="text-success">Pregunta Tres</p>
                        <p><b>3. ¿Cómo Definés una variable llamada "nroDeAutos" como tipo entero?</b></p>
                        <p class="text-success">Pregunta Tres</p>
                        <p><b>4. Define un algoritmo que reste dos variables de tipo entero "nro1", "nro2" asígnales el valor de 10 y 4 y almacene el valor en una variable llamada "respuesta"</b></p>
                        <p><b>Recomendaciones:</b>  Se necesita declarar las variables y cada vez que se asigné un valor debe llevar la palabra ESCRIBA "Digite un número".</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="row mt-3">
                <div class="col-12">
                    <h4>Respuesta pregunta uno</h4>
                    <hr class="bg-success" />
                    <div class="">
                        <div class="custom-control custom-radio">
                            <input runat="server" type="radio" id="customRadio1" name="customRadio" class="custom-control-input" />
                            <label class="custom-control-label" for="customRadio1"><b>A.</b> Es un espacio almacenado en memoria que puede variar según la ejecución del algoritmo.</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input runat="server" type="radio" id="customRadio2" name="customRadio" class="custom-control-input" />
                            <label class="custom-control-label" for="customRadio2"><b>B.</b> Es una característica o cualidad de un individuo que está propenso a adquirir diferentes valores</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input runat="server" type="radio" id="customRadio3" name="customRadio" class="custom-control-input" />
                            <label class="custom-control-label" for="customRadio3"><b>C.</b> Es una propiedad de cualquier algoritmo</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input runat="server" type="radio" id="customRadio4" name="customRadio" class="custom-control-input" />
                            <label class="custom-control-label" for="customRadio4"><b>D.</b> Es una característica podrá adquirir cualquier valor mientras se encuentre dentro de un intervalo de valores determinado.</label>
                        </div>
                    </div>
                    <hr />
                    <h4>Respuesta pregunta Dos</h4>
                    <hr class="bg-success" />
                    <div class="">
                        <div class="custom-control custom-radio" style="display: block !important">
                            <input runat="server" type="radio" id="customRadio5" name="customRadio1" class="custom-control-input"/>
                            <label class="custom-control-label" for="customRadio5"><b>A.</b> Es una variable definida como entero. </label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input runat="server" type="radio" id="customRadio6" name="customRadio1" class="custom-control-input" />
                            <label class="custom-control-label" for="customRadio6"><b>B.</b> Es una palabra que se le coloca al declarar una variable en lpp</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input runat="server" type="radio" id="customRadio7" name="customRadio1" class="custom-control-input" />
                            <label class="custom-control-label" for="customRadio7"><b>C.</b> Es un atributo de algun valor que define qué operaciones se le pueden aplicar a un dato y cómo es representado internamente por el computador</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input runat="server" type="radio" id="customRadio8" name="customRadio1" class="custom-control-input" />
                            <label class="custom-control-label" for="customRadio8"><b>D.</b> Es una variable definida como entero, cadena, real etc.</label>
                        </div>
                        <hr />
                    </div>

                    <div class="row">
                        <hr />
                        <p class="text-muted">"Al momento de validar la información se requiere que el usuario, en vez de usar las comillas dobles (""), use las comillas sencillas ('')"</p>
                        <hr />
                        <div class="col-lg-6">
                            <h4>Respuesta pregunta Tres</h4>
                            <hr class="bg-success" />
                            <textarea runat="server" id="txtAreaUno" class="form-control" cols="50" rows="3"></textarea>
                        </div>
                        <div class="col-lg-6">
                            <h4>Respuesta pregunta Cuatro</h4>
                            <hr class="bg-success" />
                            <textarea runat="server" id="txtAreaDos" class="form-control" cols="50" rows="5"></textarea>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                </div>
                <!--col-->
            </div>
        </div>
        <!--col-plan-->
    </div>

        <footer id="footerPrincipal" class="bg-dark" runat="server">
            <div class="container bg-dark">
                <div class="row bg-dark p-2 text-center justify-content-center align-items-center">
                    <div class="col-2 col-lg-4">
                        <asp:Button CssClass="btn btn-outline-light" ID="btnRegresar" runat="server" Text="Atrás" OnClick="btnRegresar_Click" />
                    </div>
                    <div class="col-2 col-lg-4">
                        <p class="text-white d-block"><span id="valor" runat="server">1</span>/<span>4</span></p>
                    </div>
                    <div class="col-2 col-lg-4">
                        <asp:Button CssClass="btn btn-outline-light" ID="btnEnviar" runat="server" Text="Siguiente" OnClick="btnEnviar_Click" />/>
                    </div>
                </div>
            </div>
        </footer>
    </form>
    


    <script src="../js/jquery-3.3.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/Script.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script src="../js/DropzoneNivel1.js"></script>
</body>
</html>
