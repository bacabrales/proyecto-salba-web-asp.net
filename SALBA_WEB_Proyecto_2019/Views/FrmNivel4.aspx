﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrmNivel4.aspx.cs" Inherits="SALBA_WEB_Proyecto_2019.FrmNivel4" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>SALBA - Nivel 4</title>
    <link rel="stylesheet" href="../css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../css/estilosPropios.css"/>
    <link href="../css/estilos.css" rel="stylesheet" />
    <link rel="stylesheet" href="../css/main.css"/>
    <link rel="icon" href="../img/logoSena.png"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" />
    <script src="https://kit.fontawesome.com/a5cc566dc4.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"/>
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web&display=swap" rel="stylesheet"/>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
</head>
<body>
    <form id="frmPreguntaNivel4" runat="server" style="height: 100%; width: 100%">
        <nav class="navbar navbar-expand-sm navbar-light bg-light bg-Blanco navbar-expand-md shadow-sm fixed-top">
            <div class="container">

                <a class="navbar-brand prueba" href="#">
                    <img src="../img/LogoSalba.png" width="130" class="img-fluid" alt="" /></a>
                <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="collapsibleNavId">
                    <div class="navbar-nav ml-auto mt-2 mt-lg-0">
                        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                            <li class="nav-item active">
                                <a href="#" class="btn  mr-1 text-primary enlace">Inicio</a>
                            </li>
                            <li class="nav-item active">
                                <a href="#" class="btn  mr-1 text-primary enlace">Scores</a>
                            </li>
                            <li class="nav-item">
                                <a href="#" style="" class="btn text-black ml-1 enlace"><i class="fas fa-user-circle mr-2"></i><b id="nombreUsuario" runat="server"></b></a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-cog"></i>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#">Configurar Perfil</a>
                                    <a class="dropdown-item" href="#">Politicas</a>
                                    <div class="dropdown-divider"></div>
                                    <asp:Button ID="btnCerrarSesion" CssClass="dropdown-item" runat="server" Text="Cerrar sesión" OnClick="btnCerrarSesion_Click" />
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
      

<%-- ///////////////////////////////Pregunta uno/////////////////////////////////////// --%>
        <div id="preguntaUno" class="row contenedor-barra1 " runat="server" visible="true">
                <div class="col-lg-4 contenedor-barra">
                    <div class="columna-info">
                        <div class=" mt-3">
                            <div class="col bg-alert alert-success">
                                <!--Titulo del Nivel-->
                                <h2 class="py-2"><strong>Procedimientos y funciones</strong> </h2>
                            </div>
                        </div>
                        <div class="">
                            <div class="">
                                <!--Contenido del nivel informacion referente-->
                                    <p class="text-justify mt-3">Introduccion a los procedimientos</p>
                            <h4><strong>¿Qué es un procedimiento o función?</strong></h4>
                                  <br />
                        <p class="pl-2 pr-2 text-justify">Introducción a <strong>Procedimientos</strong>y<strong>Funciones</strong></p>
                        <h2 class="pl-2 pr-2 text-justify">Procedimientos y Funciones</h2>
                        <p class="pl-2 pr-2 text-justify">
                          La resolución de problemas complejos se facilita considerablemente si se dividen en problemas mas pequeños; y la resolución de estos subproblemas se realiza mediante subalgoritmos, (<strong>img1</strong> ).
                        </p>
                        <p class="pl-2 pr-2 text-justify">
                          Los subalgoritmos son unidades de programa o módulos que están diseñados para ejecutar laguna tarea específica. Éstos, constituidos por funciones o procedimientos, se escriben solamente una vez, pero pueden ser referenciados en diferentes puntos del programa, de modo que se puede evitar la duplicación innecesaria del código, (<strong>img2</strong>).
                        </p>
                        <p class="pl-2 pr-2 text-justify">
                          El módulo principal se ejecuta en una primera instancia, que da la orden de inicio de ejecución de los subprogramas. Puede ser ejecutado n veces. Es importante saber que datos se van a compartir entre los programas,(<strong>img3</strong>).
                        </p>
                         <p class="pl-2 pr-2 text-justify">
                          El subprograma es un programa en sí mismo, ejecutado por la solicitud del programa principal o de otro subprograma, una n cantidad de veces. Cuando realiza la solicitud, el programa se detiene hasta que el subprograma deja de realizar su tarea, luego continúa; esto se conoce como control de ejecución.
                        </p>
                        <h2 class="pl-2 pr-2 text-justify"><strong>Procedimientos</strong></h2>
                        <p class="pl-2 pr-2 text-justify">
                            Un procedimiento es un subprograma que ejecuta una tarea determinada. Está compuesto por un conjunto de sentencias, a las que s le asigna un nombre, o identificador. Constituyen unidades del programa, y su tarea se ejecuta siempre y cuando encuentre el nombre que se le asignó a dicho procedimiento.
                        </p>
                        <p class="pl-2 pr-2 text-justify">
                            Los procedimientos deben ser declarados obligatoriamente antes de que puedan ser llamados en el cuerpo del programa principal. Para ser activados o ejecutados, deben ser llamados desde el programa en que fueron declarados.
                        </p>
                        <p class="pl-2 pr-2 text-justify">
                            Pueden recibir cero o mas valores del programa principal que lo llama y lo activa, y devolver cero o mas valores a dicho programa llamador.
                        </p>
                        <p class="pl-2 pr-2 text-justify">
                            Todo procedimiento, al igual que un programa principal, consta de una cabecera, que proporciona su nombre y sus parámetros de comunicación; de una sección de declaraciones locales y el cuerpo de sentencias ejecutables. Las ventajas mas destacables de usar procedimientos son:
                        </p>
                        <p class="pl-2 pr-2 text-justify">
1. Facilitan el diseño top-down.
                        </p>
                        <p class="pl-2 pr-2 text-justify">
2. Se pueden ejecutar mas de una vez en un programa, con solo llamarlos las veces que así desee. Con esto se ahorra tiempo de programación.
                        </p>
                        <p class="pl-2 pr-2 text-justify">
                            3. El mismo procedimiento se puede usar en distintos programas.
                        </p>
                        <p class="pl-2 pr-2 text-justify">
                            4. Su uso facilita la división de tareas entre los programadores de un equipo.
                        </p>
                        <p class="pl-2 pr-2 text-justify">
                            5. Se pueden probar individualmente e incorporarlos en librerías o bibliotecas.
                        </p>
                        <h2 class="pl-2 pr-2 text-justify"><strong>Funciones</strong></h2>
                        <p class="pl-2 pr-2 text-justify">
                            Una función es un subprograma que recibe, como argumentos o parámetros, datos de tipo numérico o no numérico, y devuelve un único resultado.
Las funciones incorporadas al sistema se denominan funciones internas, o intrínsecas; las funciones definidas por el usuario se llaman funciones externas.
El algoritmo o programa invoca la función con el nombre de esta última en una expresión seguida de una lista de argumentos que deben coincidir en cantidad, tipo y orden con los de la función que fue definida.
                        </p>
                        <p class="pl-2 pr-2 text-justify">
                            Cada vez que se llama a una función desde el algoritmo principal, se establece automáticamente una correspondencia entre los parámetros formales y los reales. Debe haber exactamente el mismo número de parámetros reales que de formales en la declaración de la función, y se presupone una correspondencia uno a uno de izquierda a derecha entre los parámetros formales y reales.
                        </p>
                        <p class="pl-2 pr-2 text-justify">
                            Una llamada a una función implica los siguientes pasos:
                        </p>
                        <p class="pl-2 pr-2 text-justify">
1. A cada parámetro formal se le asigna el valor real de su correspondiente parámetro actual (cabe destacar que digo “real” refiriéndome al valor verdadero con el cual va a trabajar el subprograma, y no al tipo de dato).
                        </p>
                        <p class="pl-2 pr-2 text-justify">
                            2. Se ejecuta el cuerpo de acciones de la función.
                        </p>
                        <p class="pl-2 pr-2 text-justify">
                            3. Se devuelve el valor de la función al nombre de la función y se retorna al punto de llamada.
                        </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    
                    <!--contenido a evaluar o preguntar-->
                    <div class="text-left my-5" >
                        <img src="../img/img1.jpg" alt="Alternate Text" title="img1"/>
                        </div>  
                        <br />
                        <div class="text-center">
                        <img src="../img/img2.jpg" alt="Alternate Text" height:"110px;" title="img2"/>
                        </div>
                        <br />
                        <div class="text-right">
                         <img src="../img/img3.jpg" alt="Alternate Text" title="img3"/>
                        </div>  
                </div>
           
        </div>

<%-- ///////////////////////////////Pregunta dos////////////////////////////////////// --%>
        <div id="preguntaDos" class="row contenedor-barra1 " runat="server" visible="false">
                <div class="col-lg-4 contenedor-barra">
                    <div class="columna-info">
                        <div class=" mt-3">
                            <div class="col bg-alert alert-success">
                                <!--Titulo del Nivel-->
                                <h2 class="py-2"><strong>Parámetro de valor</strong> </h2>
                            </div>
                        </div>
                        <div class="">
                            <div class="">
                                <!--Contenido del nivel informacion referente-->
                                <p class="text-justify mt-3">Introduccion a los procedimientos</p>
                                      
                            <p align="justify">Este tipo de parámetro se le conoce con el nombre de parámetro de valor , que
                 esta por omisión , este tipo de parámetros aunque durante el procedimiento su
                 valor cambie el valor no será asignado a la variable del programa principal , por
                 ejemplo si la variable numero del programa que presentamos abajo se le asigna
                 otro valor diferente al 10 , este cambio no se reflejaría en la variable num , y por
                 esto en el programa principal , es este tipo de parámetros que se le conoce
                 como parámetros de valor. <strong> click en el botón</strong> procedimiento para ver un ejemplo de procedimiento con parámetros. </p>
                            <h4>Sintaxis</h4>
                            <b class="text-info">Procedimiento nombre_del_procedimiento [( parámetros )]</b>
                                <br /> 
                               <b class="text-info">[variables locales]</b> 
                                <br />
                                <b class="text-info">inicio</b>
                                <br />
                                <b class="text-info">instrucciones</b>
                                <br />
                                
                                <b class="text-info my-5">fin</b>
                                
                                <hr />
                                <a type="button" runat="server" id="write" class="btn btn-outline-success border-bottom-0">Ejemplo procedimiento</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8" id="text">
                    <!--contenido a evaluar o preguntar-->
                </div>
        </div>

<%-- ///////////////////////////////Pregunta tres///////////////////////////////////// --%>
        <div class="row contenedor-barra1 " id="preguntaTres" runat="server" visible="false">
            <div class="col-lg-4 contenedor-barra">
                <div class="columna-info">
                    <div class=" mt-3">
                        <div class="col bg-alert alert-success">
                            <!--Titulo del Nivel-->
                       <h2 class="py-2"><strong>Observa con atención</strong> </h2>
                        </div>
                    </div>
                    <div class="">
                        <div class="my-3 ml-5">
                            <!--Contenido del nivel informacion referente-->
                            <br />
                            <br />
  <b style="color:blue"> Cadena  </b><b style="color:brown">  [25] </b><b style="color:black">nombre</b>
                            <br />
                          
                            <b style="color:blue"> Procedimiento  </b><b style="color:black">  asterisco</b>
                            <b style="color:blue"> Entero  </b><b> I<br />
                            <br />
                            </b><b style="color:blue">Inicio</b>
                            <br />
                            
                            <b style="color:blue" class="ml-4"> Para  </b><b> i <- 1</b> <b style="color:blue">hasta</b><b> 5 </b><b style="color:blue">haga</b>
                            <br />
                            
                            <b style="color:blue" class="ml-5">Escriba</b> <b style="color:maroon">"*"</b>
                            <br />
                            
                            <b style="color:blue" class="ml-4">Fin para</b>
                             <br />
                           
                            <b style="color:blue">Fin</b>
                            <br /> <br />
                            
                           
                             <b style="color:blue" class="mt-4">Inicio</b>
                            <br />
                            
                            <b style="color:blue" class="ml-4"> Escriba  </b> <b style="color:maroon">"Ingresa el nombre...:"</b>
                            <br />
                            
                            <b  style="color:blue" class="ml-4"> Lea  </b><b> nombre </b>
                            <br />
                            
                            <b  style="color:blue" class="ml-4"> Llamar  </b><b>asterisco</b>
                            <br />
                            
                            <b  style="color:blue" class="ml-4"> Llamar nueva_linea</b>
                            <br />
                            
                            <b  style="color:blue" class="ml-4"> Escriba  </b><b> nombre </b>
                            <br />
                            
                            <b  style="color:blue" class="ml-4"> Llamar nueva_linea</b>
                            <br />
                            
                            <b  style="color:blue" class="ml-4"> Llamar  </b><b>asterisco</b>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 my-5">
                <!--contenido a evaluar o preguntar-->
                 <h1 class="text-center">¿Qué hace falta en el código?</h1>
                    <div class=" my-4 ml-4  ">
                        <div >
                            <asp:RadioButton ID="rd1" runat="server" GroupName="preguntacuatro" text="A) Llamar el procedimiento"/>
                        </div>
                        <div class="mt-4">
                            <asp:RadioButton ID="rd2" runat="server" GroupName="preguntacuatro" text="B) Crear una variable global"/>
                        </div>
                        <div class="mt-4">
                            <asp:RadioButton ID="rd3" runat="server" GroupName="preguntacuatro" text="C) Colocar el fin"/>
                        </div>
                        <div class="mt-4">
                            <asp:RadioButton ID="rd" runat="server" GroupName="preguntacuatro" text="D) Declarar bien la cadena"/>
                        </div> 
                        <div class="text-right mx-4">
                            <img src="../img/Pensando.jpg" height="350px"/>
                        </div>
                        
                   </div>  
                
            </div>
        </div>

<%-- ///////////////////////////////Pregunta cuatro//////////////////////////////////// --%>
        <div class="row contenedor-barra1 " id="preguntaCuatro" runat="server" visible="false">
            <div class="col-lg-4 contenedor-barra">
                <div class="columna-info">
                    <div class=" mt-3">
                        <div class="col bg-alert alert-success">
                            <!--Titulo del Nivel-->
                            <h2 class="py-2"><strong>Observa y responde</strong> </h2>
                        </div>
                    </div>
                    <div class="">
                        <div class="">
                            <!--Contenido del nivel informacion referente-->
     <p class="text-justify mt-3">Introducción a los procedimientos</p>
                <p align="justify"> En el  código hace  falta una palabra reservada importante. ¿Cuál es?</p>

              <h4><strong>Recuerda : </strong></h4>
                 <p align="justify">En el programa anterio usamos un subprograma (procedimiento ) para imprimir 5
                     asteriscos , si no lo hubiéramos hecho de esta manera donde se encuentra la
                     instrucción Llamar asteriscos tendríamos que escribir el ciclo , y lo haríamos dos
                     veces , de la forma en que lo escribimos es mas estructurado, pues se divide
                     ese proceso en un subprograma, que cuando necesitamos una línea de 5
                     asteriscos solo llamamos el procedimiento.</p> 
              
               <h4><strong>Nota : </strong></h4>
                 <p align="justify" > los procedimientos se llaman con la instrucción  <b style="color: blue;">Llamar</b>.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 my-5">
                <!--contenido a evaluar o preguntar-->
                <div class="my- ml-4">
                            <!--Contenido del nivel informacion referente-->
                                 <b style="color:blue"> Cadena  </b><b style="color:brown">  [25] </b><b style="color:black">nombre</b>
                            <br />
                          
                            <%--<b style="color:blue"> Procedimiento  </b>--%><b style="color:black">  asterisco</b>
                            <b style="color:blue"> Entero  </b><b> I<br />
                            <br />
                            </b><b style="color:blue">Inicio</b>
                            <br />
                            
                            <b style="color:blue" class="ml-4"> Para  </b><b> i <- 1</b> <b style="color:blue">hasta</b><b> 5 </b><b style="color:blue">haga</b>
                            <br />
                            
                            <b style="color:blue" class="ml-5">Escriba</b> <b style="color:maroon">"*"</b>
                            <br />
                            
                            <b style="color:blue" class="ml-4">Fin para</b>
                             <br />
                           
                            <b style="color:blue">Fin</b>
                            <br /> <br />
                            
                           
                             <b style="color:blue" class="mt-4">Inicio</b>
                            <br />
                            
                            <b style="color:blue" class="ml-4"> Escriba  </b> <b style="color:maroon">"Ingresa el nombre...:"</b>
                            <br />
                            
                            <b  style="color:blue" class="ml-4"> Lea  </b><b> nombre </b>
                            <br />
                            
                            <b  style="color:blue" class="ml-4"> Llamar  </b><b>asterisco</b>
                            <br />
                            
                            <b  style="color:blue" class="ml-4"> Llamar nueva_linea</b>
                            <br />
                            
                            <b  style="color:blue" class="ml-4"> Escriba  </b><b> nombre </b>
                            <br />
                            
                            <b  style="color:blue" class="ml-4"> Llamar nueva_linea</b>
                            <br />
                            
                            <b  style="color:blue" class="ml-4"> Llamar  </b><b>asterisco</b>
                        <br />
                        <b style="color:blue">Fin</b>
                        </div>
                   <div class="form-inline justify-content-center my-5">
                                <input type="text" name="name" value="0" hidden="hidden" id="redirec" runat="server"/>

    <div class="form-check mb-2 mr-sm-2">
       <button id="bien" type="submit"  class="btn btn-primary mb-2">procedimiento</button>
    </div>
      <div class="form-check mb-2 mr-sm-2">
      <button id="mal" type="submit" class="btn btn-primary mb-2" >Comienzo</button>
    </div>
      <div class="form-check mb-2 mr-sm-2">
      <button id="mal2" type="submit" class="btn btn-primary mb-2">Proceso</button>
    </div>
   
  </div>
            </div>
        </div>

<%-- ///////////////////////////////Pregunta cinco//////////////////////////////////// --%>
        <div class="row contenedor-barra1 " id="preguntaCinco" runat="server" visible="false">
            <div class="col-lg-4 contenedor-barra">
                <div class="columna-info">
                    <div class=" mt-3">
                        <div class="col bg-alert alert-success">
                            <!--Titulo del Nivel-->
                            <h2 class="py-2"><strong>Complete los campos faltantes.</strong> </h2>
                        </div>
                    </div>
                    <div class="">
                        <div class="">
                            <!--Contenido del nivel informacion referente-->
    <h4><strong>Lee y responde</strong></h4>
                <p align="justify"> Complete los campos faltantes. </p>

              <h4><strong>Recuerda : </strong></h4>
                 <p align="justify">En el ejemplo anterior  la línea llamar asteriscos(10) estamos asignando al parámetro numero de
                 asteriscos el valor de 10 , esto hace que el ciclo recorra 10 veces, luego más
                 abajo del programa en la instrucción llamar asteriscos(num) se paso una
                 variable como parámetro asignando el valor de num a numero , el cúal número
                 en el programa principal tiene un valor de 10 el cual se le asigna a número en el
                 paso del valor de parámetro . </p> 
              
               <h4><strong>Nota : </strong></h4>
                 <p align="justify" > El parámetro debe ser definido con el tipo de dato correspondiente  <b style="color: blue;">entero,real,caracter </b>etc.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 my-5">
                <!--contenido a evaluar o preguntar-->
                       <b style="color:blue"> Cadena  </b><b style="color:brown">  [25] </b><b style="color:black">nombre</b>
                <br />
<b style="color:blue">Entero </b><b style="color:black">  num</b>
                <br />
<b style="color:blue"><asp:TextBox ID="TxtProcedimiento" runat="server"></asp:TextBox></b><b>  salba(Entero repetir)</b>
                <br />
<b style="color:blue">Entero</b> <b style="color:black">I</b>
                <br />
<b style="color:blue">Inicio</b>
                <br />
       <b style="color:blue" class="ml-4"> Para  </b><b> i <- 1</b> <b style="color:blue"><asp:TextBox ID="TxtHasta" runat="server"></asp:TextBox></b><b> 5 </b><b style="color:blue">haga</b>
                <br />
          <b style="color:blue" class="ml-5">Escriba </b><b style="color:brown"> "°/-s-a-l-b-a-\°"</b>
                <br />
     <b style="color:blue" class="ml-4"><asp:TextBox ID="TxtFinPara" runat="server"></asp:TextBox></b>
                <br />
<b style="color:blue">Fin</b>
                <br />
                <br />
<b style="color:blue">Inicio</b>
                <br />
      <b style="color:black" class="ml-4">num <-5</b>
                <br />
     <b style="color:blue" class="ml-4"><asp:TextBox ID="txtEscriba" runat="server"></asp:TextBox></b><b style="color:brown">"Ingresar el nombre ..:"</b>
                <br />
     <b style="color:blue" class="ml-4">Lea</b> <b style="color:black">nombre</b>
                <br />
     <b style="color:blue" class="ml-4"><asp:TextBox ID="txtLlamar" runat="server"></asp:TextBox></b> <b style="color:black">salba(5)</b>
                <br />
     <b style="color:blue" class="ml-4">Llamar</b> <b style="color:black">nueva_linea</b>
                <br />
     <b style="color:blue" class="ml-4">Escriba</b> <b style="color:black">nombre</b>
                <br />
     <b style="color:blue" class="ml-4">Llamar</b> <b style="color:black">nueva_linea</b>
                <br />
     <b style="color:blue" class="ml-4">Llamar</b> <b style="color:black">salba(num)</b>
                <br />
<b style="color:blue"><asp:TextBox ID="txtFin" runat="server"></asp:TextBox></b>  
            </div>
        </div>

<%-- /////////////////////////////////////Pregunta seis/////////////////////////////////////// --%>
        <div class="row contenedor-barra1 " id="preguntaSeis" runat="server" visible="false">
            <div class="col-lg-4 contenedor-barra">
                <div class="columna-info">
                    <div class=" mt-3">
                        <div class="col bg-alert alert-success">
                            <!--Titulo del Nivel-->
                            <h2 class="py-2"><strong>Parámetros de variable</strong> </h2>
                        </div>
                    </div>
                    <div class="">
                        <div class="">
              <!--Contenido del nivel informacion referente--> <p class="text-justify mt-3">Introducción a los procedimientos</p>
              <hr>
             
              <p align="justify">El siguiente programa , nos enseña el uso de los parámetros de variable o referencia, los cuales se les antepone la palabra reservada VAR para indicar que esa variable será un parámetro de referencia o variable, esto nos indica que
              cualquier cambio que sufra la variable del procedimiento , la variable del
              programa principal también lo sufrirá, de esta manera podemos enviar
              información modificarla y envirar resultados al programa principal.<strong> click en el botón</strong> procedimiento para ver un ejemplo de procedimiento con parámetros de variable o referencia.  </p>
                            <hr />
                                <a type="button" runat="server" id="proce3" class="btn btn-outline-success border-bottom-0">Ejemplo procedimiento</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8" id="text">
                <!--contenido a evaluar o preguntar-->
            </div>
        </div>

        <%-- /////////////////////////////////////Pregunta siete/////////////////////////////////////// --%>

            <div class="row contenedor-barra1 " id="preguntaSiete" runat="server" visible="false">
            <div class="col-lg-4 contenedor-barra">
                <div class="columna-info">
                    <div class=" mt-3">
                        <div class="col bg-alert alert-success">
                            <!--Titulo del Nivel-->
                            <h2 class="py-2"><strong>Complete los campos faltantes.</strong> </h2>
                        </div>
                    </div>
                    <div class="">
                        <div class="">
              <!--Contenido del nivel informacion referente--> <p class="text-justify mt-3">Introducción a los procedimientos</p>
              <hr>
             
                           <p class="text-justify mt-3">Introducción a los procedimientos</p>

              <h4><strong>Nota : </strong></h4>
                 <p align="justify"><b>Por valor :</b> significa que la función (o subrutina) recibe sólo una copia del   valor que tiene la variable, o sea que no la puede modificar.</p>
                 <p  align="justify"><b>Por referencia :</b>  significa que se pasa la posición de memoria donde esta guardada la variable, por lo que la función puede saber cuánto vale, pero además puede modificarla de cualquier manera.</p> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8" id="text">
                <!--contenido a evaluar o preguntar-->
                              <b style="color:blue"> Cadena  </b><b style="color:brown">  [25] </b><b style="color:black">nombre</b>
                <br />
<b style="color:blue">Entero </b><b style="color:black">  num</b>
                <br />
<b style="color:blue" id="drog1-4">___________<b>  salba(Entero repetir)</b>
                <br />
<b style="color:blue">Entero</b> <b style="color:black">I</b>
                <br />
<b style="color:blue">Inicio</b>
                <br />
       <b style="color:blue" class="ml-4"> Para  </b><b> i <- 1</b> <b style="color:blue" id="drog2-4">___________</b><b> 5 </b><b style="color:blue">Haga</b>
                <br />
          <b style="color:blue" class="ml-5">Escriba </b><b style="color:brown"> "°/-s-a-l-b-a-\°"</b>
                <br />
     <b style="color:blue" class="ml-4" id="drog3-4">___________</b>
                <br />
<b style="color:blue">Fin</b>
                <br />
                <br />
<b style="color:blue">Inicio</b>
                <br />
      <b style="color:black" class="ml-4">num <-5</b>
                <br />
     <b style="color:blue" class="ml-4" id="drog4-4">___________</b><b style="color:brown">"Ingresar el nombre ..:"</b>
                <br />
     <b style="color:blue" class="ml-4">Lea</b> <b style="color:black">nombre</b>
                <br />
     <b style="color:blue" class="ml-4">Llamar</b> <b style="color:black">salba(5)</b>
                <br />
     <b style="color:blue" class="ml-4">Llamar</b> <b style="color:black">nueva_linea</b>
                <br />
     <b style="color:blue" class="ml-4">Escriba</b> <b style="color:black">nombre</b>
                <br />
     <b style="color:blue" class="ml-4" id="drog5-4">Llamar</b> <b style="color:black">nueva_linea</b>
                <br />
     <b style="color:blue" class="ml-4">Llamar</b> <b style="color:black">salba(num)</b>
                <br />
<b style="color:blue">Fin</b>  
    <hr />
    <div class="form-check mb-2 mr-sm-2">
           <%--<div id="drog1-4"></div>--%>
            <b id="drag1-4">Procedimiento</b>|
           <%-- <div id="drog2-4"></div>--%>
            <b id="drag2-4">Hasta</b>|
           <%-- <div id="drog3-4"></div>--%>
            <b id="drag3-4">Fin Para</b>|
           <%-- <div id="drog4-4"></div>--%>
            <b id="drag4-4">Escriba</b>
        <%-- <div id="drog5-4"></div>--%>
            <%--<b id="drag5-4">Llamar</b>--%>
    <asp:Button ID="btnVer" runat="server" Text="Verificar" class="btn btn-outline-success"/>
    <input type="text" id="validarRedire" value="0" hidden="hidden" runat="server"/>
    </div>
            </div>
        </div>

        <%-- /////////////////////////////////////Pregunta ocho/////////////////////////////////////// --%>

           <div class="row contenedor-barra1 " id="preguntaOcho" runat="server" visible="false">
            <div class="col-lg-4 contenedor-barra">
                <div class="columna-info">
                    <div class=" mt-3">
                        <div class="col bg-alert alert-success">
                            <!--Titulo del Nivel-->
                            <h2 class="py-2"><strong>Funciones</strong> </h2>
                        </div>
                    </div>
                    <div class="">
                        <div class="">
              <!--Contenido del nivel informacion referente-->
                                   <p class="text-justify mt-3">Introducción a las funciones</p>
              <hr>
             
              <h4><strong>Funciones</strong></h4>
              <p align="justify">Las funciones son subprogramas que hacen una o más instrucciones pero que
               siempre nos devuelven un solo valor .</p>

   
                   <section>
                   
              <h4><strong> Sintaxis </strong> </h4>

                  <p style="margin:-0.5% 0;color: blue;  " >funcion nombre_funcion [( parámetros ) ]: tipo_de_retorno</p>
                  <p style="margin:-0.5% 0;color: blue ; margin-left: 45px;">[variables locales]</p>
                  <p style="margin:-0.5% 0;color: blue">inicio</p>
                  <p style="margin:-0.5% 0;color: blue; margin-left: 45px;">instrucciones</p>
                  <p style="margin:-0.5% 0;color: blue; margin-left: 45px;">Retorne valor</p>
                  <p style="margin:-0.5% 0;color: blue">fin</p>    

              </section>
               <br>
              <p align="justify">Si notamos en la sintaxis de la función observamos que esta la palabra retorno
               la cual devuelve un valor que tiene que ser del mismo tipo que fue declarado el
               Tipo_de_retorno.<strong> click en el botón</strong> funcion para ver un ejemplo .</p>
              <br />
                            <hr />
                            <a type="button" runat="server" id="funcion" CssClass="btn btn-outline-success">Ejemplo función</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8" id="text">
                <!--contenido a evaluar o preguntar-->
            
            </div>
        </div>

        <%-- /////////////////////////////////////Pregunta Nueve evaluacion parte 1/////////////////////////////////////// --%>

             <div class="row contenedor-barra1 " id="PreguntaNueve" runat="server" visible="false">
            <div class="col-lg-4 contenedor-barra">
                <div class="columna-info">
                    <div class=" mt-3">
                        <div class="col bg-alert alert-success">
                            <!--Titulo del Nivel-->
                            <h2 class="py-2"><strong>Test final</strong> </h2>
                        </div>
                    </div>
                    <div class="">
                        <div class="">
              <!--Contenido del nivel informacion referente-->
                                          <p>A continuación se encontrará con una serie de preguntas que
                                evaluarán sus conocimientos adquiridos en <strong>Procedimientos</strong> y <strong>Funciones</strong>
                                durante el recorrido en el mundo de la programación.
                            </p>
                            <br />
                            <p>Por favor responda las preguntas de forma conciente para poder aprovar la culminación de este proceso,
                                de lo contrario no podrá acceder al siguiente nivel.
                            </p>
                            <br />
                            <p><h2 style="font-family:'Segoe Print'" class="text-center my-5">¡Muchos éxitos!</h2></p>
             
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 my-5" >
                  <h1 class="text-center">¿Qué es un procedimiento?</h1>
                  <div class="  my-5 ml-4  ">
                        <div >
                            <asp:RadioButton ID="RadioButton1" runat="server" GroupName="preguntacuatro" text="A) Es un subprograma que realiza una tarea específica."/>
                        </div>
                        <div class="mt-4">
                            <asp:RadioButton ID="RadioButton2" runat="server" GroupName="preguntacuatro" text="B) Es un subprograma que hace una o más instrucciones pero que  siempre devuelve un sólo valor."/>
                        </div>
                        <div class="mt-4">
                            <asp:RadioButton ID="RadioButton3" runat="server" GroupName="preguntacuatro" text="C) Es un dato estructurado, formado por elementos lógicamente relacionados."/>
                        </div>
                        <div class="mt-4">
                            <asp:RadioButton ID="RadioButton4" runat="server" GroupName="preguntacuatro" text="D) Es una colección de datos del mismo tipo , que se almacenan en posiciones consecutivas  de memoria."/>
                        </div> 
                   </div>
            </div>
        </div>

          <%-- /////////////////////////////////////Pregunta Diez evaluacion parte 2/////////////////////////////////////// --%>
         
        
             <div class="row contenedor-barra1 " id="preguntaDiez" runat="server" visible="false">
            <div class="col-lg-4 contenedor-barra">
                <div class="columna-info">
                    <div class=" mt-3">
                        <div class="col bg-alert alert-success">
                            <!--Titulo del Nivel-->
                            <h2 class="py-2"><strong>Test final</strong> </h2>
                        </div>
                    </div>
                    <div class="">
                        <div class="">
              <!--Contenido del nivel informacion referente-->
                                          <p>A continuación se encontrará con una serie de preguntas que
                                evaluarán sus conocimientos adquiridos en <strong>Procedimientos</strong> y <strong>Funciones</strong>
                                durante el recorrido en el mundo de la programación.
                            </p>
                            <br />
                            <p>Por favor responda las preguntas de forma conciente para poder aprovar la culminación de este proceso,
                                de lo contrario no podrá acceder al siguiente nivel.
                            </p>
                            <br />
                            <p><h2 style="font-family:'Segoe Print'" class="text-center my-5">¡Muchos éxitos!</h2></p>
             
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 my-5" >
                  <h1 class="text-center">¿Qué es una función?</h1>
                  <div class="  my-5 ml-4  ">
                        <div >
                            <asp:RadioButton ID="RadioButton5" runat="server" GroupName="preguntacuatro" text="A) Es una variable global llamada en una ejecucion."/>
                        </div>
                        <div class="mt-4">
                            <asp:RadioButton ID="RadioButton6" runat="server" GroupName="preguntacuatro" text="B) Es una instrución que se ejecuta las veces que se le indique."/>
                        </div>
                        <div class="mt-4">
                            <asp:RadioButton ID="RadioButton7" runat="server" GroupName="preguntacuatro" text="C) Es un subprograma que ejecuta una serie de instruciones."/>
                        </div>
                        <div class="mt-4">
                            <asp:RadioButton ID="RadioButton8" runat="server" GroupName="preguntacuatro" text="D) Es un subprogramas que hace una o más instrucciones pero que  siempre devuelve un solo valor."/>
                        </div> 
                   </div>
            </div>
        </div>

        <%-- /////////////////////////////////////Pregunta Once evaluacion parte 3/////////////////////////////////////// --%>

           <div class="row contenedor-barra1 " id="preguntaOnce_" runat="server" visible="false">
            <div class="col-lg-4 contenedor-barra">
                <div class="columna-info">
                    <div class=" mt-3">
                        <div class="col bg-alert alert-success">
                            <!--Titulo del Nivel-->
                            <h2 class="py-2"><strong>Test final</strong> </h2>
                        </div>
                    </div>
                    <div class="">
                        <div class="">
              <!--Contenido del nivel informacion referente-->
                                          <p>A continuación se encontrará con una serie de preguntas que
                                evaluarán sus conocimientos adquiridos en <strong>Procedimientos</strong> y <strong>Funciones</strong>
                                durante el recorrido en el mundo de la programación.
                            </p>
                            <br />
                            <p>Por favor responda las preguntas de forma conciente para poder aprovar la culminación de este proceso,
                                de lo contrario no podrá acceder al siguiente nivel.
                            </p>
                            <br />
                            <p><h2 style="font-family:'Segoe Print'" class="text-center my-5">¡Muchos éxitos!</h2></p>
             
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 my-5" >
                  <h1 class="text-center">¿Qué son pa´rametros de valor?</h1>
                <div class="  my-5 ml-4  ">
                        <div >
                            <asp:RadioButton ID="RadioButton9" runat="server" GroupName="preguntacuatro" text="A) Es cuando una función recibe sólo una copia del valor que tiene la variable, o sea que no la puede modificar."/>
                        </div>
                        <div class="mt-4">
                            <asp:RadioButton ID="RadioButton10" runat="server" GroupName="preguntacuatro" text="B) Es un dato estructurado, formado por elementos lógicamente relacionados."/>
                        </div>
                        <div class="mt-4">
                            <asp:RadioButton ID="RadioButton11" runat="server" GroupName="preguntacuatro" text="C) Es una instrución que se ejecuta las veces que se le indique."/>
                        </div>
                        <div class="mt-4">
                            <asp:RadioButton ID="RadioButton12" runat="server" GroupName="preguntacuatro" text="D) Es un dato estructurado."/>
                        </div> 
                   </div>
            </div>
        </div>

        <%-- /////////////////////////////////////Pregunta Doce evaluacion parte 4/////////////////////////////////////// --%>

        
          <div class="row contenedor-barra1 " id="preguntaDoce" runat="server" visible="false">
            <div class="col-lg-4 contenedor-barra">
                <div class="columna-info">
                    <div class=" mt-3">
                        <div class="col bg-alert alert-success">
                            <!--Titulo del Nivel-->
                            <h2 class="py-2"><strong>Test final</strong> </h2>
                        </div>
                    </div>
                    <div class="">
                        <div class="">
              <!--Contenido del nivel informacion referente-->
                                          <p>A continuación se encontrará con una serie de preguntas que
                                evaluarán sus conocimientos adquiridos en <strong>Procedimientos</strong> y <strong>Funciones</strong>
                                durante el recorrido en el mundo de la programación.
                            </p>
                            <br />
                            <p>Por favor responda las preguntas de forma conciente para poder aprovar la culminación de este proceso,
                                de lo contrario no podrá acceder al siguiente nivel.
                            </p>
                            <br />
                            <p><h2 style="font-family:'Segoe Print'" class="text-center my-5">¡Muchos éxitos!</h2></p>
             
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 my-5" >
                  <h1 class="text-center">¿Qué son parámetros por referencia?</h1>
                  <div class="  my-5 ml-4  ">
                        <div >
                            <asp:RadioButton ID="RadioButton13" runat="server" GroupName="preguntacuatro" text="A) Es cuando se capturan dos datos de un procedimiento."/>
                        </div>
                        <div class="mt-4">
                            <asp:RadioButton ID="RadioButton14" runat="server" GroupName="preguntacuatro" text="B) Es cuando la variable que se recibe como parámetro en la función apunta exactamente a la misma dirección de memoria que la variable original."/>
                        </div>
                        <div class="mt-4">
                            <asp:RadioButton ID="RadioButton15" runat="server" GroupName="preguntacuatro" text="C) Es cuando una función retorna algún tipo de valor."/>
                        </div>
                        <div class="mt-4">
                            <asp:RadioButton ID="RadioButton16" runat="server" GroupName="preguntacuatro" text="D) Es cuando el resulatdo de una función y un procedimiento se unen."/>
                        </div> 
                   </div>
            </div>
        </div>

        <%-- /////////////////////////////////////Pregunta Trece evaluacion parte 4/////////////////////////////////////// --%>

              <div class="row contenedor-barra1 " id="preguntaTrece" runat="server" visible="false">
            <div class="col-lg-4 contenedor-barra">
                <div class="columna-info">
                    <div class=" mt-3">
                        <div class="col bg-alert alert-success">
                            <!--Titulo del Nivel-->
                            <h2 class="py-2"><strong>Test final</strong> </h2>
                        </div>
                    </div>
                    <div class="">
                        <div class="">
              <!--Contenido del nivel informacion referente-->
                                          <p>A continuación se encontrará con una serie de preguntas que
                                evaluarán sus conocimientos adquiridos en <strong>Procedimientos</strong> y <strong>Funciones</strong>
                                durante el recorrido en el mundo de la programación.
                            </p>
                            <br />
                            <p>Por favor responda las preguntas de forma conciente para poder aprovar la culminación de este proceso,
                                de lo contrario no podrá acceder al siguiente nivel.
                            </p>
                            <br />
                            <p><h2 style="font-family:'Segoe Print'" class="text-center my-5">¡Muchos éxitos!</h2></p>
             
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 my-5" >
                  <h1 class="text-center">¿Para qué se utiliza una función?</h1>
                    <div class="  my-5 ml-4  ">
                        <div >
                            <asp:RadioButton ID="RadioButton17" runat="server" GroupName="preguntacuatro" text="A) Se usa para llamar un porcedimiento."/>
                        </div>
                        <div class="mt-4">
                            <asp:RadioButton ID="RadioButton18" runat="server" GroupName="preguntacuatro" text="B) Se usa para acceder a un método específico."/>
                        </div>
                        <div class="mt-4">
                            <asp:RadioButton ID="RadioButton19" runat="server" GroupName="preguntacuatro" text="C) Es cuando una función retorna algún tipo de valor."/>
                        </div>
                        <div class="mt-4">
                            <asp:RadioButton ID="RadioButton20" runat="server" GroupName="preguntacuatro" text="D) Se usa para dar fin a un programa."/>
                        </div> 
                      <div class="mt-4">
                          <asp:Label ID="lblMensaje" runat="server" Text="Label" hidden="true"></asp:Label>
                          <asp:TextBox ID="txtnota" runat="server" hidden="true"></asp:TextBox>
                      </div>
                   </div>
            </div>
        </div>
        <footer id="footerPrincipal" class="bg-dark" runat="server">
            <div class="container bg-dark">
                <div class="row bg-dark p-2 text-center justify-content-center align-items-center">
                    <div class="col-2 col-lg-4">
                        <asp:Button CssClass="btn btn-outline-light" ID="btnRegresar" runat="server" Text="Atrás" OnClick="btnRegresar_Click" />
                    </div>
                    <div class="col-2 col-lg-4">
                        <p class="text-white d-block"><span id="valor" runat="server">1</span>/<span>13</span></p>
                    </div>
                    <div class="col-2 col-lg-4">
                        <asp:Button CssClass="btn btn-outline-light" ID="btnEnviar" runat="server" Text="Siguiente" OnClick="btnEnviar_Click" />
                    </div>
                </div>
            </div>
        </footer>
    </form>

    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="../js/nivel4.page8.js"></script>
    <script src="../js/Nivel4.pag7.js"></script>
    <script src="../js/Nivel4.pag6.js"></script>
    <script src="../js/nivel4.pag3.js"></script>
    <script src="../js/Animacion.js"></script>
    <script src="../js/jquery-3.3.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/popper.min.js"></script>
</body>
</html>
