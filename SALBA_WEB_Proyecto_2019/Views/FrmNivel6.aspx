﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrmNivel6.aspx.cs" Inherits="SALBA_WEB_Proyecto_2019.FrmNivel6" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>SALBA - Nivel 6</title>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>
    <link rel="stylesheet" href="../css/dragDrop.css"/>
   

    <link rel="stylesheet" href="../css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../css/estilosPropios.css"/>
    <link href="../css/estilos.css" rel="stylesheet" />
    <link rel="stylesheet" href="../css/main.css"/>
    <link rel="icon" href="../img/logoSena.png"/>
    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"/>
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web&display=swap" rel="stylesheet"/>
    
    
</head>
<body>
    <form id="frmPreguntaNivel6" runat="server" style="height: 100%; width: 100%">
        <nav class="navbar navbar-expand-sm navbar-light bg-light bg-Blanco navbar-expand-md shadow-sm fixed-top">
            <div class="container">

                <a class="navbar-brand prueba" href="#">
                    <img src="img/LogoSalba.png" width="130" class="img-fluid" alt="" /></a>
                <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="collapsibleNavId">
                    <div class="navbar-nav ml-auto mt-2 mt-lg-0">
                        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                            <li class="nav-item active">
                                <a href="#" class="btn  mr-1 text-primary enlace">Inicio</a>
                            </li>
                            <li class="nav-item active">
                                <a href="#" class="btn  mr-1 text-primary enlace">Scores</a>
                            </li>
                            <li class="nav-item">
                                <a href="#" style="" class="btn text-black ml-1 enlace"><i class="fas fa-user-circle mr-2"></i><b id="nombreUsuario" runat="server"></b></a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-cog"></i>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#">Configurar Perfil</a>
                                    <a class="dropdown-item" href="#">Politicas</a>
                                    <div class="dropdown-divider"></div>
                                    <asp:Button ID="btnCerrarSesion" CssClass="dropdown-item" runat="server" Text="Cerrar sesión" OnClick="btnCerrarSesion_Click" />
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>



        <div id="preguntaUno" class="row contenedor-barra1 " runat="server" visible="true">
            
                <div class="col-lg-4 contenedor-barra">
                    <div class="columna-info">
                        <div class=" mt-3">
                            <div class="col bg-alert alert-success">
                                <!--Titulo del Nivel-->
                                <h2 class="py-2"><strong>Nivel 6</strong> </h2>
                            </div>
                        </div>
                        <div class="">
                            <div class="">
                                <!--Contenido del nivel informacion referente-->
                                <p class="text-justify mt-3">Introducción a los Arreglos</p>
                                <h4><strong>¿Qué es un arreglo?</strong></h4>
                                <p>Un arreglo puede definirse como un grupo o una colección finita, homogénea y ordenada de elementos. Los arreglos pueden ser de los siguientes tipos:</p>
                                <ul>
                                    <li>De una dimensión.</li>
                                    <li>De dos dimensiones.</li>
                                    <li>De tres o más dimensiones.</li>
                                </ul>
                                 <h3>Declaración de vectores en LPP</h3>

                                 <p>Un arreglo unidimensional en LPP se declara usando la palabra reservada arreglo seguida por una pareja de corchetes cuadrados que entre ellos tiene la cantidad MÁXIMA de datos que puede albergar el arreglo; posteriormente se escribe la palabra reservada de seguida del tipo de datos que se van a guardar y, por último, se escribe el nombre que va a llevar el arreglo.</p>
                           
                                 <p>Ejemplo: Crear un arreglo que pueda almacenar hasta 8 número enteros</p>
                                 <p>ARREGLO[8] de ENTERO numero</p>
                                 
                                    
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <h2 class="mt-4">Actividad</h2>
                    <p class="mb-3 mt-5">Ordenar cada imagen de manera correcta con el fin de crear un arreglo de 5 posiciones el cual almacena cadena de caracteres</p>
                       <div class="d-flex flex-row">
                           <div class="ui-widget-header droppable" id="cont1-1">
                               <p class="text-center">------------</p>
                           </div>
                           <div class="ui-widget-header droppable" id="cont1-2">
                               <p class="text-center">-----------</p>
                           </div>
                           <div class="ui-widget-header droppable">
                               <p class="text-center">    letras   </p>
                           </div>

                       </div> 
                    
                        <p>Elementos arrastrables</p>
                        <ul style="list-style:none">
                            <li id="item1-1" class="ui-widget-content draggable">
                                <p><span style="color:blue">ARREGLO</span>[5]</p>
                            </li>
                            <li id="item1-2" class="ui-widget-content draggable" >
                                <p style="color:blue">de CADENA</p>
                            </li>
                            <li id="item1-3" class="ui-widget-content draggable">
                                <p><span style="color:blue">de CADENA</span>[5]</p>
                            </li>
                            <li id="item1-4" class="ui-widget-content draggable">
                                <p><span style="color:blue">ARREGLO</span>[8]</p>
                            </li>
                        </ul>
 
                </div>
        </div>

        <div id="preguntaDos" class="row contenedor-barra1 " runat="server" visible="false">
            
                <div class="col-lg-4 contenedor-barra">
                    <div class="columna-info">
                        <div class=" mt-3">
                            <div class="col bg-alert alert-success">
                                <!--Titulo del Nivel-->
                                <h2 class="py-2"><strong>Nivel 6</strong> </h2>
                            </div>
                        </div>
                        <div class="">
                            <div class="">
                                <!--Contenido del nivel informacion referente-->
                                <p class="text-justify mt-3">Introduccion a los arreglos</p>
                                <h4><strong>Llenar y recorrer un vector</strong></h4>
                                <p>Para llenar un arreglo unidimensional en LPP basta con invocar la posición del arreglo a la cual se quiere acceder (esto se hace escribiendo el nombre del arreglo seguido de un par de corchetes cuadrados con la posición a la que se desea acceder entre ellos) y asignarle el valor que se desea guardar en dicha posición.</p>
                                <p>Ejemplo:</p>
                                <p>Para asignar un valor de 3 a la primera posición del arreglo que se declaró en el ejemplo anterior, la instrucción sería:</p>
                                <p>numeros[1]<-3</p>
                                <img class="img-fluid" src="../img/llenarVector.jpg" />

                             
                                <h4><strong>Recorrer un vector</strong></h4>
                                <p>Recorrer un vector es pasar casilla por casilla del vector para conocer los elementos guardados en las posiciones, para posteriormente realizar alguna proceso con ellos.</p>
                                <p>Ejemplo en LPP:</p>
                                <img class="img-fluid"  src="../img/recorrerVector.jpg"/>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <!--contenido a evaluar o preguntar-->
                    <h2 class="mt-4">Actividad</h2>
                    <p class="mb-3 mt-5">Ordenar cada imagen de manera correcta deacuerdo a la sísntasis de LPP</p>
                       <div class="d-flex flex-row">
                           <div class="ui-widget-header droppable" id="cont2-1">
                               <p class="text-center">------------</p>
                           </div>
                           <div class="ui-widget-header droppable" id="cont2-2">
                               <p class="text-center">-----------</p>
                           </div>
                           <div class="ui-widget-header droppable">
                               <p class="text-center">    edad   </p>
                           </div>
                        </div> 
                        <div class="d-flex flex-column">
                            <div class="ui-widget-header droppable">
                                <p style="color:blue" class="text-center">INICIO</p>
                            </div>
                            <div class="d-flex flex-column pl-4">
                                
                                <div class="ui-widget-header droppable" id="cont2-3">
                                    <p class="text-center">-----------</p>
                                </div>
                                <div class="ui-widget-header droppable" id="cont2-4">
                                    <p class="text-center">-----------</p>
                                </div>
                                <div class="ui-widget-header droppable" id="cont2-5">
                                   <p class="text-center">-----------</p>
                                </div>
                                
                            </div>
                            <div class="ui-widget-header droppable">
                                <p style="color:blue" class="text-center">FIN</p>
                            </div>
                        </div>
                    
                        <p>Elementos arrastrables</p>
                        <ul style="list-style:none">
                            <li id="item2-1" class="ui-widget-content draggable">
                                <p><span style="color:blue">ARREGLO</span>[4]</p>
                            </li>

                            <li id="item2-2" class="ui-widget-content draggable" >
                                <p style="color:blue">de ENTERO</p>
                            </li>
                            <li id="item2-3" class="ui-widget-content draggable">
                                <p>edad[1]<-3</p>
                            </li>
                            <li id="item2-4" class="ui-widget-content draggable">
                                <p>edad[2]<-3</p>
                            </li>
                            <li id="item2-5" class="ui-widget-content draggable">
                                <p>edad[3]<-3</p>
                            </li>
                            <li id="item2-6" class="ui-widget-content draggable">
                                <p>edad[0]<-3</p>
                            </li>
                        </ul>
                
            </div>
        </div>

        <div id="preguntaTres" class="row contenedor-barra1 " runat="server" visible="false">
            <div class="col-lg-4 contenedor-barra">
                <div class="columna-info">
                    <div class=" mt-3">
                        <div class="col bg-alert alert-success">
                            <!--Titulo del Nivel-->
                            <h2 class="py-2"><strong>Nivel 6</strong> </h2>
                        </div>
                    </div>
                    <div class="">
                        <div class="">
                            <!--Contenido del nivel informacion referente-->
                            <p class="text-justify mt-3">Introduccion a los Arreglos</p>
                            <h4><strong>Arreglos bidimensionales (Matrices)</strong></h4>
                            <p>Un arreglo bidimensional en LPP se declara usando la palabra reservada arreglo seguida por una pareja de corchetes cuadrados que entre ellos tienen dos cifras separadas por una coma: la primera cifra indica la cantidad de filas que tendrá el arreglo, mientras que la segunda cifra indica la cantidad de columnas que tendrá el mismo. Posteriormente se escribe la palabra reservada de seguida del tipo de datos que se van a guardar y, por último, se escribe el nombre que va a llevar la matriz.</p>
                            <p>Ejemplo: crearemos una matriz de 2 filas * 4 columnas</p>
                            <p>arreglo[2,4] de cadena[20] palabra</p>
                            <p>Asignar valores a una matriz en LPP</p>
                            <p>Para llenar un arreglo bidimensional en LPP basta con invocar la posición del arreglo a la cual se quiere acceder (esto se hace escribiendo el nombre del arreglo seguido de un par de corchetes cuadrados con la posición a la que se desea acceder entre ellos) y asignarle el valor que se desea guardar en dicha posición.</p>
                            <p>Ejemplo: Para asignar la palabra "Programación" en el arreglo que se declaró en el ejemplo anterior, la instrucción sería:</p>
                            <p>palabra[1,1]<-"Programación"</p>

                            <p class="mt-5">Recorrer y llenar un arreglo mediante un ciclo</p>
                            <p>Ejemplo en LPP</p>
                            <img class="img-fluid" src="../img/matriz.jpg" />
                            
                            <p class="mt-2 mb-3">Ejecución</p>
                            <img class="img-fluid" src="../img/ejecucionMatriz.jpg" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <!--contenido a evaluar o preguntar-->
                 <h2 class="mt-4">Actividad</h2>
                 <p>Dependiendo de la siguiente imagen responda:</p>
                <img class="img-fluid" src="../img/actividadMatriz.jpg" />

                <div class="form-group">
                    <p class="mb-3">¿Cuántos números se mostrarán en pantalla?</p>

                    <ul class="list-group">
                        <li class="list-group-item">
                              <input  type="radio" name="r1" value="0"/>Se mostrarán en pantalla 3 números
                        </li>
                        <li class="list-group-item">
                             <input  type="radio" name="r1" value="1"/>Se mostrarán en pantalla 5 números
                        </li>
                        <li class="list-group-item">
                             <input  type="radio" name="r1" value="2"/>Se mostrarán en pantalla 9 números
                        </li>
                        <li class="list-group-item">
                            <input  type="radio" name="r1" value="3"/>NO se mostrará en pantalla ningún número
                        </li>
                    </ul>
                    <div>
                        <button type="button" class="btn btn-outline-success mt-2" id="btnActividad3">Evaluar</button>

                    </div>
          

                </div>
                
            </div>
        </div>

        <div id="preguntaCuatro" class="row contenedor-barra1 "  runat="server" visible="false">
            <div class="col-lg-4 contenedor-barra">
                <div class="columna-info">
                    <div class=" mt-3">
                        <div class="col bg-alert alert-success">
                            <!--Titulo del Nivel-->
                            <h2 class="py-2"><strong>Nivel 6</strong> </h2>
                        </div>
                    </div>
                    <div class="">
                        <div class="">
                            <!--Contenido del nivel informacion referente-->
                            <p class="text-justify mt-3">Introducción a los Arreglos</p>
                            <h4><strong>Algoritmo de ordenamiento</strong></h4>
                            <p>Es el proceso de organizar datos en algún orden o secuencia específica, tal como creciente o decreciente, para datos numéricos o alfabéticos.</p>
                            <p>Algoritmo de ordenamiento Burbuja</p>
                            <p>Se basa en el principio de comparar pares de elementos adyacentes e intercambiarlos entre si hasta que estén todos ordenados.</p>
                            <p>Ejemplo en LPP</p>
                            <img class="img-fluid" src="../img/burbuja.jpg" />
                            <img class="img-fluid" src="../img/ejecucionBurbuja.jpg" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <!--contenido a evaluar o preguntar-->
                 <h2 class="mt-4">Actividad</h2>
                 <p>Dependiendo el contenido expuesto:</p>

                <div class="form-group">
                    <p class="mb-3">¿Para que sirve la variable aux en el algoritmo?</p>

                    <ul class="list-group">
                        <li class="list-group-item">
                              <input  type="radio" name="r2" value="0"/> Se utiliza para almacenar en valor del vector en la posición i 
                        </li>
                        <li class="list-group-item">
                             <input  type="radio" name="r2" value="1"/> Se utiliza para almacenar en valor de la posición j
                        </li>
                        <li class="list-group-item">
                             <input  type="radio" name="r2" value="2"/> Se le asigna al vector en la posición i
                        </li>
                        <li class="list-group-item">
                            <input  type="radio" name="r2" value="3"/> Se utiliza como vector
                        </li>
                    </ul>
                    <div>
                        <button type="button" class="btn btn-outline-success mt-2" id="btnActividad4">Evaluar</button>

                    </div>
                </div>
            </div>
        </div>
         <div id="PreguntarExamen" class="d-flex justify-content-center contenedor-barra1 align-items-center " runat="server" visible="false">
            
           
            <div class="card" style="width: 400px;">
		        <img src="../img/Examen6.jpg" class="card-img-top img-fluid"/>
		        <div class="card-body text-center">
			        <h3 class="card-title">Examen nivel 6</h3>
			        <p class="card-text">Desea realizar el examen</p>
                    <div class="d-flex flex-row justify-content-center">
                               
                        <asp:Button ID="btnSi" CssClass="btn btn-outline-success mr-1" runat="server" Text="SI" OnClick="btnSi_Click" />
                        <asp:Button ID="btnNo" CssClass="btn btn-outline-danger ml-1" runat="server" Text="NO" OnClick="btnNo_Click" />

                    </div>
				            
		        </div>
	        </div>
    

         </div>
         <div id="preguntaCinco" class="container mb-5 contenedor-barra1 " runat="server" visible="false">
            <div class="row">
                <div class="col-lg-4">
                        <div class="columna-info">
                            <div class=" mt-3">
                                <div class="col bg-alert alert-success">
                                    <!--Titulo del Nivel-->
                                    <h2 class="py-2"><strong>Examen Nivel 6</strong> </h2>
                                </div>
                            </div>
                            <div class="">
                                <div class="">
                                    <!--Contenido del nivel informacion referente-->
                       
                                    <p class="mt-5 mb-4">Este examen constara de una seria de preguntas relacionadas al contenido expuesto anteriormente sobre arreglos y matrices.</p>
                                    <p>“Si el código y los comentarios no coinciden, posiblemente ambos sean erróneos” –  <span class="text-muted">Norm Schryer</span></p>
                                 
                                    <img class="img-fluid" src="../img/Examen6.jpg"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row col-lg-8 mt-5">
                    <h2 class="mt-4">Preguntas</h2>
                    <h4 class="mb-3 mt-5 mb-5">1) Hacer un algoritmo que almacene números en una matriz de 3 * 3, luego imprimir el número mayor</h4>
                    <div class="col-6 mr-5">
                           
                    <asp:RadioButton ID="p11" value="1" runat="server" style="display:none" />
                    <asp:RadioButton ID="p12" value="2" runat="server" style="display:none" />
                    <asp:RadioButton ID="p13" value="3" runat="server" style="display:none" />
                    <asp:RadioButton ID="p14" value="4" runat="server" style="display:none" />
                    <asp:RadioButton ID="p15" value="4" runat="server" style="display:none" />
                            <div class="d-flex flex-column">
                                <div class="ui-widget-header droppable1" id="cont3-1">
                                    <p class="text-center">------------</p>
                                </div>
                                <div class="ui-widget-header droppable1">
                                    <p style="color:blue" class="text-center">INICIO</p>
                                </div>
                                <div class="d-flex flex-column pl-4">
                                    <div class="ui-widget-header droppable1" id="cont3-2">
                                       <p class="text-center">-----------</p>
                                   </div>
                                
                                    <div class="ui-widget-header droppable1" id="cont3-3">
                                        <p class="text-center">-----------</p>
                                    </div>
                                    <div class="ui-widget-header droppable1" id="cont3-4">
                                        <p class="text-center">-----------</p>
                                    </div>
                                    <div class="ui-widget-header droppable1" id="cont3-5">
                                       <p class="text-center">-----------</p>
                                    </div>
                                
                                </div>
                                <div class="ui-widget-header droppable1">
                                    <p style="color:blue" class="text-center">FIN</p>
                                </div>
                            </div>
                        </div>
                    <div class="ml-5 col-4">
                        <p>Elementos arrastrables</p>
                        <ul style="list-style:none">
                            <li id="item3-1" class="ui-widget-content draggable1">
                                <img class="img-fluid" src="../img/Examen6/1.jpg" />
                            </li>

                            <li id="item3-4" class="ui-widget-content draggable1" >
                                <img class="img-fluid" src="../img/Examen6/4.jpg" />
                            </li>
                            <li id="item3-3" class="ui-widget-content draggable1">
                                <img class="img-fluid" src="../img/Examen6/3.jpg" />
                            </li>
                            <li id="item3-2" class="ui-widget-content draggable1">
                                <img class="img-fluid" src="../img/Examen6/2.jpg" />
                            </li>
                            <li id="item3-5" class="ui-widget-content draggable1">
                                <img class="img-fluid" src="../img/Examen6/5.jpg" />
                            </li>
                            
                        </ul>
                    </div>

                    <h4 class="mb-3 mt-5">2) Dependiendo la siguiente imagen escoja la respuesta correcta</h4>
                    <img class="img-fluid" src="../img/Examen6/p2.jpg" />
                    <p>
                    <asp:RadioButtonList ID="p2" runat="server">
                        <asp:ListItem Value="0">Recorre un vector de tres posiciones</asp:ListItem>
                        <asp:ListItem Value="1">Recorre e imprime una matriz 3*3</asp:ListItem>
                        <asp:ListItem Value="2">Guarda valores en una matriz</asp:ListItem>
                        <asp:ListItem Value="3"> NA</asp:ListItem>
                    </asp:RadioButtonList>
                    </p>
                    <hr />
                    <h4 class="mb-3 mt-5">3) Selecione como se declara correctamente un vector en LPP</h4>
                    <p>
                    <asp:RadioButtonList ID="p3" runat="server">
                        <asp:ListItem Value="0">ARREGLO[2] de ENTERO numero</asp:ListItem>
                        <asp:ListItem Value="1">ARREGLO[2,2] de ENTERO numero</asp:ListItem>
                        <asp:ListItem Value="2">ARREGLO[2] ENTERO numero</asp:ListItem>
                        <asp:ListItem Value="3"> NA</asp:ListItem>
                    </asp:RadioButtonList>
                    </p>
                    <hr />
                    <h4 class="mb-3 mt-5">4) Selecione como se declara correctamente un arreglo bidimencional en LPP</h4>
                    <p>
                    <asp:RadioButtonList ID="p4" runat="server">
                        <asp:ListItem Value="0">ARREGLO[2] de ENTERO numero</asp:ListItem>
                        <asp:ListItem Value="1">ARREGLO[2,2] de ENTERO edad</asp:ListItem>
                        <asp:ListItem Value="2">ARREGLO[2] ENTERO numero</asp:ListItem>
                        <asp:ListItem Value="3"> NA</asp:ListItem>
                    </asp:RadioButtonList>
                    </p>
                    <hr />
                     <h4 class="mb-3 mt-5">5) ¡Cuál es el objetivo del algoritmo conocido como Burbuja?</h4>
                    <p>
                    <asp:RadioButtonList ID="p5" runat="server">
                        <asp:ListItem Value="0">Ordenar de manera descendente y ascendente matrices</asp:ListItem>
                        <asp:ListItem Value="1">Ordenar de manera descendente y ascendente vectores</asp:ListItem>
                        <asp:ListItem Value="2">Hallar el mayor número de un vector</asp:ListItem>
                        <asp:ListItem Value="3">Recorrer una matriz</asp:ListItem>
                    </asp:RadioButtonList>
                    </p>


                    
 
                </div>
  
        </div>

        <footer id="footerPrincipal" class="bg-dark" runat="server">
            <div class="container bg-dark">
                <div class="row bg-dark p-2 text-center justify-content-center align-items-center">
                    <div class="col-2 col-lg-4">
                        <asp:Button CssClass="btn btn-outline-light" ID="btnRegresar" runat="server" Text="Atrás" OnClick="btnRegresar_Click" />
                    </div>
                    <div class="col-2 col-lg-4">
                        <p class="text-white d-block"><span id="valor" runat="server">1</span>/<span>5</span></p>
                    </div>
                    <div class="col-2 col-lg-4">
                        <asp:Button CssClass="btn btn-outline-light" name="0"  ID="btnEnviar" runat="server" Text="Siguiente" Enabled="false" OnClick="btnEnviar_Click" />
                    </div>
                </div>
            </div>
        </footer>
    </form>
    
    <!--<script src="../js/jquery-3.4.1.js"></script>
    <script src="../js/jquery-ui.js"></script>-->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.js"></script>
    <script src="../js/level6.js"></script>
    <p>
        &nbsp;</p>
</body>

</html>
