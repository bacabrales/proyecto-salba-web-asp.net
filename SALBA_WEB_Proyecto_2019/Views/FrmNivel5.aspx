﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrmNivel5.aspx.cs" Inherits="SALBA_WEB_Proyecto_2019.FrmNivel5" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>SALBA - Nivel 5</title>
    <link rel="stylesheet" href="../css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../css/estilosPropios.css"/>
    <link href="../css/estilos.css" rel="stylesheet" />
    <link rel="stylesheet" href="../css/main.css"/>
    <link rel="icon" href="../img/logoSena.png"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" />
    <script src="https://kit.fontawesome.com/a5cc566dc4.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"/>
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web&display=swap" rel="stylesheet"/>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
 
   
        <div id="cambiarScript" runat="server">

        </div>
         

 


        <style type="text/css" media="screen">
    	
    	#cajaDestino{

    		float: left;
    		width: 450px;
    		height: 500px;
    		margin: 80px;
    		border:  3px solid black;

    	}

        .contenedor-barra-modificarCarlos{

    min-height: 100%;
    box-sizing: border-box;
    overflow-y: scroll;
    

}

    	#cajaOrigen{

    		float:  left;
    		width: 200px;
    		margin: 30px;
    	
    	}
    </style>



</head>
<body>
    <form id="frmPreguntaNivel5" runat="server" style="height: 100%; width: 100%">
        <nav class="navbar navbar-expand-sm navbar-light bg-light bg-Blanco navbar-expand-md shadow-sm fixed-top">
            <div class="container">

                <a class="navbar-brand prueba" href="#">
                    <img src="../img/LogoSalba.png" width="130" class="img-fluid" alt="" /></a>
                <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="collapsibleNavId">
                    <div class="navbar-nav ml-auto mt-2 mt-lg-0">
                        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                            <li class="nav-item active">
                                <a href="#" class="btn  mr-1 text-primary enlace">Inicio</a>
                            </li>
                            <li class="nav-item active">
                                <a href="#" class="btn  mr-1 text-primary enlace">Scores</a>
                            </li>
                            <li class="nav-item">
                                <a href="#" style="" class="btn text-black ml-1 enlace"><i class="fas fa-user-circle mr-2"></i><b id="nombreUsuario" runat="server"></b></a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-cog"></i>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#">Configurar Perfil</a>
                                    <a class="dropdown-item" href="#">Politicas</a>
                                    <div class="dropdown-divider"></div>
                                    <asp:Button ID="btnCerrarSesion" CssClass="dropdown-item" runat="server" Text="Cerrar sesión" OnClick="btnCerrarSesion_Click" />
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>

        
        <div id="introduccion" class="row contenedor-barra1 " runat="server" visible="true">
        <input type="hidden"  id="ultimoNivel" runat="server"/>
                <div class="col-lg-4 contenedor-barra" style="height: 960px !important">
                    <div class="columna-info">
                        <div class=" mt-3">
                            <div class="col bg-alert alert-success">
                                <!--Titulo del Nivel-->
                                <h2 class="py-2"><strong>Introducción</strong> </h2>
                            </div>
                        </div>
                        <div class="">
                            <div class="">
                                <!--Contenido del nivel informacion referente-->
                                <p class="text-justify mt-3">Introducción a los vectores</p>
                                <p>Definición: </p>
                                <p>Es considerado como una agrupación de objetos del mismo tipo que se nombran mediante un único identificador; tales objetos ocupan una posición del arreglo y pueden ser leídos o escritos accediendo a dicha posición.</p>
                                <br />
                                <p>Otra Definición: </p>
                                <p>Es una Colección de datos del mismo tipo , que se almacenan en posiciones consecutivas de memoria y reciben un nombre común. Y para referirse a un determinado elemento tendremos de acceder usando un índice para especificar la posición que queremos extraer o modificar su valor. </p>
                         
                                    <h2 class="py-2"><strong>El índice de un vector en lpp empieza en 1</strong> </h2>
                                <br />
                                 <img class="img-fluid" src="../img/vector18.png" />
                                <br />
                                <br />
                                <img class="img-fluid" src="../img/vector19.png" />
                </div>
                                </div>
                        </div>
                    </div>
              

             
                   
      
                <div class="col-lg-8">


                                 <p> <strong>Arreglos de una dimensión</strong>  </p>
                                 <p> <strong>Declaración: </strong> arreglo[Dimensión] de (Tipo de dato) (Nombre de la variable)<p/>
                                 <p> <strong>Dimensión:</strong> es el tamaño del arreglo , es un número entero con el cual indicamos el numero de elementos que queremos guardar con el mismo tipo. </p>
                                 <p> <strong>Tipo de dato: </strong> es el tipo que establecemos a la colección , puede ser entero , real , cadena , carácter o un registro. </p>
                                 <p> <strong>Nombre de la variable: </strong>es el nombre con el cual vamos a ser referencia en el programa principal </p>
                      
                            <h4><strong>Declaración de vector de tipo entero:</strong></h4>
                            <br />
                           <img class="img-fluid" src="../img/N1.png" />

                        <h4><strong>Declaración de vector de tipo cadena:</strong></h4>
                            <br />
                           <img class="img-fluid" src="../img/vector20.png" />
                     


            </div>
      
       </div>
 





        <div id="preguntaUno" class="row contenedor-barra1 " runat="server" visible="false">
  
                <div class="col-lg-4 contenedor-barra">
                    <div class="columna-info">
                        <div class=" mt-3">
                            <div class="col bg-alert alert-success">
                                <!--Titulo del Nivel-->
                                <h2 class="py-2"><strong>Pregunta uno</strong> </h2>
                            </div>
                        </div>
                        <div class="">
                            <div class="">
                                <!--Contenido del nivel informacion referente-->
                                <p class="text-justify mt-3"></p>
                             <h2 class="py-2"><strong>Instruciones de la evaluación</strong> </h2>
                                <p>En base a la intrucción de vectores  y algunas  destrezas propias se establecera un test de cinco preguntas
                                    ,cada pregunta vale un punto debe obtener mínimo 4 puntos para superar el nivel ¡suerte!

                                </p>
                                <img class="img-fluid" src="../img/vector1.png" />
                </div>
                                </div>
                        </div>
                    </div>
              

             
                   
      
                <div class="col-lg-8">


                            <h4><strong>¿Qué es un Vector?</strong></h4>
                            
                            <br />
                           <h4><strong>Posibles Respuestas:</strong></h4>
                         <asp:RadioButtonList ID="rbPregunta1" runat="server">


                    
                                    <asp:ListItem>A) Un conjunto de elementos ordenados en filas y columnas</asp:ListItem>
                                    <asp:ListItem>B)Un conjunto de objetos de diferentes tipos</asp:ListItem>
                                    <asp:ListItem >C)una zona de almacenamiento contiguo que contiene una serie de elementos del mismo tipo</asp:ListItem>
                                    <asp:ListItem>D) Contenedor de múltiples tipos de datos</asp:ListItem>

                               </asp:RadioButtonList>
                          


            </div>
      
       </div>
 

        <div id="preguntaDos" class="row contenedor-barra1 " runat="server" visible="false">
 
                <div class="col-lg-4 contenedor-barra-modificarCarlos">
                    <div class="columna-info">
                        <div class=" mt-3">
                            <div class="col bg-alert alert-success">
                                <!--Titulo del Nivel-->
                                <h2 class="py-2"><strong>Pregunta dos</strong> </h2>
                            </div>
                        </div>
                        <div class="">
                            <div class="">
                                <!--Contenido del nivel informacion referente-->
                                <p class="text-justify mt-3"></p>
                                <h4><strong>Asignación de un valor en las diferentes posiciones de un vector</strong></h4>
                                <p>En un vector se puede asignar un valor  a cada posición  en caso de que este sea necesario
                                    Como lo vemos en la siguiente imagen:


                                </p>
                                <img class="img-fluid"  src="../img/Vector3.png"/>

                                <p>Este tipo de asignación no es tan recomendada ya que no es nada dinámico 
                                   lo mas recomendado para asignar valores en un vector es un ciclo repetitivo(MIENTRAS/WHILE)
                                   o un ciclo repetitivo(PARA/FOR) como lo veremos en la siguiente imagen:
                                </p>

                                <img class="img-fluid"  src="../img/Vector4.png" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">

                    <h4><strong>Analice el siguiente pseudocódigo y eliga la respuesta correcta</strong></h4>

                    <img  class="img-fluid" src="../img/Vector2.png" />

                      <h4><strong>Posibles Respuestas:</strong></h4>

                         <asp:RadioButtonList ID="rbPregunta2" runat="server">


                           
                                    <asp:ListItem>A)70 </asp:ListItem>
                                    <asp:ListItem>B)168</asp:ListItem>
                                    <asp:ListItem >C)104</asp:ListItem>
                                    <asp:ListItem>D)Ninguna de las anteriores</asp:ListItem>

                               </asp:RadioButtonList>

                            
                   
                </div>
            </div>


        <div class="row contenedor-barra1 " id="preguntaTres" runat="server" visible="false">
            <div class="col-lg-4 contenedor-barra-modificarCarlos">
                <div class="columna-info">
                    <div class=" mt-3">
                        <div class="col bg-alert alert-success">
                            <!--Titulo del Nivel-->
                            <h2 class="py-2"><strong>Pregunta tres</strong> </h2>
                        </div>
                    </div>
                    <div class="">
                        <div class="">
                            <!--Contenido del nivel informacion referente-->
                            <p class="text-justify mt-3"></p>
                            <h4><strong>Sintaxis correcta</strong></h4>
                            <p>
                                Una habilidad que adquieren los programadores a lo largo de los años
                                es la de detectar errores en sus códigos y arreglarlos. en esta fase
                                determinaremos su habilidad para detectar dichos errores de sintaxis.
                                a continuacion mostraremos la imagen de un código con error de sintaxis.  ¿Lo podras detectar?
                            </p>
                            <img class="img-fluid" src="../img/Vector4.png" />
                           
                            <h4><strong>Código correcto</strong></h4>
                           <asp:Button ID="btnVermas" runat="server" CssClass="btn btn-outline-info" Text="Ver más" OnClick="btnVermas_Click" />
                            <div id="verMas" runat="server">
                          
                             <img  class="img-fluid" src="../img/Vector6.png" />
                            </div>
                        
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <h4><strong>Analice el siguiente pseudocódigo de las imagenes elija (verdadero)
                    si la sintaxis esta correcta de lo contrario elija (falso)
                    </strong></h4>

                <div class="float-left mr-5">

         
                <h3>1)</h3>
                <img  class="img-fluid" src="../img/Vector7.png" />

                    <h4><strong>Posibles Respuestas:</strong></h4>

                    <asp:DropDownList ID="dl1" runat="server">
                        <asp:ListItem>Verdadero</asp:ListItem>
                        <asp:ListItem>Falso</asp:ListItem>
                    </asp:DropDownList>

                 <br />
                 <br />
                           </div>
              <div class="float-left">

           
                     <h3>2)</h3>

                    <img  class="img-fluid" src="../img/Vector8.png" />
                  <h4><strong>Posibles Respuestas:</strong></h4>

                     <asp:DropDownList ID="dl2" runat="server">
                        <asp:ListItem>Verdadero</asp:ListItem>
                        <asp:ListItem>Falso</asp:ListItem>
                    </asp:DropDownList>

                <br />
                <br />
                     </div>





            </div>
        </div>

        <div class="row contenedor-barra1 " id="preguntaCuatro" runat="server" visible="false">
            <div class="col-lg-4 contenedor-barra">
                <div class="columna-info">
                    <div class=" mt-3">
                        <div class="col bg-alert alert-success">
                            <!--Titulo del Nivel-->
                            <h2 class="py-2"><strong>Pregunta cuatro</strong> </h2>
                        </div>
                    </div>
                    <div class="">
                        <div class="">
                            <!--Contenido del nivel informacion referente-->
                            <p class="text-justify mt-3"></p>
                            <h4><strong>¿Esta conciente del código que compila?</strong></h4>
                            <p>En la progrmación existen diversas prácticas. Una buena práctica en el mundo de la programación 
                                es no compilar el código hasta estar seguros de que todo ha quedado resuelto, muchos programadores
                                no tienen esto en cuenta y por lo contrario compilan demasiadas veces para probar su código convirtiendose
                                en un ciclo de ensayo y error,la pregunta es: ¿Esta Seguro de lo que esta programando? Esta  tecnica 
                                es recomendada ya que  desarrolla  habilidades como reducir el tiempo de elaboración del software, mayor entendimiento del
                                código,una mayor lógica al escribir el código en esta fase se evaluara su hablidad en dicha tecnica.
                            
                              
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div  class="col-lg-8" id="caja1">
               <h4><strong> El siguiente código esta incompleto. Debe  lograr que el algortimo cumpla su función correctamente 
                   para ello debe escoger un framento de código y completar los campos vacíos del código.
                            

                   </strong></h4>

              <asp:Button  ID="btnRestablecerImagenes" runat="server" CssClass="btn btn-outline-info float-lg-right mt-3  ml-2" Text="Restablecer" OnClick="btnRestablecerImagenes_Click" />
             <div class="float-right" id="cajaOrigen07" runat="server">
                    
            
                 <img src="../img/Vector12.png" id="imagenOrigen08" />

               <hr />
                 <img src="../img/Vector9.png" id="imagenOrigen07" />
                

                 <hr />

                   <img src="../img/Vector16.png" id="imagenOrigen09" />



           
               
            </div>
                      
                
	  


              

                  <p class=""><img src="../img/Vector10.png"/></p>
                  <div id="cajaDestino07" runat="server">
                       Campo Vacío
                  </div>
                <br />
                        
                    <p><img src="../img/Vector11.png"/></p>

                           <div id="cajaDestino08" runat="server">
                                     Campo Vacío
                            </div>

                     <p><img src="../img/Vector13.png"/></p>

                              <div id="cajaDestino09" runat="server">
                                     Campo Vacío
                            </div>

                      <p><img src="../img/Vector15.png"/></p>

	 	          
                   
          
        

                <input  type="hidden"  id="TotalTres" runat="server"/>

                 
  

 


            
            </div>
        </div>

        <div class="row contenedor-barra1 " id="preguntaCinco" runat="server" visible="false">
            <div class="col-lg-4 contenedor-barra-modificarCarlos">
                <div class="columna-info">
                    <div class=" mt-3">
                        <div class="col bg-alert alert-success">
                            <!--Titulo del Nivel-->
                            <h2 class="py-2"><strong>Pregunta cinco</strong> </h2>
                        </div>
                    </div>
                    <div class="">
                        <div class="">
                            <!--Contenido del nivel informacion referente-->
                            <p class="text-justify mt-3">Método de ordenamiendo burbuja</p>
                            <h4><strong>¿Qué es el método de ordenamiento burbuja?</strong></h4>
                            <p>Es un sencillo algoritmo de ordenamiento. Funciona revisando cada elemento de la lista que va a ser ordenada con el siguiente, intercambiándolos de posición si están en el orden equivocado. Es necesario revisar varias veces toda la lista hasta que no se necesiten más intercambios, lo cual significa que la lista está ordenada. Este algoritmo obtiene su nombre de la forma con la que suben por la lista los elementos durante los intercambios, como si fueran pequeñas burbujas. También es conocido como el método del intercambio directo. Dado que solo usa comparaciones para operar elementos, se lo considera un algoritmo de comparación, siendo el más sencillo de implementar.</p>
                               <img src="../img/Vector17.png" class="img-fluid" />
                            </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 ">
                <div class="row">
                <div class="col">
                    <h4><strong>Analice el pseudocódigo de las imagenes y arraste hacia el campo en blanco
                    la imagen que tenga el método de ordenamiento burbuja correcto
                    </strong></h4>
                </div>
                </div>
                <div class="row">
                <div class="col-lg-6 float-left ml-0">
                    
                <div id="cajaDestino" class="text-center">
	 	            Arraste el elemento
                   
                  </div>
                </div>
                <div class="col-lg-6 ">
                    <div class="float-right ml-0">
	       <div id="cajaOrigen" >
                <img src="../img/Vector5.png" id="img1N1" />
               <hr />
                <img src="../img/Vector8.png" id="img1N2" />
            </div>
                <input  type="hidden"  id="ResultadoOne" runat="server"/>

                 
  

        </div>
                </div>
                </div>
                
            
      
        </div>
 
        </div>

        <footer id="footerPrincipal" class="bg-dark" runat="server">
            <div class="container bg-dark">
                <div class="row bg-dark p-2 text-center justify-content-center align-items-center">
                    <div class="col-2 col-lg-4">
                        <asp:Button CssClass="btn btn-outline-light" ID="btnRegresar" runat="server" Text="Atrás" OnClick="btnRegresar_Click" />
                    </div>
                    <div class="col-2 col-lg-4">
                        <p class="text-white d-block"><span id="valor" runat="server">0</span>/<span>5</span></p>
                    </div>
                    <div class="col-2 col-lg-4">
            

                 
                        <asp:Button CssClass="btn btn-outline-light" ID="btnEnviar" runat="server" Text="Siguiente" OnClick="btnEnviar_Click" />
                      
                    </div>
                </div>
            </div>
        </footer>
    </form>
 
    <script src="../js/jquery-3.3.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/popper.min.js"></script>

</body>
</html>

