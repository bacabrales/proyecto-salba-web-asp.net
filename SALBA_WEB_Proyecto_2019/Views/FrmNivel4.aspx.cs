﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SALBA_WEB_Proyecto_2019
{
    public partial class FrmNivel4 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["pregunta"] = 1;
                //string usuario = (string)Session["usuario"];
                //string password = (string)Session["password"];
                //if (usuario != string.Empty && password != string.Empty)
                //{
                //    nombreUsuario.InnerText = (string)Session["nombre"] + " " + (string)Session["apellido"];
                //}
                //else
                //{
                //    Response.Redirect("https://localhost:44300/FrmHome.aspx");
                //}
            }
        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            if ((int)ViewState["pregunta"] == 1)
            {
                preguntaUno.Visible = false;
                preguntaDos.Visible = true;
                ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;
            }
            else if ((int)ViewState["pregunta"] == 2)
            {
                preguntaDos.Visible = false;
                preguntaTres.Visible = true;
                ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;
            }
            else if ((int)ViewState["pregunta"] == 3)
            {
                if (rd3.Checked == true)
                {
                    Response.Write("<script language=javascript>alert('Correcto');</script>");
                    preguntaTres.Visible = false;
                    preguntaCuatro.Visible = true;
                    ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;
                }
                else
                {
                    Response.Write("<script language=javascript>alert('Incorrecto');</script>");
                }
            }
            else if ((int)ViewState["pregunta"] == 4)
            {
                if (Convert.ToInt32(redirec.Value) == 1)
                {
                    preguntaCuatro.Visible = false;
                    preguntaCinco.Visible = true;
                    ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;
                }
                else
                {
                    Response.Write("<script language=javascript>alert('Incorrecto');</script>");
                }
            }

            else if ((int)ViewState["pregunta"] == 5)
            {
      
                if (TxtProcedimiento.Text == "Procedimiento" && TxtHasta.Text == "Hasta" && TxtFinPara.Text == "Fin Para" && txtEscriba.Text == "Escriba" && txtLlamar.Text == "Llamar" && txtFin.Text == "Fin")
                {
                    Response.Write("<script language=javascript>alert('Correcto');</script>");
                    preguntaCinco.Visible = false;
                    preguntaSeis.Visible = true;
                    ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;
                }
                else
                {
                    Response.Write("<script language=javascript>alert('Incorrecto');</script>");
                }
            }
            else if ((int)ViewState["pregunta"] == 6)
            {

                preguntaSeis.Visible = false;
                preguntaSiete.Visible = true;
                ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;
            }
            else if ((int)ViewState["pregunta"] == 7)
            {
                if (Convert.ToInt32(validarRedire.Value) == 1)
                {
                    preguntaSiete.Visible = false;
                    preguntaOcho.Visible = true;
                    ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;
                }
                else
                {
                    
                }         
            }
            else if ((int)ViewState["pregunta"] == 8)
            {

                preguntaOcho.Visible = false;
                PreguntaNueve.Visible = true;
                ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;
            }
            else if ((int)ViewState["pregunta"] == 9)
            {
                if (RadioButton1.Checked == true)
                {
                    ViewState["Evaluacion"] =+ 1;
                    PreguntaNueve.Visible = false;
                    preguntaDiez.Visible = true;
                    ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;
                }
                else
                {
                    ViewState["Evaluacion"] = (int)ViewState["Evaluacion"] + 0;
                    PreguntaNueve.Visible = false;
                    preguntaDiez.Visible = true;
                    btnRegresar.Enabled = false;
                    ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;
                }
               
            }
            else if ((int)ViewState["pregunta"] == 10)
            {
                if (RadioButton8.Checked == true)
                {
                    ViewState["Evaluacion"] = (int)ViewState["Evaluacion"] + 1;
                    preguntaDiez.Visible = false;
                    preguntaOnce_.Visible = true;
                    ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;
                }
                else
                {
                    ViewState["Evaluacion"] = (int)ViewState["Evaluacion"] + 0;
                    preguntaDiez.Visible = false;
                    preguntaOnce_.Visible = true;
                    btnRegresar.Enabled = false;
                    ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;
                }

            }
            else if ((int)ViewState["pregunta"] == 11)
            {
                if (RadioButton9.Checked == true)
                {
                    ViewState["Evaluacion"] = (int)ViewState["Evaluacion"] + 1;
                    preguntaOnce_.Visible = false;
                    preguntaDoce.Visible = true;
                    ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;
                }
                else
                {
                    ViewState["Evaluacion"] = (int)ViewState["Evaluacion"] + 0;
                    preguntaOnce_.Visible = false;
                    preguntaDoce.Visible = true;
                    btnRegresar.Enabled = false;
                    ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;
                }

            }
            else if ((int)ViewState["pregunta"] == 12)
            {
                if (RadioButton14.Checked == true)
                {
                    ViewState["Evaluacion"] = (int)ViewState["Evaluacion"] + 1;
                    preguntaDoce.Visible = false;
                    preguntaTrece.Visible = true;
                    ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;
                }
                else
                {
                    ViewState["Evaluacion"] = (int)ViewState["Evaluacion"] + 0;
                    preguntaDoce.Visible = false;
                    preguntaTrece.Visible = true;
                    btnRegresar.Enabled = false;
                    ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;
                }

            }
            else if ((int)ViewState["pregunta"] == 13)
            {
                if (RadioButton19.Checked == true)
                {
                    Response.Write("<script language=javascript>alert('Prueba terminada, ya puede dormi joven');</script>");
                    ViewState["Evaluacion"] = (int)ViewState["Evaluacion"] + 1;
                    preguntaDoce.Visible = false;
                    preguntaTrece.Visible = true;
                    ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;
                }
                else
                {
                    Response.Write("<script language=javascript>alert('Prueba terminada, ya puede dormi joven');</script>");
                    ViewState["Evaluacion"] = (int)ViewState["Evaluacion"] + 0;
                    preguntaDoce.Visible = false;
                    preguntaTrece.Visible = true;
                    btnRegresar.Enabled = false;
                    ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;
                }

            }
            //else if ((int)ViewState["pregunta"] == 5)
            //{
            //    //Aqui validar el envio a la base de datos de cuantas preguntas fueron correctas
            //}
            valor.InnerText = Convert.ToString((int)ViewState["pregunta"]);
        }

        protected void btnRegresar_Click(object sender, EventArgs e)
        {
            if ((int)ViewState["pregunta"] == 1)
            {
                //Redirecciona al inicio de la pagina de las instrucciones
            }
            else if ((int)ViewState["pregunta"] == 2)
            {
                preguntaDos.Visible = false;
                preguntaUno.Visible = true;
                ViewState["pregunta"] = (int)ViewState["pregunta"] - 1;
            }
            else if ((int)ViewState["pregunta"] == 3)
            {
                preguntaTres.Visible = false;
                preguntaDos.Visible = true;
                ViewState["pregunta"] = (int)ViewState["pregunta"] - 1;
            }
            else if ((int)ViewState["pregunta"] == 4)
            {
                preguntaCuatro.Visible = false;
                preguntaTres.Visible = true;
                ViewState["pregunta"] = (int)ViewState["pregunta"] - 1;
            }
            else if ((int)ViewState["pregunta"] == 5)
            {
                preguntaCinco.Visible = false;
                preguntaCuatro.Visible = true;
                //Aqui validar el envio a la base de datos de cuantas preguntas fueron correctas
                ViewState["pregunta"] = (int)ViewState["pregunta"] - 1;
            }
            else if ((int)ViewState["pregunta"] == 6)
            {
                preguntaSeis.Visible = false;
                preguntaCinco.Visible = true;
                //Aqui validar el envio a la base de datos de cuantas preguntas fueron correctas
                ViewState["pregunta"] = (int)ViewState["pregunta"] - 1;
            }

            else if ((int)ViewState["pregunta"] == 7)
            {
                preguntaSiete.Visible = false;
                preguntaSeis.Visible = true;
                //Aqui validar el envio a la base de datos de cuantas preguntas fueron correctas
                ViewState["pregunta"] = (int)ViewState["pregunta"] - 1;
            }

            else if ((int)ViewState["pregunta"] == 8)
            {
                preguntaOcho.Visible = false;
                preguntaSiete.Visible = true;
                //Aqui validar el envio a la base de datos de cuantas preguntas fueron correctas
                ViewState["pregunta"] = (int)ViewState["pregunta"] - 1;
            }
            else if ((int)ViewState["pregunta"] == 9)
            {
                PreguntaNueve.Visible = false;
                preguntaOcho.Visible = true;
                //Aqui validar el envio a la base de datos de cuantas preguntas fueron correctas
                ViewState["pregunta"] = (int)ViewState["pregunta"] - 1;
            }

            else if ((int)ViewState["pregunta"] == 10)
            {
                preguntaDiez.Visible = false;
                PreguntaNueve.Visible = true;
                //Aqui validar el envio a la base de datos de cuantas preguntas fueron correctas
                ViewState["pregunta"] = (int)ViewState["pregunta"] - 1;
            }

            else if ((int)ViewState["pregunta"] == 11)
            {
                preguntaOnce_.Visible = false;
                preguntaDiez.Visible = true;
                //Aqui validar el envio a la base de datos de cuantas preguntas fueron correctas
                ViewState["pregunta"] = (int)ViewState["pregunta"] - 1;
            }

            else if ((int)ViewState["pregunta"] == 12)
            {
                preguntaDoce.Visible = false;
                preguntaOnce_.Visible = true;
                //Aqui validar el envio a la base de datos de cuantas preguntas fueron correctas
                ViewState["pregunta"] = (int)ViewState["pregunta"] - 1;
            }

            else if ((int)ViewState["pregunta"] == 13)
            {
                preguntaTrece.Visible = false;
                preguntaDoce.Visible = true;
                //Aqui validar el envio a la base de datos de cuantas preguntas fueron correctas
                ViewState["pregunta"] = (int)ViewState["pregunta"] - 1;
            }
            valor.InnerText = Convert.ToString((int)ViewState["pregunta"]);
        }

        protected void btnCerrarSesion_Click(object sender, EventArgs e)
        {
            Session["usuario"] = string.Empty;
            Session["password"] = string.Empty;
            Response.Redirect("https://localhost:44300/FrmHome.aspx");
        }
    }
}