﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace SALBA_WEB_Proyecto_2019
{
    public partial class FrmNivel3 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["pregunta"] = 1;
                /*
                
                string usuario = (string)Session["usuario"];
                string password = (string)Session["password"];
                if (usuario != string.Empty && password != string.Empty)
                {
                    nombreUsuario.InnerText = (string)Session["nombre"] + " " + (string)Session["apellido"];
                }
                else
                {
                    Response.Redirect("https://localhost:44300/FrmHome.aspx");
                }*/
                Session["valor"] = 0;
            }
        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            if ((int)ViewState["pregunta"] == 1)
            {
                preguntaUno.Visible = false;
                preguntaDos.Visible = true;
                ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;
            }
            else if ((int)ViewState["pregunta"] == 2)
            {
                if (Convert.ToInt32(txtParaUno.Value) == 1 && Convert.ToInt32(txtParaDos.Value) == 1 && Convert.ToInt32(txtParaTres.Value) == 1)
                {
                    string script = "<script type=\"text/javascript\">alert('Correcto');</script>";
                    Response.Write(script);
                    preguntaDos.Visible = false;
                    preguntaTres.Visible = true;
                    ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;
                }
                else
                {
                    string script = "<script type=\"text/javascript\">alert('Respuesta Incorrecta');</script>";
                    Response.Write(script);
                }

            }
            else if ((int)ViewState["pregunta"] == 3)
            {
                if(Convert.ToInt32(txtmientrasUno.Value) ==1 && Convert.ToInt32(txtMientrasDos.Value) == 1 && Convert.ToInt32(txtMientrasTres.Value) == 1)
                {
                    string script = "<script type=\"text/javascript\">alert('Correcto');</script>";
                    Response.Write(script);
                    preguntaTres.Visible = false;
                    preguntaCuatro.Visible = true;
                    ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;
                }
                else
                {
                    string script = "<script type=\"text/javascript\">alert('Respuesta Incorrecta');</script>";
                    Response.Write(script);
                }
            }
            else if ((int)ViewState["pregunta"] == 4)
            {
                if(Convert.ToInt32(txtRepitaUno.Value) ==1 && Convert.ToInt32(txtRepitaUno.Value) == 1 && Convert.ToInt32(txtRepitaTres.Value) == 1)
                {
                    string script = "<script type=\"text/javascript\">alert('Correcto');</script>";
                    Response.Write(script);
                    preguntaCuatro.Visible = false;
                    preguntaCinco.Visible = true;
                    ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;
                }
                else
                {
                    string script = "<script type=\"text/javascript\">alert('Respues Incorrecta');</script>";
                    Response.Write(script);
                }
            }
            else if ((int)ViewState["pregunta"] == 5)
            {
                //Primera pregunta
                if (rbtTres.Checked == true)
                {
                    Session["valor"] = +1;
                }
                else
                {
                    Session["valor"] = +0;
                }
                preguntaCinco.Visible = false;
                preguntaSeis.Visible = true;
                ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;
            }
            else if ((int)ViewState["pregunta"] == 6)
            {
                //Segunda pregunta
                if (Convert.ToInt32(txtSegunda1.Value) == 1 && Convert.ToInt32(txtSegunda2.Value) == 1 && Convert.ToInt32(txtSegunda3.Value) == 1)
                {
                    Session["valor"] = +1;
                }
                else
                {
                    Session["valor"] = +0;
                }
                preguntaSeis.Visible = false;
                preguntaSiete.Visible = true;
                ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;
            }
            else if ((int)ViewState["pregunta"] == 7)
            {
                //Tercera pregunta
                if (rbtTresCUATRO.Checked == true)
                {
                    Session["valor"] = +1;
                }
                else
                {
                    Session["Tercero"] = 0;
                }
                preguntaSiete.Visible = false;
                preguntaOcho.Visible = true;
                ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;
            }
            else if ((int)ViewState["pregunta"] == 8)
            {
                //Cuarta pregunta
                if (Convert.ToInt32(txtCuatro1.Value) == 1 && Convert.ToInt32(txtCuatro2.Value) == 1 && Convert.ToInt32(txtCuatro3.Value) == 1)
                {
                    Session["valor"] = +1;
                }
                else
                {
                    Session["valor"] = +0;
                }
                preguntaOcho.Visible = false;
                preguntaNueve.Visible = true;
                ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;
            }
            else if ((int)ViewState["pregunta"] == 9)
            {
                //Quinta pregunta
                if (txtQuintoUno.Text.Trim() == "CARACTER" && txtQuintoDos.Text.Trim()=="REPITA" && txtQuintaTres.Text.Trim() == "respuesta")
                {
                    Session["valor"] = +1;
                }
                else
                {
                    Session["valor"] = +0;
                }
                ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;
            }
            valor.InnerText = Convert.ToString((int)ViewState["pregunta"]);
        }

        protected void btnRegresar_Click(object sender, EventArgs e)
        {
            if ((int)ViewState["pregunta"] == 1)
            {
            }
            else if ((int)ViewState["pregunta"] == 2)
            {
                preguntaDos.Visible = false;
                preguntaUno.Visible = true;
                ViewState["pregunta"] = (int)ViewState["pregunta"] - 1;
            }
            else if ((int)ViewState["pregunta"] == 3)
            {
                preguntaTres.Visible = false;
                preguntaDos.Visible = true;
                ViewState["pregunta"] = (int)ViewState["pregunta"] - 1;
            }
            else if ((int)ViewState["pregunta"] == 4)
            {
                preguntaCuatro.Visible = false;
                preguntaTres.Visible = true;
                ViewState["pregunta"] = (int)ViewState["pregunta"] - 1;
            }
            else if ((int)ViewState["pregunta"] == 5)
            {
                preguntaCinco.Visible = false;
                preguntaCuatro.Visible = true;
                ViewState["pregunta"] = (int)ViewState["pregunta"] - 1;
            }
            else if ((int)ViewState["pregunta"] == 6)
            {
                preguntaSeis.Visible = false;
                preguntaCinco.Visible = true;
                ViewState["pregunta"] = (int)ViewState["pregunta"] - 1;
            }
            else if ((int)ViewState["pregunta"] == 7)
            {
                preguntaSiete.Visible = false;
                preguntaSeis.Visible = true;
                ViewState["pregunta"] = (int)ViewState["pregunta"] - 1;
            }
            else if ((int)ViewState["pregunta"] == 8)
            {
                //Aqui validar el envio a la base de datos de cuantas preguntas fueron correctas
                preguntaOcho.Visible = false;
                preguntaSiete.Visible = true;
                ViewState["pregunta"] = (int)ViewState["pregunta"] - 1;
            }
            else if ((int)ViewState["pregunta"] == 9)
            {
                //Aqui validar el envio a la base de datos de cuantas preguntas fueron correctas
                preguntaNueve.Visible = false;
                preguntaOcho.Visible = true;
                ViewState["pregunta"] = (int)ViewState["pregunta"] - 1;
            }
            valor.InnerText = Convert.ToString((int)ViewState["pregunta"]);
        }

        protected void btnCerrarSesion_Click(object sender, EventArgs e)
        {
            Session["usuario"] = string.Empty;
            Session["password"] = string.Empty;
            Response.Redirect("https://localhost:44300/FrmHome.aspx");
        }

        protected void btnPrimera_Click(object sender, EventArgs e)
        {
                    
        }

        protected void btnenviar_Click1(object sender, EventArgs e)
        {
                             
        }

        protected void btnPreguntaCinco_Click(object sender, EventArgs e)
        {
            
        }
    }
}