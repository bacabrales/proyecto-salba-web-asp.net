﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SALBA_WEB_Proyecto_2019
{
    public partial class FrmNivel1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["pregunta"] = 1;
                ViewState["nivelUno"] = 0;
                //string usuario = (string)Session["usuario"];
                //string password = (string)Session["password"];
                //if (usuario != string.Empty && password != string.Empty)
                //{
                //    nombreUsuario.InnerText = (string)Session["nombre"] + " " + (string)Session["apellido"];
                //}
                //else
                //{
                //    Response.Redirect("https://localhost:44300/FrmHome.aspx");
                //}
            }
        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            if((int)ViewState["pregunta"] == 1)
            {
                if (Convert.ToInt32(txtValidarIntroA.Value)==1 && Convert.ToInt32(txtValidarIntroA.Value) == 1)
                {
                    string script = string.Format("alert('Correcto')");
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", script, true);
                    preguntaUno.Visible = false;
                    preguntaDos.Visible = true;
                    ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;
                }
                else
                {
                    string script = string.Format("alert('Incorrecto')");
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", script, true);
                }
                
            }else if ((int)ViewState["pregunta"] == 2)
            {
                if (txtValidarUno.Text.Trim() == "nro1" && txtValidarDos.Text.Trim() == "resultado")
                {
                    string script = string.Format("alert('Correcto')");
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", script, true);
                    preguntaDos.Visible = false;
                    preguntaTres.Visible = true;
                    ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;
                    btnEnviar.Text = "Iniciar test";
                }
                else
                {
                    string script = string.Format("alert('Incorrecto')");
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", script, true);
                }
                
            }else if ((int)ViewState["pregunta"] == 3)
            {
                if (Convert.ToInt32(txtValiPeg3A.Value) == 1 && Convert.ToInt32(txtValiPeg3B.Value) == 1 && Convert.ToInt32(txtValiPeg3C.Value) == 1)
                {
                    string script = string.Format("alert('Correcto')");
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", script, true);
                    preguntaTres.Visible = false;
                    preguntaCinco.Visible = true;
                    ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;
                    btnEnviar.Text = "Finalizar test";
                    btnRegresar.Enabled = false;
                    btnRegresar.Visible = false;
                }
                else
                {
                    string script = string.Format("alert('Incorrecto')");
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", script, true);
                }
                
            }else if ((int)ViewState["pregunta"] == 4)
            {
                string validarDos = "ENTEROnro1,nro2,resultadoINICIOESCRIBA'Digiteunnumero'nro1<-6ESCRIBA'Digiteunnumero'nro2<-2resultado<-nro1-nro2FIN".ToString();
                string validarUno = "ENTEROnroDeAutos".ToString();
                string datoDos = txtAreaDos.Value.Replace(" ","").ToString();
                string datoUno = txtAreaUno.Value.Replace(" ", "").ToString();

                if (customRadio1.Checked==true)
                {
                    ViewState["nivelUno"] = (int)ViewState["nivelUno"] + 1;
                }
                else
                {
                    ViewState["nivelUno"] = (int)ViewState["nivelUno"] + 0;
                }

                if (customRadio7.Checked == true)
                {
                    ViewState["nivelUno"] = (int)ViewState["nivelUno"] + 1;
                }
                else
                {
                    ViewState["nivelUno"] = (int)ViewState["nivelUno"] + 0;
                }

                if (datoUno == validarUno)
                {
                    ViewState["nivelUno"] = (int)ViewState["nivelUno"] + 1;
                }
                else
                {
                    ViewState["nivelUno"] = (int)ViewState["nivelUno"] + 0;
                }

                if (datoDos == validarDos)
                {
                    ViewState["nivelUno"] = (int)ViewState["nivelUno"] + 1;
                }
                else
                {
                    ViewState["nivelUno"] = (int)ViewState["nivelUno"] + 0;
                }

                if ((int)ViewState["nivelUno"] >= 3)
                {
                    //La nota se guarda en nivelUno
                    string script = string.Format("alert('Paso')");
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", script, true);
                }
                else
                {
                    string script = string.Format("alert('Perdio')");
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", script, true);
                }

                //preguntaCuatro.Visible = false;
                //preguntaCinco.Visible = true;
                //ViewState["pregunta"] = (int)ViewState["pregunta"] + 1;
            }
            //else if ((int)ViewState["pregunta"] == 5)
            //{


            //    //Aqui validar el envio a la base de datos de cuantas preguntas fueron correctas
            //}
            valor.InnerText = Convert.ToString((int)ViewState["pregunta"]);
        }

        protected void btnRegresar_Click(object sender, EventArgs e)
        {
            if ((int)ViewState["pregunta"] == 1)
            {
                //Redirecciona al inicio de la pagina de las instrucciones
            }
            else if ((int)ViewState["pregunta"] == 2)
            {
                preguntaDos.Visible = false;
                preguntaUno.Visible = true;
                ViewState["pregunta"] = (int)ViewState["pregunta"] - 1;
            }
            else if ((int)ViewState["pregunta"] == 3)
            {
                preguntaTres.Visible = false;
                preguntaDos.Visible = true;
                ViewState["pregunta"] = (int)ViewState["pregunta"] - 1;
            }
            else if ((int)ViewState["pregunta"] == 4)
            {
                preguntaCuatro.Visible = false;
                preguntaTres.Visible = true;
                ViewState["pregunta"] = (int)ViewState["pregunta"] - 1;
            }
            else if ((int)ViewState["pregunta"] == 5)
            {
                preguntaCinco.Visible = false;
                preguntaCuatro.Visible = true;
                //Aqui validar el envio a la base de datos de cuantas preguntas fueron correctas
                ViewState["pregunta"] = (int)ViewState["pregunta"] - 1;
            }
            valor.InnerText = Convert.ToString((int)ViewState["pregunta"]);
        }

        protected void btnCerrarSesion_Click(object sender, EventArgs e)
        {
            Session["usuario"] = string.Empty;
            Session["password"] = string.Empty;
            Response.Redirect("https://localhost:44300/FrmHome.aspx");
        }
    }
}