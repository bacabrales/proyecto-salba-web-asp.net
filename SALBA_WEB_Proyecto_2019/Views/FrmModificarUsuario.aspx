﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrmModificarUsuario.aspx.cs" Inherits="SALBA_WEB_Proyecto_2019.Views.FrmModificarUsuario" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>SALBA - Modificar</title>
    <link rel="stylesheet" href="../css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../css/estilosPropios.css"/>
    <link href="../css/estilos.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" />
    <script src="https://kit.fontawesome.com/a5cc566dc4.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"/>
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web&display=swap" rel="stylesheet"/>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <link rel="stylesheet" href="../css/main.css"/>
    <link rel="icon" href="../img/logoSena.png"/>
</head>
<body>

    <form id="frmModificar" runat="server">
        <nav class="navbar navbar-expand-sm navbar-light bg-light bg-Blanco navbar-expand-md shadow-sm fixed-top">
            <div class="container">

                <a class="navbar-brand prueba" href="#">
                    <img src="../img/LogoSalba.png" width="130" class="img-fluid" alt="" /></a>
                <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="collapsibleNavId">
                    <div class="navbar-nav ml-auto mt-2 mt-lg-0">
                        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                            <li class="nav-item active">
                                <a href="FrmPrincipal.aspx" class="btn  mr-1 text-primary enlace">Inicio</a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="btn text-black ml-1 enlace"><i class="fas fa-user-circle mr-2"></i><b id="nombreUsuario" runat="server"></b></a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-cog"></i>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#">Configurar Perfil</a>
                                    <a class="dropdown-item" href="#">Politicas</a>
                                    <div class="dropdown-divider"></div>
                                    <asp:Button ID="btnCerrarSesion" CssClass="dropdown-item" runat="server" Text="Cerrar sesión"/>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    
        <div class="contenedor-principal py-5">
            <div class="contenedor-config">
                <div class="card shadow-sm" style="margin: 60px">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-4" style="border-right: 1px solid #e1e1e1">
                                <i class="fas fa-user-circle mr-2 d-block text-center mb-2" style="font-size: 40px"></i>
                                <h4 class="card-title text-center">Información de usuario</h4>
                            </div>
                            <!--col-->
                            <div class="col-lg-8">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <p class="card-text text-center my-3">Configuración general de la cuenta <i class="fas fa-pen-fancy"></i></p>
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-lg-8 clearfix">
                                        <label for="txtNombreMo">Nombre usuario</label>
                                        <div class="input-group">
                                            <asp:TextBox CssClass="form-control form-control-m" disabled="disabled" ID="txtNombreMo" placeholder="Nombre" runat="server"></asp:TextBox>
                                            <%--<input type="text" class="form-control form-control-m" id="txtNombreMo" placeholder="Nombre" disabled="disabled" value=""/>--%>
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="button" id="btnModNom">Editar</button>
                                                <%--<asp:Button ID="btnModNom" CssClass="btn btn-default" runat="server" Text="Editar" OnClick="btnModNom_Click" />--%>
                                            </span>
                                        </div>
                                        <label for="txtApellidoMo">Apellido usuario</label>
                                        <div class="input-group">
                                            <asp:TextBox CssClass="form-control form-control-m" disabled="disabled" ID="txtApellidoMo" placeholder="Apellido" runat="server"></asp:TextBox>
                                            <%--<input type="text" class="form-control form-control-m" id="txtApellidoMo" placeholder="Apellido" disabled="disabled" value=""/>--%>
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="button" id="btnModApe">Editar</button>
                                                <%--<asp:Button ID="btnModApe" CssClass="btn btn-default" runat="server" Text="Editar" OnClick="btnModApe_Click" />--%>
                                            </span>
                                        </div>
                                        <label for="txtUsuarioMo">Usuario</label>
                                        <div class="input-group">
                                            <asp:TextBox CssClass="form-control form-control-m" disabled="disabled" ID="txtUsuarioMo" placeholder="Usuario" runat="server"></asp:TextBox>
                                            <%--<input type="text" class="form-control form-control-m"  id="txtUsuarioMo" placeholder="Usuario" disabled="disabled" value=""/>--%>
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="button" id="btnModUsu">Editar</button>
                                                <%--<asp:Button ID="btnModUsu" CssClass="btn btn-default" runat="server" Text="Editar" OnClick="btnModUsu_Click" />--%>
                                            </span>
                                        </div>
                                        <label for="txtCorreoMo">Correo</label>
                                        <div class="input-group">
                                            <asp:TextBox TextMode="Email" CssClass="form-control form-control-m" disabled="disabled" ID="txtCorreoMo" placeholder="Correo" runat="server"></asp:TextBox>
                                            <%--<input type="email" class="form-control form-control-m" id="txtCorreoMo" placeholder="Correo" disabled="disabled" value=""/>--%>
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="button" id="btnModCorr">Editar</button>
                                                <%--<asp:Button ID="btnModCorr" CssClass="btn btn-default" runat="server" Text="Editar" OnClick="btnModCorr_Click" />--%>
                                            </span>
                                        </div>
                                        <label for="txtPasswordMo">Contraseña actual</label>
                                        <div class="input-group">
                                            <asp:TextBox TextMode="Password" CssClass="form-control form-control-m" disabled="disabled" ID="txtPasswordMo" placeholder="Password" runat="server"></asp:TextBox>
                                            <%--<input type="password" class="form-control form-control-m"  id="txtPasswordMo" placeholder="Password" disabled="disabled"/>--%>
                                            <button class="btn btn-default" type="button" id="btnModContra">Editar</button>
                                            <%--<asp:Button ID="btnModContra" CssClass="btn btn-default" runat="server" Text="Editar" OnClick="btnModContra_Click" />--%>
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" runat="server" hidden="hidden" type="button" id="btnModContraCerr">Cerrar</button>
                                                <%--<asp:Button ID="btnModContraCerr" CssClass="btn btn-default" visible="false" runat="server" Text="Cerrar" OnClick="btnModContraCerr_Click" />--%>
                                            </span>
                                        </div>
                                        <div id="modificarContrasena" runat="server" hidden="hidden">
                                            <div class="row justify-content-center">
                                                <div class="col-lg-10">
                                                    <label for="txtPasswordMo" class="d-block">Nueva contraseña</label>
                                                    <asp:TextBox TextMode="Password" ID="txtPassNew1" CssClass="form-control  d-block" placeholder="Contraseña nueva" runat="server"></asp:TextBox>
                                                    <label for="txtPasswordMo" class="d-block">Confirma nueva contraseña</label>
                                                     <asp:TextBox TextMode="Password" ID="txtPassNew2" CssClass="form-control  d-block" placeholder="Confirmar contraseña" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <!--#modificarContrasea-->
                                        <div id="contGuardar" class="input-group mt-3 text-center" runat="server" hidden="hidden">
                                            <span class="input-group-btn">
                                                 <asp:Button ID="btnEnviar" CssClass="btn btn-outline-success" runat="server" Text="Guardar Datos" OnClick="btnEnviar_Click" />
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--col-->
                        </div>
                        <!--row-->
                    </div>
                </div>
            </div>
        </div><!--contenedor-principal-->
    </form>
    

    
    <script src="../js/jquery-3.3.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/validarFrmModificar.js"></script>
</body>
</html>
