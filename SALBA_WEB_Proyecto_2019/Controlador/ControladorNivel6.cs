﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using SALBA_WEB_Proyecto_2019.Modelo;

namespace SALBA_WEB_Proyecto_2019.Controlador
{
    public class ControladorNivel6
    {
        private Nivel6 nivel = new Nivel6();
        public int guardar(FrmNivel6 nivel6,int id,int r1)
        {
            int  r2, r3, r4, r5,nota;
            
            if (r1 != 5)
            {
                r1 = 0;
            }
            if (((RadioButtonList)nivel6.Page.FindControl("p2")).SelectedValue=="1")
            {
                r2 = 5;
            }
            else
            {
                r2 = 0;
            }
            if (((RadioButtonList)nivel6.Page.FindControl("p3")).SelectedValue == "0")
            {
                r3 = 5;
            }
            else
            {
                r3 = 0;
            }
            if (((RadioButtonList)nivel6.Page.FindControl("p4")).SelectedValue == "1")
            {
                r4 = 5;
            }
            else
            {
                r4 = 0;
            }
            if (((RadioButtonList)nivel6.Page.FindControl("p5")).SelectedValue == "1")
            {
                r5 = 5;
            }
            else
            {
                r5 = 0;
            }
            nota = (r1 + r2 + r3 + r4 + r5) / 5;


            return nivel.insertarNota6(nota,id);
        }
    }
}