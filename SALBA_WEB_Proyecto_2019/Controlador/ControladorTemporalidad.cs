﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SALBA_WEB_Proyecto_2019.Modelo;

namespace SALBA_WEB_Proyecto_2019.Controlador
{
    public class ControladorTemporalidad
    {
        private EntidadTemporalidad temporalidad = new EntidadTemporalidad();
        private Temporalidad tem = new Temporalidad();
        public int bucarTemporalidadNivel6(int id)
        {
            temporalidad.id = id;
            return tem.bucarTemporalidadNivel6(temporalidad);
        }
        public void insertarTem6(int id ,int level6)
        {
            temporalidad.id = id;
            temporalidad.level6 = level6;
            tem.insertarTem6(temporalidad);
        }
    }
}