﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace SALBA_WEB_Proyecto_2019.Modelo
{
    public class Temporalidad
    {
        private SqlConnection con;
        private int tem;
        public int bucarTemporalidadNivel6(EntidadTemporalidad tem)
        {
            con = Singleton.obtenerConexion();
            SqlCommand comando = new SqlCommand("pa_temporalidad", con);
            comando.CommandType = CommandType.StoredProcedure;

            comando.Parameters.AddWithValue("@id", tem.id);
            SqlDataReader red;
            red= comando.ExecuteReader();
            red.Read();
            
            if (red["levelSeis"].ToString() != "")
            {
                this.tem = Convert.ToInt32(red["levelSeis"]);
            }
            else
            {
                this.tem = 0;
            }
            comando.Dispose();
            red.Close();
            return this.tem;
            
        }
        public void insertarTem6(EntidadTemporalidad tem)
        {
            con = Singleton.obtenerConexion();
            SqlCommand comando = new SqlCommand("pa_insertarTem6", con);
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@id", tem.id);
            comando.Parameters.AddWithValue("@tem", tem.level6);
            comando.ExecuteNonQuery();

        }
    }
}