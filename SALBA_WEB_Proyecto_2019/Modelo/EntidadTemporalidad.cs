﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SALBA_WEB_Proyecto_2019.Modelo
{
    public class EntidadTemporalidad
    {
        public int level1 { get; set; }
        public int level2 { get; set; }
        public int level3 { get; set; }
        public int level4 { get; set; }
        public int level5 { get; set; }
        public int level6 { get; set; }
        public int id { get; set; }
    }
}