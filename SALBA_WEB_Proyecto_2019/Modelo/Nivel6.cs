﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace SALBA_WEB_Proyecto_2019.Modelo
{
    public class Nivel6
    {
        SqlConnection con;
        public int insertarNota6(int nota, int id)
        {
            con = Singleton.obtenerConexion();
            SqlCommand comando = new SqlCommand("pa_guardarLevel6", con);
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@id", id);
            comando.Parameters.AddWithValue("@nota", nota);

            return comando.ExecuteNonQuery();

        }
    }
}