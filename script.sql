USE [Salba]
GO
/****** Object:  Table [dbo].[Rol]    Script Date: 12/03/2019 16:29:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Rol](
	[idRol] [int] IDENTITY(1,1) NOT NULL,
	[nombreRol] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[idRol] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Nivel]    Script Date: 12/03/2019 16:29:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Nivel](
	[idNivel] [int] IDENTITY(1,1) NOT NULL,
	[nombreNivel] [varchar](25) NULL,
	[numeroNivel] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[idNivel] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 12/03/2019 16:29:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuario](
	[idUser] [int] IDENTITY(1,1) NOT NULL,
	[nomUser] [varchar](68) NULL,
	[apelUser] [varchar](68) NULL,
	[correUser] [varchar](68) NULL,
	[contraUser] [varchar](68) NULL,
	[estadoUser] [char](1) NOT NULL,
	[fechaCreacion] [date] NULL,
	[idRol] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[idUser] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[correUser] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[pa_insertar]    Script Date: 12/03/2019 16:29:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pa_insertar]
@nombre VARCHAR(68),
@apellido VARCHAR(68),
@correo VARCHAR(68),
@password VARCHAR(68),
@tipo int
AS
begin
INSERT INTO Usuario VALUES (@nombre,@apellido,@correo,@password,'A',SYSDATETIME ( ),@tipo) SELECT SCOPE_IDENTITY();
end
GO
/****** Object:  StoredProcedure [dbo].[pa_buscar]    Script Date: 12/03/2019 16:29:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pa_buscar]
@correo VARCHAR(68),
@password VARCHAR(68)
as
begin
SELECT * FROM Usuario WHERE correUser = @correo AND contraUser = @password;
end
GO
/****** Object:  Table [dbo].[Temporalidad]    Script Date: 12/03/2019 16:29:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Temporalidad](
	[levelUno] [int] NULL,
	[levelDos] [int] NULL,
	[leveltres] [int] NULL,
	[levelCuatro] [int] NULL,
	[levelCinco] [int] NULL,
	[levelSeis] [int] NULL,
	[idUser] [int] NULL,
	[idTem] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[idTem] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Examen]    Script Date: 12/03/2019 16:29:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Examen](
	[idExamen] [int] IDENTITY(1,1) NOT NULL,
	[fechaInicio] [date] NULL,
	[puntaje] [int] NULL,
	[fechaExamen] [date] NULL,
	[idUser] [int] NULL,
	[idNivel] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[idExamen] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Competencia]    Script Date: 12/03/2019 16:29:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Competencia](
	[idCompe] [int] IDENTITY(1,1) NOT NULL,
	[puntaje] [int] NULL,
	[fechaComp] [date] NULL,
	[idUser] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[idCompe] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Codigo]    Script Date: 12/03/2019 16:29:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Codigo](
	[idCodigo] [varchar](10) NOT NULL,
	[idUser] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[idCodigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[pa_temporalidad]    Script Date: 12/03/2019 16:29:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[pa_temporalidad]
@id int
as
begin
	select MAX(levelSeis) as levelSeis from Temporalidad where idUser=@id;
end
GO
/****** Object:  StoredProcedure [dbo].[pa_tem]    Script Date: 12/03/2019 16:29:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[pa_tem]
@id int
as
begin
	insert into Temporalidad(levelUno,levelDos,leveltres,levelCuatro,levelCinco,levelSeis,idUser) values(1,1,1,1,1,1,@id);
end
GO
/****** Object:  StoredProcedure [dbo].[pa_insertarTem6]    Script Date: 12/03/2019 16:29:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[pa_insertarTem6]
@id int,
@tem int
as
begin
	insert into Temporalidad(levelSeis,idUser) values(@tem,@id);
end
GO
/****** Object:  Table [dbo].[Aso]    Script Date: 12/03/2019 16:29:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Aso](
	[idAso] [int] IDENTITY(1,1) NOT NULL,
	[idCodigo] [varchar](10) NULL,
	[idUsuarioEstudiante] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[idAso] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Check [CK__Usuario__estadoU__07F6335A]    Script Date: 12/03/2019 16:29:55 ******/
ALTER TABLE [dbo].[Usuario]  WITH CHECK ADD CHECK  (([estadoUser]='I' OR [estadoUser]='A'))
GO
/****** Object:  ForeignKey [FK__Aso__idCodigo__1920BF5C]    Script Date: 12/03/2019 16:29:55 ******/
ALTER TABLE [dbo].[Aso]  WITH CHECK ADD FOREIGN KEY([idCodigo])
REFERENCES [dbo].[Codigo] ([idCodigo])
GO
/****** Object:  ForeignKey [FK__Aso__idUsuarioEs__1A14E395]    Script Date: 12/03/2019 16:29:55 ******/
ALTER TABLE [dbo].[Aso]  WITH CHECK ADD FOREIGN KEY([idUsuarioEstudiante])
REFERENCES [dbo].[Usuario] ([idUser])
GO
/****** Object:  ForeignKey [FK__Codigo__idUser__145C0A3F]    Script Date: 12/03/2019 16:29:55 ******/
ALTER TABLE [dbo].[Codigo]  WITH CHECK ADD FOREIGN KEY([idUser])
REFERENCES [dbo].[Usuario] ([idUser])
GO
/****** Object:  ForeignKey [FK__Competenc__idUse__0DAF0CB0]    Script Date: 12/03/2019 16:29:55 ******/
ALTER TABLE [dbo].[Competencia]  WITH CHECK ADD FOREIGN KEY([idUser])
REFERENCES [dbo].[Usuario] ([idUser])
GO
/****** Object:  ForeignKey [FK__Examen__idNivel__239E4DCF]    Script Date: 12/03/2019 16:29:55 ******/
ALTER TABLE [dbo].[Examen]  WITH CHECK ADD FOREIGN KEY([idNivel])
REFERENCES [dbo].[Nivel] ([idNivel])
GO
/****** Object:  ForeignKey [FK__Examen__idUser__22AA2996]    Script Date: 12/03/2019 16:29:55 ******/
ALTER TABLE [dbo].[Examen]  WITH CHECK ADD FOREIGN KEY([idUser])
REFERENCES [dbo].[Usuario] ([idUser])
GO
/****** Object:  ForeignKey [FK__Temporali__idUse__0F975522]    Script Date: 12/03/2019 16:29:55 ******/
ALTER TABLE [dbo].[Temporalidad]  WITH CHECK ADD FOREIGN KEY([idUser])
REFERENCES [dbo].[Usuario] ([idUser])
GO
/****** Object:  ForeignKey [FK__Usuario__idRol__08EA5793]    Script Date: 12/03/2019 16:29:55 ******/
ALTER TABLE [dbo].[Usuario]  WITH CHECK ADD FOREIGN KEY([idRol])
REFERENCES [dbo].[Rol] ([idRol])
GO
